<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class ModulesTableSeeder extends Seeder
{
    public function run()
    {
        

        $modules = [
                    ['icon' => 'fa fa-bullhorn','name' => 'Recruitment','permalink' => 'recruitment'],
                    ['icon' => 'fa fa-user','name' => 'Employee','permalink' => 'employee'],
                    ['icon' => 'fa fa-money','name' => 'Payroll','permalink' => 'payroll'],
                    ['icon' => 'fa fa-users','name' => 'Users','permalink' => 'users'],
        ];

        $module_roles = [
                            ['module_id' => 1, 'role_id' => 1 ], /* Recruitment Admin */
                            ['module_id' => 1, 'role_id' => 2 ], /* Recruitment Supervisor */
                            ['module_id' => 1, 'role_id' => 3 ], /* Recruitment Coordinator */
                            ['module_id' => 2, 'role_id' => 1 ], /* Employee Admin */
                            ['module_id' => 2, 'role_id' => 2 ], /* Employee Supervisor */
                            ['module_id' => 2, 'role_id' => 3 ], /* Employee Coordinator */
                            ['module_id' => 3, 'role_id' => 1 ], /* Payroll Admin */
                            ['module_id' => 3, 'role_id' => 2 ], /* Payroll Supervisor */
                            ['module_id' => 3, 'role_id' => 3 ], /* Payroll Coordinator */
                            ['module_id' => 4, 'role_id' => 1 ], /* User Admin */
                            ['module_id' => 4, 'role_id' => 2 ], /* User Supervisor */
                            ['module_id' => 4, 'role_id' => 3 ], /* User Coordinator */
                        ];



        DB::table('modules')->insert($modules);
        DB::table('module_roles')->insert($module_roles);
    }
}
