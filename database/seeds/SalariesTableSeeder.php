<?php

use Illuminate\Database\Seeder;

class SalariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $salaries = [
                        ['employee_id' => 1, 'amount' => 15500, 'is_active' => "active"],
                        ['employee_id' => 2, 'amount' => 18000, 'is_active' => "active"],
                        ['employee_id' => 3, 'amount' => 16000, 'is_active' => "active"],
                        ['employee_id' => 4, 'amount' => 14000, 'is_active' => "active"],
                        ['employee_id' => 5, 'amount' => 15000, 'is_active' => "active"],
                        ['employee_id' => 6, 'amount' => 12000, 'is_active' => "active"],
                        ['employee_id' => 7, 'amount' => 13000, 'is_active' => "active"],
                        ['employee_id' => 8, 'amount' => 15000, 'is_active' => "active"],
                        ['employee_id' => 9, 'amount' => 14000, 'is_active' => "active"],
                        ['employee_id' => 10, 'amount' => 14000, 'is_active' => "active"],
                        ['employee_id' => 11, 'amount' => 13000, 'is_active' => "active"],
                        ['employee_id' => 12, 'amount' => 12000, 'is_active' => "active"],

                  ];


          DB::table('salaries')->insert($salaries);
    }
}
