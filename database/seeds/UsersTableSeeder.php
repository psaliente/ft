<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;
use Carbon\Carbon as Carbon;
class UsersTableSeeder extends Seeder
{
    public function run()
    {

    	//DB::table('users')->truncate();

    	$users = [
        			[
            			'username' => 'FT-000001',
            			'password' => bcrypt('FT-000001'),
                        'user_type' => 1,
            			'created_at' => Carbon::now(),
            			'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'FT-000002',
                        'password' => bcrypt('FT-000002'),
                        'user_type' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'FT-000003',
                        'password' => bcrypt('FT-000003'),
                        'user_type' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'FT-000004',
                        'password' => bcrypt('FT-000004'),
                        'user_type' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'FT-000005',
                        'password' => bcrypt('FT-000005'),
                        'user_type' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    
                    [
                        'username' => 'crisvillegas@yahoo.com',
                        'password' => bcrypt('crisvillegas@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'aarontolentino@gmail.com',
                        'password' => bcrypt('aarontolentino@gmail.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'michaeltuazon@yahoo.com',
                        'password' => bcrypt('michaeltuazon@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'lesliegomez@gyahoo.com',
                        'password' => bcrypt('lesliegomez@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    [
                        'username' => 'dominicroxas@gyahoo.com',
                        'password' => bcrypt('dominicroxas@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ]

                    ,

                    
                    [
                        'username' => 'froilanmaga@yahoo.com',
                        'password' => bcrypt('froilanmaga@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'noeljarilla@yahoo.com',
                        'password' => bcrypt('noeljarilla@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'joycemagno@yahoo.com',
                        'password' => bcrypt('joycemagno@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'louvega@yahoo.com',
                        'password' => bcrypt('louvega@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'alchui@yahoo.com',
                        'password' => bcrypt('alchui@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'tonylim@yahoo.com',
                        'password' => bcrypt('tonylim@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'francisloy@yahoo.com',
                        'password' => bcrypt('francisloy@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'chriscruz@yahoo.com',
                        'password' => bcrypt('chriscruz@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'bamuy@yahoo.com',
                        'password' => bcrypt('bamuy@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],

                    [
                        'username' => 'beasi@yahoo.com',
                        'password' => bcrypt('beasi@yahoo.com'),
                        'user_type' => 3,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'is_verified' => true
                    ],
                    
    			];
                



    	$user_detail = [
                            [
        					'user_id' => 1,
        					'firstname' => 'Lex',
                            'middlename' => '',
        					'lastname' => 'Villaralvo',
        					//'user_slug' => 'jerichorosales',
        					'profile_photo' => 'avatar.png',
                            'email' => 'lex.villaralvo@yahoo.com',
                            'contact' => '091567891011'
        				   ],
                           [
                            'user_id' => 2,
                            'firstname' => 'Jean',
                            'middlename' => '',
                            'lastname' => 'Cruz',
                            //'user_slug' => 'deancruzana',
                            'profile_photo' => 'avatar.png',
                            'email' => 'jean.cruz@yahoo.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 3,
                            'firstname' => 'Ana',
                            'middlename' => '',
                            'lastname' => 'Zubiri',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'jeric.rafael@yahoo.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 4,
                            'firstname' => 'Coco',
                            'middlename' => '',
                            'lastname' => 'Martin',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'jeric.rafael@yahoo.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 5,
                            'firstname' => 'Coleen',
                            'middlename' => '',
                            'lastname' => 'Garcia',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'jeric.rafael@yahoo.com',
                            'contact' => '091789101112'
                           ],
                          
                           [
                            'user_id' => 6,
                            'firstname' => 'Cris',
                            'middlename' => '',
                            'lastname' => 'Villegas',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'crisvillegas@gmail.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 7,
                            'firstname' => 'Aaron',
                            'middlename' => '',
                            'lastname' => 'Tolentino',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'aarontolentino@gmail.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 8,
                            'firstname' => 'Michael',
                            'middlename' => '',
                            'lastname' => 'Tuazon',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'michaeltuazon@yahoo.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 9,
                            'firstname' => 'Leslie',
                            'middlename' => '',
                            'lastname' => 'Gomez',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'lesliegomez@yahoo.com',
                            'contact' => '091789101112'
                           ],
                           [
                            'user_id' => 10,
                            'firstname' => 'Dominic',
                            'middlename' => '',
                            'lastname' => 'Roxas',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'dominicroxas@yahoo.com',
                            'contact' => '091789101112'
                           ],


                            [
                            'user_id' => 11,
                            'firstname' => 'Froilan',
                            'middlename' => '',
                            'lastname' => 'Maga',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'froilanmaga@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 12,
                            'firstname' => 'Noel',
                            'middlename' => '',
                            'lastname' => 'Jarilla',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'noeljarilla@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 13,
                            'firstname' => 'Joyce',
                            'middlename' => '',
                            'lastname' => 'Magno',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'joycemagno@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 14,
                            'firstname' => 'Lou',
                            'middlename' => '',
                            'lastname' => 'Vega',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'louvega@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 15,
                            'firstname' => 'Al',
                            'middlename' => '',
                            'lastname' => 'Chui',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'alchui@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 16,
                            'firstname' => 'Tony',
                            'middlename' => '',
                            'lastname' => 'Lim',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'tonylim@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 17,
                            'firstname' => 'Francis',
                            'middlename' => '',
                            'lastname' => 'Loy',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'francisloy@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 18,
                            'firstname' => 'Chris',
                            'middlename' => '',
                            'lastname' => 'Cruz',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'chriscruz@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 19,
                            'firstname' => 'Bam',
                            'middlename' => '',
                            'lastname' => 'Uy',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'bamuy@gmail.com',
                            'contact' => '091789101112'
                           ],

                            [
                            'user_id' => 20,
                            'firstname' => 'Bea',
                            'middlename' => '',
                            'lastname' => 'Si',
                            //'user_slug' => 'jericrafael',
                            'profile_photo' => 'avatar.png',
                            'email' => 'beasi@gmail.com',
                            'contact' => '091789101112'
                           ],
                           
                        ];


        $module_role_users = [
                                ['module_role_id'=> 1, 'user_id' => 1],
                                ['module_role_id'=> 4, 'user_id' => 1],
                                ['module_role_id'=> 7, 'user_id' => 1],
                                ['module_role_id'=> 10, 'user_id' => 1],
                                
                                ['module_role_id'=> 1, 'user_id' => 3],
                                ['module_role_id'=> 4, 'user_id' => 4],
                                ['module_role_id'=> 7, 'user_id' => 2],

                                ['module_role_id'=> 1, 'user_id' => 5],
                                ['module_role_id'=> 4, 'user_id' => 5]
                                
                             ];
 
        DB::table('users')->insert($users);
        DB::table('user_details')->insert($user_detail);
        DB::table('module_role_users')->insert($module_role_users);
    }
}
