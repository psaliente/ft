<?php

use Illuminate\Database\Seeder;
	
class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
                        //['name' => 'Report','permalink' => 'app/recruitment/report','type' => 1],
                        ['name' => 'Clients','permalink' => 'app/recruitment/clients','type' => 1],
                        ['name' => 'Job Posts','permalink' => 'app/recruitment/jobs','type' => 1],
                        ['name' => 'Applicants','permalink' => 'app/recruitment/applicants','type' => 1],
                        
                        //['name' => 'Report','permalink' => 'app/employee/report','type' => 1],
                        ['name' => 'All Employees','permalink' => 'app/employee/employees','type' => 1],

                        //['name' => 'Report','permalink' => 'app/payroll/report','type' => 1],
                        ['name' => 'Clients','permalink' => 'app/payroll/clients','type' => 1],

                        ['name' => 'All Users','permalink' => 'app/users','type' => 1],
                    ];


        $module_role_actions = [
        							['module_role_id' => 1, 'action_id' => 1],
                      ['module_role_id' => 1, 'action_id' => 2],
                      ['module_role_id' => 1, 'action_id' => 3],

                      ['module_role_id' => 4, 'action_id' => 4],


                      ['module_role_id' => 7, 'action_id' => 5],

                      ['module_role_id' => 10, 'action_id' => 6],
        					   ];


      	DB::table('actions')->insert($actions);
      	DB::table('module_role_actions')->insert($module_role_actions);
    }
}
