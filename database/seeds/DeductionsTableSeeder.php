<?php

use Illuminate\Database\Seeder;

class DeductionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $deductions = [
                         [
                            'employee_id' => 1,
                            'name' => 'SSS Contribution',
                            'amount' => 150,
                        ],
                        [
                            'employee_id' => 1,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 1,
                            'name' => 'Withholding Tax',
                            'amount' => 1922,
                        ],
                        [
                            'employee_id' => 1,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 2,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 2,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 2,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 2,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 3,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 3,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 3,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 3,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 4,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 4,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 4,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 4,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 5,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 5,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 5,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 5,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 6,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 6,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 6,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 6,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 7,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 7,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 7,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 7,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 8,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 8,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 8,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 8,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 9,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 9,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 9,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 9,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 10,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 10,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 10,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 10,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 11,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 11,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 11,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 11,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ],


                        [
                            'employee_id' => 12,
                            'name' => 'SSS Contribution',
                            'amount' => 220,
                        ],
                        [
                            'employee_id' => 12,
                            'name' => 'PAGIBIG Contribution',
                            'amount' => 200,
                        ],
                        [
                            'employee_id' => 12,
                            'name' => 'Withholding Tax',
                            'amount' => 2360,
                        ],
                        [
                            'employee_id' => 12,
                            'name' => 'PhilHealth',
                            'amount' => 100,
                        ]
                      ];

          DB::table('deductions')->insert($deductions);
    }
}
