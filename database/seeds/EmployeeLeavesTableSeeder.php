<?php

use Illuminate\Database\Seeder;

class EmployeeLeavesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $leaves = [
                        [
                            'leave_type_id' => '1',
                            'employee_id' => '1',
                            'balance' => 7 
                        ],
                         [
                            'leave_type_id' => '2',
                            'employee_id' => '1',
                            'balance' => 7 
                        ],
                         [
                            'leave_type_id' => '1',
                            'employee_id' => '2',
                            'balance' => 7 
                        ],
                         [
                            'leave_type_id' => '2',
                            'employee_id' => '2',
                            'balance' => 7 
                        ]
                      ];

          DB::table('leaves')->insert($leaves);
    }
}
