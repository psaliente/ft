<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $clients = [
                        [
                        'name'=> 'Jollibee',
                        'logo' => 'avatar.png',
                        'representative' => 'Norman Cruz', 
                        'address' => 'Jollibee Centre Ortigas Mandaluyong City', 
                        'email' => 'jobs@kusinanigracia.com', 
                        'contact' => '642-22-54',
                        'created_at' => Carbon::now()
                        ],
                        [
                        'name'=> 'Kaymu Philippines',
                        'logo' => 'avatar.png',
                        'representative' => 'Kris Mendoza', 
                        'address' => '4Floor, 405 Angelus Medical Plaza, 104 V.A. Rufino St. cor Dela Rosa St., Makati City', 
                        'email' => 'info@kaymu.com', 
                        'contact' => '454-15-55',
                        'created_at' => Carbon::now()
                        ],
                         [
                        'name'=> 'Paotsin',
                        'logo' => 'avatar.png',
                        'representative' => 'Andre Manzano', 
                        'address' => 'Worldwide Corporation Bldg. Shaw Blvd. Mandaluyong City', 
                        'email' => 'jobs@paostsin.com', 
                        'contact' => '621-50-95',
                        'created_at' => Carbon::now()
                        ],
                         [
                        'name'=> 'Seaoil',
                        'logo' => 'avatar.png',
                        'representative' => 'Ludwig Tuazon', 
                        'address' => 'Ortigas Extension Cainta, Rizal', 
                        'email' => 'recruitment@petron.com', 
                        'contact' => '222-53-06',
                        'created_at' => Carbon::now()
                        ],
                         [
                        'name'=> 'Megasign Corporation',
                        'logo' => 'avatar.png',
                        'representative' => 'Robert Zuniga', 
                        'address' => '2/f Promenade Bldg., San Juan, Philippines', 
                        'email' => 'recruitment@shell.com', 
                        'contact' => '422-62-95',
                        'created_at' => Carbon::now()
                        ],

                        
                   
                   ];


        DB::table('clients')->insert($clients);
    }
}
