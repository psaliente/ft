<?php

use Illuminate\Database\Seeder;

class BenefitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $benefits = [
                        [
                            'employee_id' => 1,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 1,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ],
                        [
                            'employee_id' => 2,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 2,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ],
                         [
                            'employee_id' => 2,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 3,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 3,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 4,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 4,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 5,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 5,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 6,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 6,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 7,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 7,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 8,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 8,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 9,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 9,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 10,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 10,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 11,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 11,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]
                        ,
                        [
                            'employee_id' => 12,
                            'name' => 'Clothing allowance',
                            'amount' => 750,
                        ],
                        [
                            'employee_id' => 12,
                            'name' => 'Rice allowance',
                            'amount' => 1000,
                        ]

                      ];

          DB::table('benefits')->insert($benefits);
    }
}
