<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class LeavesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $types = [
                        ['name' => 'Vacation', 'created_at' => Carbon::now()],
                        ['name' => 'Sick', 'created_at' => Carbon::now()],
                        ['name' => 'Bereavement', 'created_at' => Carbon::now()],
                        ['name' => 'Paternal', 'created_at' => Carbon::now()],
                        ['name' => 'Maternal', 'created_at' => Carbon::now()],
                      ];

          DB::table('leave_types')->insert($types);
    }
}
