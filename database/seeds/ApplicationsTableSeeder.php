<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $applications = [
                    [
                     'job_id' => 9,
                     'user_id' => 6,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 1,
                     'user_id' => 8,
                     'status' => 'New',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 5,
                     'user_id' => 9,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 4,
                     'user_id' => 11,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 5,
                     'user_id' => 12,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 6,
                     'user_id' => 13,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],

                     [
                     'job_id' => 6,
                     'user_id' => 14,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 7,
                     'user_id' => 15,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 9,
                     'user_id' => 16,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 8,
                     'user_id' => 17,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 4,
                     'user_id' => 18,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 2,
                     'user_id' => 19,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ],
                     [
                     'job_id' => 3,
                     'user_id' => 20,
                     'status' => 'Passed',
                     'created_at' => Carbon::now()
                    ]
        		];

        DB::table('applications')->insert($applications);
    }
}
