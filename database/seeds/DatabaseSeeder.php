<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('LeavesTableSeeder');
        $this->call('RoleTableSeeder');
        $this->call('ModulesTableSeeder');
        $this->call('ActionsTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('ClientsTableSeeder');
        $this->call('JobsTableSeeder');
        $this->call('ApplicantsTableSeeder');
        $this->call('ApplicationsTableSeeder');
        $this->call('EmployeesTableSeeder');
        $this->call('EmployeeLeavesTableSeeder');
        $this->call('BenefitsTableSeeder');
        $this->call('DeductionsTableSeeder');
        $this->call('SalariesTableSeeder');
        $this->call('TimesheetsTableSeeder');
        
        Model::reguard();
    }
}
