<?php

use Illuminate\Database\Seeder;

class TimesheetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 12 ; $i++) { 
            
        
         $time_sheets = [
                        /*
                        ['user_id' => 1, 'time_in' => '07:00:00','lunch_out' => '11:56:00', 'lunch_in' => '12:55:00', 'time_out' => '16:05:00','date' => '2015-07-15', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:05:00','lunch_out' => '12:01:00', 'lunch_in' => '12:59:00', 'time_out' => '16:10:00','date' => '2015-07-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:03:00','lunch_out' => '12:10:00', 'lunch_in' => '13:05:00', 'time_out' => '16:03:00','date' => '2015-07-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:00:00','lunch_out' => '12:00:00', 'lunch_in' => '13:00:00', 'time_out' => '16:05:00','date' => '2015-07-20', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:00:00','lunch_out' => '11:59:00', 'lunch_in' => '12:54:00', 'time_out' => '16:05:00','date' => '2015-07-21', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:02:00','lunch_out' => '11:55:00', 'lunch_in' => '12:55:00', 'time_out' => '16:03:00','date' => '2015-07-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:16:00','lunch_out' => '12:07:00', 'lunch_in' => '12:53:00', 'time_out' => '16:09:00','date' => '2015-07-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:00:00','lunch_out' => '12:06:00', 'lunch_in' => '13:05:00', 'time_out' => '16:05:00','date' => '2015-07-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['user_id' => 1, 'time_in' => '07:00:00','lunch_out' => '12:02:00', 'lunch_in' => '12:52:00', 'time_out' => '16:02:00','date' => '2015-07-27', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 1, 'time_in' => '07:01:00','lunch_out' => '11:58:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-07-28', 'day_type' => 'Regular','status' => 'No' ],


                        ['user_id' => 2, 'time_in' => '07:01:00','lunch_out' => '11:56:00', 'lunch_in' => '12:56:00', 'time_out' => '16:02:00','date' => '2015-07-15', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:02:00','lunch_out' => '12:04:00', 'lunch_in' => '12:58:00', 'time_out' => '16:00:00','date' => '2015-07-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:04:00','lunch_out' => '12:00:00', 'lunch_in' => '13:06:00', 'time_out' => '16:00:00','date' => '2015-07-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:00:00','lunch_out' => '12:00:00', 'lunch_in' => '13:00:00', 'time_out' => '16:01:00','date' => '2015-07-20', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:00:00','lunch_out' => '11:59:00', 'lunch_in' => '12:51:00', 'time_out' => '16:05:00','date' => '2015-07-21', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:00:00','lunch_out' => '11:55:00', 'lunch_in' => '12:56:00', 'time_out' => '16:04:00','date' => '2015-07-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:10:00','lunch_out' => '12:01:00', 'lunch_in' => '12:58:00', 'time_out' => '16:06:00','date' => '2015-07-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:00:00','lunch_out' => '12:03:00', 'lunch_in' => '13:09:00', 'time_out' => '16:07:00','date' => '2015-07-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['user_id' => 2, 'time_in' => '07:01:00','lunch_out' => '12:00:00', 'lunch_in' => '12:51:00', 'time_out' => '16:01:00','date' => '2015-07-27', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 2, 'time_in' => '07:06:00','lunch_out' => '11:56:00', 'lunch_in' => '12:52:00', 'time_out' => '16:16:00','date' => '2015-07-28', 'day_type' => 'Regular','status' => 'No' ],


                        ['user_id' => 3, 'time_in' => '07:00:00','lunch_out' => '11:52:00', 'lunch_in' => '12:56:00', 'time_out' => '16:09:00','date' => '2015-07-15', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:03:00','lunch_out' => '12:03:00', 'lunch_in' => '12:57:00', 'time_out' => '16:01:00','date' => '2015-07-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:00:00','lunch_out' => '12:01:00', 'lunch_in' => '13:08:00', 'time_out' => '16:02:00','date' => '2015-07-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:00:00','lunch_out' => '12:00:00', 'lunch_in' => '13:09:00', 'time_out' => '16:03:00','date' => '2015-07-20', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:06:00','lunch_out' => '11:57:00', 'lunch_in' => '12:51:00', 'time_out' => '16:04:00','date' => '2015-07-21', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:08:00','lunch_out' => '11:58:00', 'lunch_in' => '12:52:00', 'time_out' => '16:05:00','date' => '2015-07-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:12:00','lunch_out' => '12:02:00', 'lunch_in' => '12:53:00', 'time_out' => '16:06:00','date' => '2015-07-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:00:00','lunch_out' => '12:03:00', 'lunch_in' => '13:04:00', 'time_out' => '16:07:00','date' => '2015-07-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['user_id' => 3, 'time_in' => '07:00:00','lunch_out' => '12:01:00', 'lunch_in' => '12:55:00', 'time_out' => '16:08:00','date' => '2015-07-27', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 3, 'time_in' => '07:06:00','lunch_out' => '11:58:00', 'lunch_in' => '12:56:00', 'time_out' => '16:19:00','date' => '2015-07-28', 'day_type' => 'Regular','status' => 'No' ],

                        ['user_id' => 4, 'time_in' => '07:00:00','lunch_out' => '11:52:00', 'lunch_in' => '12:57:00', 'time_out' => '16:01:00','date' => '2015-07-15', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:06:00','lunch_out' => '12:06:00', 'lunch_in' => '12:58:00', 'time_out' => '16:02:00','date' => '2015-07-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:01:00','lunch_out' => '12:01:00', 'lunch_in' => '13:09:00', 'time_out' => '16:03:00','date' => '2015-07-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:00:00','lunch_out' => '12:00:00', 'lunch_in' => '13:00:00', 'time_out' => '16:04:00','date' => '2015-07-20', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:00:00','lunch_out' => '11:56:00', 'lunch_in' => '12:51:00', 'time_out' => '16:05:00','date' => '2015-07-21', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:08:00','lunch_out' => '11:57:00', 'lunch_in' => '12:52:00', 'time_out' => '16:06:00','date' => '2015-07-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:10:00','lunch_out' => '12:02:00', 'lunch_in' => '12:53:00', 'time_out' => '16:07:00','date' => '2015-07-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:00:00','lunch_out' => '12:09:00', 'lunch_in' => '13:04:00', 'time_out' => '16:08:00','date' => '2015-07-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['user_id' => 4, 'time_in' => '07:01:00','lunch_out' => '12:01:00', 'lunch_in' => '12:56:00', 'time_out' => '16:09:00','date' => '2015-07-27', 'day_type' => 'Regular','status' => 'No' ],
                        ['user_id' => 4, 'time_in' => '07:02:00','lunch_out' => '11:52:00', 'lunch_in' => '12:57:00', 'time_out' => '16:10:00','date' => '2015-07-28', 'day_type' => 'Regular','status' => 'No' ],
                        */

                        ['employee_id' => $i, 'time_in' => '07:04:00','lunch_out' => '11:56:00', 'lunch_in' => '12:58:00', 'time_out' => '16:05:00','date' => '2015-06-01', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-06-02', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:06:00','lunch_out' => '12:01:00', 'lunch_in' => '13:00:00', 'time_out' => '16:07:00','date' => '2015-06-03', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:07:00','lunch_out' => '12:03:00', 'lunch_in' => '13:01:00', 'time_out' => '16:08:00','date' => '2015-06-04', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:08:00','lunch_out' => '12:00:00', 'lunch_in' => '12:52:00', 'time_out' => '16:09:00','date' => '2015-06-05', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:00:00', 'lunch_in' => '12:53:00', 'time_out' => '16:02:00','date' => '2015-06-08', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:00:00','lunch_out' => '12:08:00', 'lunch_in' => '12:54:00', 'time_out' => '16:03:00','date' => '2015-06-09', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:05:00', 'lunch_in' => '13:05:00', 'time_out' => '16:04:00','date' => '2015-06-10', 'day_type' => 'Regular','status' => 'No' ], 
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:12:00', 'lunch_in' => '12:56:00', 'time_out' => '16:05:00','date' => '2015-06-11', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:11:00','lunch_out' => '12:15:00', 'lunch_in' => '12:57:00', 'time_out' => '16:16:00','date' => '2015-06-12', 'day_type' => 'Regular','status' => 'No' ],

                        ['employee_id' => $i, 'time_in' => '07:04:00','lunch_out' => '11:56:00', 'lunch_in' => '12:58:00', 'time_out' => '16:05:00','date' => '2015-06-15', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-06-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:06:00','lunch_out' => '12:01:00', 'lunch_in' => '13:00:00', 'time_out' => '16:07:00','date' => '2015-06-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:07:00','lunch_out' => '12:03:00', 'lunch_in' => '13:01:00', 'time_out' => '16:08:00','date' => '2015-06-18', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:08:00','lunch_out' => '12:00:00', 'lunch_in' => '12:52:00', 'time_out' => '16:09:00','date' => '2015-06-19', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:00:00', 'lunch_in' => '12:53:00', 'time_out' => '16:02:00','date' => '2015-06-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:00:00','lunch_out' => '12:08:00', 'lunch_in' => '12:54:00', 'time_out' => '16:03:00','date' => '2015-06-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:05:00', 'lunch_in' => '13:05:00', 'time_out' => '16:04:00','date' => '2015-06-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:12:00', 'lunch_in' => '12:56:00', 'time_out' => '16:05:00','date' => '2015-06-25', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:11:00','lunch_out' => '12:15:00', 'lunch_in' => '12:57:00', 'time_out' => '16:16:00','date' => '2015-06-26', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:08:00','lunch_out' => '12:00:00', 'lunch_in' => '12:52:00', 'time_out' => '16:09:00','date' => '2015-06-29', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:00:00', 'lunch_in' => '12:53:00', 'time_out' => '16:02:00','date' => '2015-06-30', 'day_type' => 'Regular','status' => 'No' ],

                        ['employee_id' => $i, 'time_in' => '07:04:00','lunch_out' => '11:56:00', 'lunch_in' => '12:58:00', 'time_out' => '16:05:00','date' => '2015-07-01', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-07-02', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:06:00','lunch_out' => '12:01:00', 'lunch_in' => '13:00:00', 'time_out' => '16:07:00','date' => '2015-07-03', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:07:00','lunch_out' => '12:03:00', 'lunch_in' => '13:01:00', 'time_out' => '16:08:00','date' => '2015-07-04', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:08:00','lunch_out' => '12:00:00', 'lunch_in' => '12:52:00', 'time_out' => '16:09:00','date' => '2015-07-06', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:00:00', 'lunch_in' => '12:53:00', 'time_out' => '16:02:00','date' => '2015-07-07', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:00:00','lunch_out' => '12:08:00', 'lunch_in' => '12:54:00', 'time_out' => '16:03:00','date' => '2015-07-08', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:05:00', 'lunch_in' => '13:05:00', 'time_out' => '16:04:00','date' => '2015-07-09', 'day_type' => 'Regular','status' => 'No' ], 
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:12:00', 'lunch_in' => '12:56:00', 'time_out' => '16:05:00','date' => '2015-07-10', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:11:00','lunch_out' => '12:15:00', 'lunch_in' => '12:57:00', 'time_out' => '16:16:00','date' => '2015-07-13', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:04:00','lunch_out' => '11:56:00', 'lunch_in' => '12:58:00', 'time_out' => '16:05:00','date' => '2015-07-14', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-07-15', 'day_type' => 'Regular','status' => 'No' ],

                        
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-07-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:06:00','lunch_out' => '12:01:00', 'lunch_in' => '13:00:00', 'time_out' => '16:07:00','date' => '2015-07-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:07:00','lunch_out' => '12:03:00', 'lunch_in' => '13:01:00', 'time_out' => '16:08:00','date' => '2015-07-20', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:08:00','lunch_out' => '12:00:00', 'lunch_in' => '12:52:00', 'time_out' => '16:09:00','date' => '2015-07-21', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:00:00', 'lunch_in' => '12:53:00', 'time_out' => '16:02:00','date' => '2015-07-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:00:00','lunch_out' => '12:08:00', 'lunch_in' => '12:54:00', 'time_out' => '16:03:00','date' => '2015-07-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:05:00', 'lunch_in' => '13:05:00', 'time_out' => '16:04:00','date' => '2015-07-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:12:00', 'lunch_in' => '12:56:00', 'time_out' => '16:05:00','date' => '2015-07-27', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:11:00','lunch_out' => '12:15:00', 'lunch_in' => '12:57:00', 'time_out' => '16:16:00','date' => '2015-07-28', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:04:00','lunch_out' => '11:56:00', 'lunch_in' => '12:58:00', 'time_out' => '16:05:00','date' => '2015-07-29', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-07-30', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:06:00','lunch_out' => '12:01:00', 'lunch_in' => '13:00:00', 'time_out' => '16:07:00','date' => '2015-07-31', 'day_type' => 'Regular','status' => 'No' ],



                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:06:00','date' => '2015-08-03', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:06:00','lunch_out' => '12:01:00', 'lunch_in' => '13:00:00', 'time_out' => '16:07:00','date' => '2015-08-04', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:07:00','lunch_out' => '12:03:00', 'lunch_in' => '13:01:00', 'time_out' => '16:08:00','date' => '2015-08-05', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:08:00','lunch_out' => '12:00:00', 'lunch_in' => '12:52:00', 'time_out' => '16:09:00','date' => '2015-08-06', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:00:00', 'lunch_in' => '12:53:00', 'time_out' => '16:02:00','date' => '2015-08-07', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:00:00','lunch_out' => '12:08:00', 'lunch_in' => '12:54:00', 'time_out' => '16:03:00','date' => '2015-08-10', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:01:00','lunch_out' => '12:05:00', 'lunch_in' => '13:05:00', 'time_out' => '16:04:00','date' => '2015-08-11', 'day_type' => 'Regular','status' => 'No' ], 
                        ['employee_id' => $i, 'time_in' => '07:09:00','lunch_out' => '12:12:00', 'lunch_in' => '12:56:00', 'time_out' => '16:05:00','date' => '2015-08-12', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:11:00','lunch_out' => '12:15:00', 'lunch_in' => '12:57:00', 'time_out' => '16:16:00','date' => '2015-08-13', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => $i, 'time_in' => '07:04:00','lunch_out' => '11:56:00', 'lunch_in' => '12:58:00', 'time_out' => '16:05:00','date' => '2015-08-14', 'day_type' => 'Regular','status' => 'No' ],


                        /*
                        ['employee_id' => 2, 'time_in' => '07:15:00','lunch_out' => '11:52:00', 'lunch_in' => '12:58:00', 'time_out' => '16:07:00','date' => '2015-07-15', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:09:00','lunch_out' => '12:07:00', 'lunch_in' => '12:59:00', 'time_out' => '16:08:00','date' => '2015-07-16', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:00:00','lunch_out' => '12:08:00', 'lunch_in' => '13:00:00', 'time_out' => '16:09:00','date' => '2015-07-17', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:01:00','lunch_out' => '12:06:00', 'lunch_in' => '13:01:00', 'time_out' => '16:12:00','date' => '2015-07-20', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:00:00','lunch_out' => '11:55:00', 'lunch_in' => '12:54:00', 'time_out' => '16:02:00','date' => '2015-07-21', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:00:00','lunch_out' => '11:59:00', 'lunch_in' => '12:55:00', 'time_out' => '16:13:00','date' => '2015-07-22', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:02:00','lunch_out' => '12:00:00', 'lunch_in' => '12:56:00', 'time_out' => '16:05:00','date' => '2015-07-23', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:00:00','lunch_out' => '12:00:00', 'lunch_in' => '13:07:00', 'time_out' => '16:06:00','date' => '2015-07-24', 'day_type' => 'Regular','status' => 'No' ], 
                        ['employee_id' => 2, 'time_in' => '07:00:00','lunch_out' => '12:07:00', 'lunch_in' => '12:58:00', 'time_out' => '16:08:00','date' => '2015-07-27', 'day_type' => 'Regular','status' => 'No' ],
                        ['employee_id' => 2, 'time_in' => '07:04:00','lunch_out' => '11:52:00', 'lunch_in' => '12:59:00', 'time_out' => '16:10:00','date' => '2015-07-28', 'day_type' => 'Regular','status' => 'No' ],
                        */
                       ];

          DB::table('time_sheets')->insert($time_sheets);
      }
    }
}
