<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = [
                        [
                        'user_id' => 8,
                        'client_id' => 1,
                        'job_id' => 1,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 9,
                        'client_id' => 3,
                        'job_id' => 5,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 11,
                        'client_id' => 3,
                        'job_id' => 4,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 12,
                        'client_id' => 3,
                        'job_id' => 5,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 13,
                        'client_id' => 4,
                        'job_id' => 6,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 14,
                        'client_id' => 4,
                        'job_id' => 6,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 15,
                        'client_id' => 4,
                        'job_id' => 7,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 16,
                        'client_id' => 5,
                        'job_id' => 9,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 17,
                        'client_id' => 4,
                        'job_id' => 8,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 18,
                        'client_id' => 3,
                        'job_id' => 4,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 19,
                        'client_id' => 1,
                        'job_id' => 2,
                        'created_at' => Carbon::now()
                        ],
                        [
                        'user_id' => 20,
                        'client_id' => 2,
                        'job_id' => 3,
                        'created_at' => Carbon::now()
                        ],
            		];


        $employments = [
                        [
                        'employee_id' => 1
                        ],
                        [
                        'employee_id' => 2
                        ],
                        [
                        'employee_id' => 3
                        ],
                        [
                        'employee_id' => 4
                        ],
                        [
                        'employee_id' => 2
                        ],
                        [
                        'employee_id' => 5
                        ],
                        [
                        'employee_id' => 6
                        ],
                        [
                        'employee_id' => 7
                        ],
                        [
                        'employee_id' => 8
                        ],
                        [
                        'employee_id' => 9
                        ],
                        [
                        'employee_id' => 10
                        ],
                        [
                        'employee_id' => 11
                        ]
                    ];


      $employment_numbers = [
                                [
                                'employee_id' => 1
                                ],
                                [
                                'employee_id' => 2
                                ],
                                [
                                'employee_id' => 3
                                ],
                                [
                                'employee_id' => 4
                                ],
                                [
                                'employee_id' => 2
                                ],
                                [
                                'employee_id' => 5
                                ],
                                [
                                'employee_id' => 6
                                ],
                                [
                                'employee_id' => 7
                                ],
                                [
                                'employee_id' => 8
                                ],
                                [
                                'employee_id' => 9
                                ],
                                [
                                'employee_id' => 10
                                ],
                                [
                                'employee_id' => 11
                                ]
                            ];

        DB::table('employees')->insert($employees);
        DB::table('employments')->insert($employments);
        DB::table('employment_numbers')->insert($employment_numbers);
    }
}
