<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobs = [
        			     [
                      'client_id' => 1,
                			'created_at' => Carbon::now(),
                			'posted_at' => Carbon::now(),
                			'expires_at' => Carbon::now()->addDays(16),
                			'updated_at' => Carbon::now()
                    ],
                    [
                      'client_id' => 1,
                			'created_at' => Carbon::now(),
                			'posted_at' => Carbon::now(),
                			'expires_at' => Carbon::now()->addMonths(12),
                			'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 2,
                  			'created_at' => Carbon::now(),
                  			'posted_at' => Carbon::now(),
                  			'expires_at' => Carbon::now()->addWeeks(4),
                  			'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 3,
                  			'created_at' => Carbon::now(),
                  			'posted_at' => Carbon::now(),
                  			'expires_at' => Carbon::now()->addMonths(1),
                  			'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 3,
                  			'created_at' => Carbon::now(),
                  			'posted_at' => Carbon::now(),
                  			'expires_at' => Carbon::now()->addWeeks(6),
                  			'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 4,
                  			'created_at' => Carbon::now(),
                  			'posted_at' => Carbon::now(),
                  			'expires_at' => Carbon::now()->addMonths(3),
                  			'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 4,
                        'created_at' => Carbon::now(),
                        'posted_at' => Carbon::now(),
                        'expires_at' => Carbon::now()->addWeeks(3),
                        'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 4,
                        'created_at' => Carbon::now(),
                        'posted_at' => Carbon::now(),
                        'expires_at' => Carbon::now()->addWeeks(1),
                        'updated_at' => Carbon::now()
                    ],
                    [
                        'client_id' => 5,
                        'created_at' => Carbon::now(),
                        'posted_at' => Carbon::now(),
                        'expires_at' => Carbon::now()->addMonths(2),
                        'updated_at' => Carbon::now()
                    ],
                ];

        $job_categories = [
                  				  ['name' => 'Services'],
                  					['name' => 'Education'],
                  					['name' => 'Construction'],
                            ['name' => 'Manufacturing'],
                            ['name' => 'Others']
                  				];

       	$job_details = [
       						['job_id' => 1, 
       						 'job_category_id' => 1,
       						 'vacancy' => 8,
       						 'title' => 'Service Crew', 
       						 'description' => 'Laborum. Sit, placeat, suscipit et incididunt proident, vero sed magnam velit quae ut quod hic tempora vel ut dolor.', 
       						 'location' => 'Jollibee Centre Ortigas Mandaluyong City',
       						 'employment_type' => 1,
       						 'education' => "College Undergrad"
       						],
       						['job_id' => 2, 
       						 'job_category_id' => 1,
       						 'vacancy' => 2,
       						 'title' => 'Cashier', 
       						 'description' => 'Impedit, eum dolorem commodo doloribus velit esse voluptate vitae culpa, eligendi qui excepturi aperiam labore enim vel.', 
       						 'location' => 'Jollibee Centre Ortigas Mandaluyong City',
       						 'employment_type' => 1,
       						 'education' => "College Undergrad"
       						],
       						['job_id' => 3, 
       						 'job_category_id' => 2,
       						 'vacancy' => 2,
       						 'title' => 'Junior Administrative Assistant', 
       						 'description' => 'Quia esse eaque voluptatem. Suscipit nostrud dolorum placeat, placeat, nihil officiis exercitation vel temporibus nostrud.', 
       						 'location' => '4Floor, 405 Angelus Medical Plaza, 104 V.A. Rufino St. cor Dela Rosa St., Makati City, 1209',
       						 'employment_type' => 1,
       						 'education' => "College Graduate"
       						],
       						['job_id' => 4, 
       						 'job_category_id' => 1,
       						 'vacancy' => 3,
       						 'title' => 'Store Helper', 
       						 'description' => 'Reiciendis sed rerum porro tempor repudiandae consequatur id alias nihil voluptas est aliquam exercitationem qui.', 
       						 'location' => 'Worldwide Corporation Bldg. Shaw Blvd. Mandaluyong City',
       						 'employment_type' => 1,
       						 'education' => "Highschool Graduate"
       						],
                  ['job_id' => 5, 
                   'job_category_id' => 1,
                   'vacancy' => 3,
                   'title' => 'Cashier', 
                   'description' => 'Nihil voluptas vel libero aliquip eos ducimus, elit, numquam quidem sunt labore id exercitationem proident, consequuntur occaecat nisi qui amet.', 
                   'location' => 'Worldwide Corporation Bldg. Shaw Blvd. Mandaluyong City',
                   'employment_type' => 1,
                   'education' => "College Undergrad"
                  ],
       						['job_id' => 6, 
       						 'job_category_id' => 1,
       						 'vacancy' => 3,
       						 'title' => 'Gasoline Boy', 
       						 'description' => 'Deserunt officia sapiente quo deserunt quas quia excepteur exercitation eum ut et enim perspiciatis, adipisci accusantium ipsa, repudiandae consequatur.', 
       						 'location' => 'Ortigas Extension Cainta, Rizal',
       						 'employment_type' => 1,
       						 'education' => "Highschool Graduate"
       						],
       						['job_id' => 7, 
       						 'job_category_id' => 1,
       						 'vacancy' => 3,
       						 'title' => 'Security Guard', 
       						 'description' => 'Suscipit quos amet, a beatae itaque sed ad impedit, eaque atque reiciendis dolor in laboriosam, quis.', 
       						 'location' => 'Ortigas Extension Cainta, Rizal',
       						 'employment_type' => 1,
       						 'education' => "College Undergrad"
       						],
                  ['job_id' => 8, 
                   'job_category_id' => 1,
                   'vacancy' => 1,
                   'title' => 'Cashier', 
                   'description' => 'Dolorem voluptas vero ut nihil elit, repellendus. Aut omnis eos, exercitationem labore dolorem expedita rerum consequatur ex reiciendis quam nostrum.', 
                   'location' => 'Ortigas Extension Cainta, Rizal',
                   'employment_type' => 1,
                   'education' => "College Undergrad"
                  ],
                  ['job_id' => 9, 
                   'job_category_id' => 1,
                   'vacancy' => 2,
                   'title' => 'Storage Keeper', 
                   'description' => 'Reprehenderit magna dolores quos beatae asperiores est deserunt rerum fugit, aut ipsa, quos qui laborum. Et in qui earum temporibus.', 
                   'location' => '2/f Promenade Bldg., San Juan, Philippines',
                   'employment_type' => 1,
                   'education' => "Highschool Graduate"
                  ],
       				   ];

        $job_qualifications =  [
                                ['job_id' => 1, 'gender' => 'Both Male and Female', 'age' => '18-30', 'experience' => 'Atleast 1yr', 'education' => 'College Undergrad'],
                                ['job_id' => 2, 'gender' => 'Female', 'age' => '18-35', 'experience' => 'Atleast 1yr', 'education' => 'College Undergrad'],
                                ['job_id' => 3, 'gender' => 'Female', 'age' => '18-30', 'experience' => 'Atleast 1yr', 'education' => 'College Graduate'],
                                ['job_id' => 4, 'gender' => 'Male', 'age' => '18-30', 'experience' => 'No experience', 'education' => 'Highschool Graduate'],
                                ['job_id' => 5, 'gender' => 'Female', 'age' => '18-30', 'experience' => 'Atleast 1yr', 'education' => 'College Undergrad'],
                                ['job_id' => 6, 'gender' => 'Male', 'age' => '18-35', 'experience' => 'No experience', 'education' => 'Highschool Graduate'],
                                ['job_id' => 7, 'gender' => 'Male', 'age' => '20-30', 'experience' => 'Atleast 2yrs', 'education' => 'College Undergrad'],
                                ['job_id' => 8, 'gender' => 'Female', 'age' => '20-35', 'experience' => 'Atleast 1yr', 'education' => 'College Undergrad'],
                                ['job_id' => 9, 'gender' => 'Male', 'age' => '20-35', 'experience' => 'No experience', 'education' => 'Highschool Graduate']
                              ];


        DB::table('job_categories')->insert($job_categories);
        DB::table('jobs')->insert($jobs);
        DB::table('job_details')->insert($job_details);
        DB::table('job_qualifications')->insert($job_qualifications);
        
    }
}
