<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;

class ApplicantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $applicants = [
                        ['user_id' => 6,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '160',
                         'weight' => '75',
                         'address' => 'Pionerr Mandaluyong City',
                         'resume' => 'cris_villegas_resume.docx'
                        ],

                        ['user_id' => 7,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '110',
                         'weight' => '80',
                         'address' => 'Araneta Ave. Quezon City',
                         'resume' => ''
                        ],

                        ['user_id' => 8,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '120',
                         'weight' => '80',
                         'address' => 'Chino Roces Makati City',
                         'resume' => 'michael_tuazon_resume.docx'
                        ],

                        ['user_id' => 9,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Female',
                         'civil_status' => 'Single',
                         'height' => '125',
                         'weight' => '85',
                         'address' => 'Pioneer St. Cor. Bluemintritt Metro Manila',
                         'resume' => 'leslie_gomez_resume.docx'
                        ]
                        ,
                        ['user_id' => 10,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '125',
                         'weight' => '75',
                         'address' => 'Metro Manila',
                         'resume' => ''
                        ],

                        ['user_id' => 11,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '125',
                         'weight' => '75',
                         'address' => 'Metro Manila',
                         'resume' => 'froilan_maga_resume.docx'
                        ],

                         ['user_id' => 12,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '125',
                         'weight' => '75',
                         'address' => 'Caloocan City',
                         'resume' => 'noel_jarilla_resume.docx'
                        ],

                         ['user_id' => 13,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Female',
                         'civil_status' => 'Married',
                         'height' => '185',
                         'weight' => '105',
                         'address' => 'Antipolo City',
                         'resume' => 'joyce_magno_resume.docx'
                        ],

                         ['user_id' => 14,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '125',
                         'weight' => '75',
                         'address' => 'Mandaluyong City',
                         'resume' => 'lou_vega_resume.docx'
                        ],

                         ['user_id' => 15,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '165',
                         'weight' => '95',
                         'address' => 'Caloocan City',
                         'resume' => 'al_chui_resume.docx'
                        ],

                         ['user_id' => 16,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '135',
                         'weight' => '55',
                         'address' => 'Pasig City',
                         'resume' => 'tony_lim_resume.docx'
                        ],

                         ['user_id' => 17,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '145',
                         'weight' => '85',
                         'address' => 'Makati City',
                         'resume' => 'francis_loy_resume.docx'
                        ],

                         ['user_id' => 18,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '155',
                         'weight' => '105',
                         'address' => 'Caloocan City',
                         'resume' => 'chris_cruz_resume.docx'
                        ],

                         ['user_id' => 19,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Male',
                         'civil_status' => 'Single',
                         'height' => '135',
                         'weight' => '95',
                         'address' => 'Caloocan City',
                         'resume' => 'bam_uy_resume.docx'
                        ],

                         ['user_id' => 20,
                         'birthdate' => Carbon::now(),
                         'gender' => 'Female',
                         'civil_status' => 'Single',
                         'height' => '105',
                         'weight' => '85',
                         'address' => 'Caloocan City',
                         'resume' => 'bea_si_resume.docx'
                        ]
                      ];

        DB::table('applicants')->insert($applicants);
    }
}
