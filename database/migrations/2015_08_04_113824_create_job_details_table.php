<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->integer('job_category_id')->unsigned();
            $table->foreign('job_category_id')->references('id')->on('job_categories');
            $table->smallInteger('vacancy')->unsigned();
            $table->string('title', 250);
            $table->text('description');
            $table->text('location');
            $table->string('employment_type',25);
            $table->string('education',25);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_details');
    }
}
