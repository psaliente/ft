<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleRoleActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_role_actions', function (Blueprint $table) {
            $table->integer('module_role_id')->unsigned();
            $table->foreign('module_role_id')->references('id')->on('module_roles');
            $table->integer('action_id')->unsigned();
            $table->foreign('action_id')->references('id')->on('actions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_role_actions');
    }
}
