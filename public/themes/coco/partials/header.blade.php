<header class="inverted">        
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navigation">
                        <span class="icon-navicon"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <img src="{!! Theme::asset()->url('front/assets/img/logo.png') !!}" data-dark-src="{!! Theme::asset()->url('front/assets/img/logo_dark.png') !!}" alt="Coco Frontend Template" class="logo">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="main-navigation">
                    <ul class="nav navbar-nav navbar-right">
                         @if(Auth::check())
                                @if(Auth::user()->user_type == 2)
                                        <li >
                                            <a href="{!! url('client/dashboard') !!}" >
                                               <span class="text">HOME</span>
                                            </a>
                                        </li>
                                        
                                        <li class="dropdown ">
                                        <a href="#" class="dropdown-toggle dropdown-hover" data-toggle="dropdown"> 
                                            <span class="text">{!! Theme::get('name') !!}</span>
                                            <span class="caret"></span>
                                        </a>
                                            <ul class="dropdown-menu dropdown-menu-alt">
                                                <li><a href="{!! url('employer/profile/view/'. Auth::user()->id) !!}">Profile</a></li>
                                                <li><a href="{!! url('employer/logout') !!}">Logout</a></li>
                                            </ul>
                                        </li>
                                    
                                @else
                                        <li >
                                            <a href="{!! url('home') !!}" >
                                                <span class="meta">
                                                    <span class="text">HOME</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li >
                                            <a href="{!! url('jobs') !!}" >
                                                <span class="meta">
                                                    <span class="text">JOB</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="dropdown ">
                                        <a href="#" class="dropdown-toggle dropdown-hover" data-toggle="dropdown"> 
                                            <span class="meta">
                                                <span class="text">{!! Theme::get('name') !!}</span>
                                                 <span class="caret"></span>
                                            </span>
                                           
                                        </a>
                                            <ul class="dropdown-menu dropdown-menu-alt">
                                                <li><a href="{!! url('account') !!}">My Account</a></li>
                                                <li><a href="{!! url('logout') !!}">Logout</a></li>
                                            </ul>
                                        </li>
                                @endif
                            @else
                            <li>
                                <a href="{!! url('/') !!}">
                                    <span class="meta">
                                        <span class="text">HOME</span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{!! url('jobs') !!}">
                                    <span class="meta">
                                        <span class="text">JOBS</span>
                                    </span>
                                </a>
                            </li>
                          
                            <li>
                                <a href="{!! url('signup') !!}"> 
                                    <span class="meta">
                                        <span class="text">SIGNUP</span>
                                    </span>
                                </a>
                            </li>
                               <li>
                                <a href="{!! url('login') !!}">
                                    <span class="meta">
                                        <span class="text">LOGIN</span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{!! url('contact') !!}">
                                    <span class="meta">
                                        <span class="text">CONTACT US</span>
                                    </span>
                                </a>
                            </li>
                            @endif
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-->
        </nav> 

         
</header>