<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Fastrust');

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{{ $crumb["label"] }}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            // $theme->asset()->usePath()->add('core', 'core.js');
            // $theme->asset()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', array('jquery'));


                
            


            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', Auth::user());
            // });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                $theme->asset()->usePath()->add('jquery-ui', 'libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css');
                $theme->asset()->usePath()->add('bootstrap', 'libs/bootstrap/css/bootstrap.min.css');
                $theme->asset()->usePath()->add('font-awesome', 'libs/font-awesome/css/font-awesome.min.css');
                $theme->asset()->usePath()->add('fontello', 'libs/fontello/css/fontello.css');
                $theme->asset()->usePath()->add('animate.min', 'libs/animate-css/animate.min.css');
                $theme->asset()->usePath()->add('component', 'libs/nifty-modal/css/component.css');
                $theme->asset()->usePath()->add('magnific-popup', 'libs/magnific-popup/magnific-popup.css');
                $theme->asset()->usePath()->add('ios7-switch', 'libs/ios7-switch/ios7-switch.css');
                $theme->asset()->usePath()->add('pace', 'libs/pace/pace.css');
                $theme->asset()->usePath()->add('sortable-theme-bootstrap', 'libs/sortable/sortable-theme-bootstrap.css');
                $theme->asset()->usePath()->add('datepicker', 'libs/bootstrap-datepicker/css/datepicker.css');
                $theme->asset()->usePath()->add('all', 'libs/jquery-icheck/skins/all.css');

                
                //$theme->asset()->usePath()->add('all', 'libs/prettify/github.css');

                
              
                $theme->asset()->usePath()->add('style', 'css/style.css');
               
                $theme->asset()->usePath()->add('style-responsive', 'css/style-responsive.css');



                $theme->asset()->usePath()->add('jquery-1.11.1.min', 'libs/jquery/jquery-1.11.1.min.js');
                $theme->asset()->usePath()->add('bootstrap.min', 'libs/bootstrap/js/bootstrap.min.js');
                $theme->asset()->usePath()->add('jquery-ui-1.10.4.custom.min', 'libs/jqueryui/jquery-ui-1.10.4.custom.min.js');
                $theme->asset()->usePath()->add('jquery.ui.touch-punch.min', 'libs/jquery-ui-touch/jquery.ui.touch-punch.min.js');
                $theme->asset()->usePath()->add('jquery-detectmobile/detec', 'libs/jquery-detectmobile/detect.js');
                $theme->asset()->usePath()->add('libs/jquery-animate-numbers/jquery.animateNumbers', 'libs/jquery-animate-numbers/jquery.animateNumbers.js');
                $theme->asset()->usePath()->add('ios7.switch', 'libs/ios7-switch/ios7.switch.js');
                $theme->asset()->usePath()->add('fastclick', 'libs/fastclick/fastclick.js');
                $theme->asset()->usePath()->add('jquery.blockUI', 'libs/jquery-blockui/jquery.blockUI.js');
                $theme->asset()->usePath()->add('bootbox.min', 'libs/bootstrap-bootbox/bootbox.min.js');
                $theme->asset()->usePath()->add('jquery.slimscroll', 'libs/jquery-slimscroll/jquery.slimscroll.js');
                $theme->asset()->usePath()->add('jquery-sparkline', 'libs/jquery-sparkline/jquery-sparkline.js');
                $theme->asset()->usePath()->add('classie', 'libs/nifty-modal/js/classie.js');
                $theme->asset()->usePath()->add('modalEffects', 'libs/nifty-modal/js/modalEffects.js');
                $theme->asset()->usePath()->add('sortable.min', 'libs/sortable/sortable.min.js');
                $theme->asset()->usePath()->add('file-input', 'libs/bootstrap-fileinput/bootstrap.file-input.js');
                $theme->asset()->usePath()->add('bootstrap-select.min', 'libs/bootstrap-select/bootstrap-select.min.js');
                $theme->asset()->usePath()->add('select2.min', 'libs/bootstrap-select2/select2.min.js');
                $theme->asset()->usePath()->add('jquery.magnific-popup.min', 'libs/magnific-popup/jquery.magnific-popup.min.js');
                $theme->asset()->usePath()->add('pace.min', 'libs/pace/pace.min.js');
                $theme->asset()->usePath()->add('bootstrap-datepicker', 'libs/bootstrap-datepicker/js/bootstrap-datepicker.js');
                $theme->asset()->usePath()->add('icheck.min', 'libs/jquery-icheck/icheck.min.js');
                $theme->asset()->usePath()->add('prettify', 'libs/prettify/prettify.js');
                $theme->asset()->usePath()->add('init', 'js/init.js');
            },

            'backend' => function($theme)
            {
                $theme->asset()->usePath()->add('jquery-ui', 'libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css');
                $theme->asset()->usePath()->add('bootstrap', 'libs/bootstrap/css/bootstrap.min.css');
                $theme->asset()->usePath()->add('font-awesome', 'libs/font-awesome/css/font-awesome.min.css');
                $theme->asset()->usePath()->add('fontello', 'libs/fontello/css/fontello.css');
                $theme->asset()->usePath()->add('animate.min', 'libs/animate-css/animate.min.css');
                $theme->asset()->usePath()->add('component', 'libs/nifty-modal/css/component.css');
                $theme->asset()->usePath()->add('magnific-popup', 'libs/magnific-popup/magnific-popup.css');
                $theme->asset()->usePath()->add('ios7-switch', 'libs/ios7-switch/ios7-switch.css');
                $theme->asset()->usePath()->add('pace', 'libs/pace/pace.css');
                $theme->asset()->usePath()->add('sortable-theme-bootstrap', 'libs/sortable/sortable-theme-bootstrap.css');
                $theme->asset()->usePath()->add('datepicker', 'libs/bootstrap-datepicker/css/datepicker.css');
                $theme->asset()->usePath()->add('all', 'libs/jquery-icheck/skins/all.css');

                
                //$theme->asset()->usePath()->add('all', 'libs/prettify/github.css');

                
              
                $theme->asset()->usePath()->add('style', 'css/style.css');
               
                $theme->asset()->usePath()->add('style-responsive', 'css/style-responsive.css');



                $theme->asset()->usePath()->add('jquery-1.11.1.min', 'libs/jquery/jquery-1.11.1.min.js');
                $theme->asset()->usePath()->add('bootstrap.min', 'libs/bootstrap/js/bootstrap.min.js');
                $theme->asset()->usePath()->add('jquery-ui-1.10.4.custom.min', 'libs/jqueryui/jquery-ui-1.10.4.custom.min.js');
                $theme->asset()->usePath()->add('jquery.ui.touch-punch.min', 'libs/jquery-ui-touch/jquery.ui.touch-punch.min.js');
                $theme->asset()->usePath()->add('jquery-detectmobile/detec', 'libs/jquery-detectmobile/detect.js');
                $theme->asset()->usePath()->add('libs/jquery-animate-numbers/jquery.animateNumbers', 'libs/jquery-animate-numbers/jquery.animateNumbers.js');
                $theme->asset()->usePath()->add('ios7.switch', 'libs/ios7-switch/ios7.switch.js');
                $theme->asset()->usePath()->add('fastclick', 'libs/fastclick/fastclick.js');
                $theme->asset()->usePath()->add('jquery.blockUI', 'libs/jquery-blockui/jquery.blockUI.js');
                $theme->asset()->usePath()->add('bootbox.min', 'libs/bootstrap-bootbox/bootbox.min.js');
                $theme->asset()->usePath()->add('jquery.slimscroll', 'libs/jquery-slimscroll/jquery.slimscroll.js');
                $theme->asset()->usePath()->add('jquery-sparkline', 'libs/jquery-sparkline/jquery-sparkline.js');
                $theme->asset()->usePath()->add('classie', 'libs/nifty-modal/js/classie.js');
                $theme->asset()->usePath()->add('modalEffects', 'libs/nifty-modal/js/modalEffects.js');
                $theme->asset()->usePath()->add('sortable.min', 'libs/sortable/sortable.min.js');
                $theme->asset()->usePath()->add('file-input', 'libs/bootstrap-fileinput/bootstrap.file-input.js');
                $theme->asset()->usePath()->add('bootstrap-select.min', 'libs/bootstrap-select/bootstrap-select.min.js');
                $theme->asset()->usePath()->add('select2.min', 'libs/bootstrap-select2/select2.min.js');
                $theme->asset()->usePath()->add('jquery.magnific-popup.min', 'libs/magnific-popup/jquery.magnific-popup.min.js');
                $theme->asset()->usePath()->add('pace.min', 'libs/pace/pace.min.js');
                $theme->asset()->usePath()->add('bootstrap-datepicker', 'libs/bootstrap-datepicker/js/bootstrap-datepicker.js');
                $theme->asset()->usePath()->add('icheck.min', 'libs/jquery-icheck/icheck.min.js');
                $theme->asset()->usePath()->add('prettify', 'libs/prettify/prettify.js');
                $theme->asset()->usePath()->add('init', 'js/init.js');


            }, 

            'frontend' => function($theme)
            {

                $theme->asset()->usePath()->add('bootstrap.min', 'front/assets/libs/bootstrap/css/bootstrap.min.css');
                $theme->asset()->usePath()->add('pace', 'front/assets/libs/pace/pace.css');
                $theme->asset()->usePath()->add('animate.min', 'front/assets/libs/animate-css/animate.min.css');
                $theme->asset()->usePath()->add('iconmoon', 'front/assets/libs/iconmoon/style.css');
                $theme->asset()->usePath()->add('style', 'front/assets/css/style.css');
      


                $theme->asset()->usePath()->add('less-1.7.5.min', 'front/assets/libs/less-js/less-1.7.5.min.js');
                $theme->asset()->usePath()->add('pace.min', 'front/assets/libs/pace/pace.min.js');
                $theme->asset()->usePath()->add('jquery-1.11.1.min', 'front/assets/libs/jquery/jquery-1.11.1.min.js');
                $theme->asset()->usePath()->add('bootstrap.min', 'front/assets/libs/bootstrap/js/bootstrap.min.js');
                $theme->asset()->usePath()->add('jquery.browser.min', 'front/assets/libs/jquery-browser/jquery.browser.min.js');
                $theme->asset()->usePath()->add('fastclick', 'front/assets/libs/fastclick/fastclick.js');
                $theme->asset()->usePath()->add('jquery.stellar.min', 'front/assets/libs/stellarjs/jquery.stellar.min.js');
                $theme->asset()->usePath()->add('jquery.appear', 'front/assets/libs/jquery-appear/jquery.appear.js');
                $theme->asset()->usePath()->add('init', 'front/assets/js/init.js');

         
            }

        )

    )

);