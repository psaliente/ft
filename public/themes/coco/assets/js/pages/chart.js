$(function () {
    $.ajax({
        url: '../recruitment',
        cache: false,
        type: 'GET',
        dataType: 'json',
    }).done(function (data) {


        $('#chart-bar').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Fastrust Recruitment Report'
            },
            xAxis: {
                categories: [
                    'Applying',
                    'Interview',
                    'Deployed',
                    'Failed',
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Applicants'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    },
                    exportButton: {
                        text: 'Download',
                        // Use only the download related menu items from the default context button
                        menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.splice(2)
                    },
                    printButton: {
                        text: 'Print',
                        onclick: function () {
                            this.print();
                        }
                    }
                }
            },
            series: [{
                name: 'Applicants',
                data: data,
                color: '#539b7e'

            }]
        });
    });
});