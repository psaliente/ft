<!DOCTYPE html>
<html>
    <head>
        <title>{!! Theme::get('title') !!}</title>
        <meta charset="utf-8">
        <meta name="keywords" content="{{! Theme::get('keywords') !!}">
        <meta name="description" content="{{! Theme::get('description') !!}">
        <!-- Plugins stylesheet : optional -->
        {!!  Theme::asset()->container('header')->styles() !!}
        <!--/ Plugins stylesheet : optional -->
        {!! Theme::asset()->styles() !!}
       
    </head>
    <body>
        {!! Theme::content() !!}
    </body>
    {!! Theme::asset()->scripts() !!}
    <!-- Plugins and page level script : optional -->
    {!!  Theme::asset()->container('footer')->scripts() !!}
    <!--/ Plugins and page level script : optional -->
</html>