<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{!! Theme::get('title') !!}</title>  
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />



        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        {!!  Theme::asset()->container('header')->styles() !!}
        <!--/ Plugins stylesheet : optional -->
        

        <!-- Application stylesheet : mandatory -->
        {!! Theme::asset()->styles() !!}
        <!--/ Application stylesheet -->

        <!-- Theme stylesheet : optional -->
        <!--/ Theme stylesheet : optional -->


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="assets/img/favicon.ico">
        <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-touch-icon-152x152.png" />
        <style type="text/css">
        @media print
        {    

            .no-print, .dataTables_length, .dataTables_filter, .dataTables_paginate
            {
                display: none !important;
            }

           
        }

        </style>
    </head>
    <body class="fixed-left ">
        <!-- Begin page -->
    <div id="wrapper">
        
            <!-- Top Bar Start -->
            <div class="topbar no-print">
                <div class="topbar-left">
                    <div class="logo">
                        <h1><a href="#"><img src="{!! Theme::asset()->url('img/logo.png') !!}" alt="Logo"></a></h1>
                    </div>
                    <button class="button-menu-mobile open-left">
                    <i class="fa fa-bars"></i>
                    </button>
                </div>
                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="navbar-collapse2">
                            
                            <ul class="nav navbar-nav navbar-right top-navbar">
                                
                                
                                <li class="dropdown topbar-profile">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                         <strong>{!! Theme::get('name') !!}</strong> <i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">My Profile</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{!! url('app/logout') !!}"><i class="icon-logout-1"></i> Logout</a></li>
                                    </ul>
                                </li>
                                
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
            <!-- Left Sidebar Start -->
            <div class="left side-menu no-print">
                <div class="sidebar-inner slimscrollleft">
                    <div class="clearfix"></div>
                    <hr class="divider" />
                    <!--- Profile -->
                    <div class="profile-info">
                        
                        <div class="col-xs-12">
                            <div class="profile-text">Hi, <b>{!! Theme::get('name') !!}</b></div>
                            <div class="profile-buttons">
                              <a href="{!! url('app/logout') !!}" title="Sign Out"><i class="fa fa-power-off text-red-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--- Divider -->
                    <div class="clearfix"></div>
                    <hr class="divider" />
                    <div class="clearfix"></div>
                    <!--- Divider -->
                    <div id="sidebar-menu">

                        <ul>
                        <li>
                            <a href="{!! url('app') !!}">
                            <i class="fa fa-home"></i>
                            <span>Home</span>
                            </a>
                        </li>
                        @forelse(Auth::user()->roles as $role)
                        <li class="has_sub ">
                            <a href="javascript:void(0);">
                                <i class="{!! $role->module->icon !!}"></i>
                                <span>{!! $role->module->name !!}</span><span class="pull-right"><i class="fa fa-angle-down"></i></span>
                                <span class="arrow"></span>
                            </a>
                            <!-- START 2nd Level Menu -->
                            <ul>
                                     @forelse($role->actions as $action)
                                            <li class="{{ Request::url() == url($action->permalink) ? 'active' : ''   }}">
                                                <a href="{!! url($action->permalink) !!}">
                                                    <span>{!! $action->name !!}</span>
                                                </a>
                                            </li>
                                    @empty
                                    @endforelse   
                            </ul>
                        </li>
                        @empty
                        @endforelse  
                        <li>
                            <a href="{!! url('activity') !!}">
                            <i class="fa fa-list"></i>
                            <span>Audi Trail</span>
                            </a>
                        </li>
                        </ul>                    
                        <div class="clearfix"></div>
                    </div>
                <div class="clearfix"></div>
                
                
            </div>
            
        </div>
        <div class="content-page">
            <!-- ============================================================== -->
            <!-- Start Content here -->
            <!-- ============================================================== -->
            <div class="content">
                <!-- Page Heading Start -->
                <div class="page-heading no-print">
                    <h1>{!! Theme::get('page-title') !!}</h1>         
                </div>
                <!-- Page Heading End-->
                <div class="row">
                    {!! Theme::content() !!}
                </div>

                <!-- Footer Start -->
             
            </div>
        </div>
        
    </div>
    <!-- End of page -->
        <!-- the overlay modal element -->
    <div class="md-overlay"></div>
    <!-- End of eoverlay modal -->   
    <script>
        var resizefunc = [];
    </script>

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Application and vendor script : mandatory -->
        {!! Theme::asset()->scripts() !!}
        
        <!--/ Application and vendor script : mandatory -->

        <!-- Plugins and page level script : optional -->
        {!!  Theme::asset()->container('footer')->scripts() !!}
        <!--/ Plugins and page level script : optional -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
</html>