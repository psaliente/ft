<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>{!! Theme::get('title') !!}</title>  
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">
        <meta name="author" content="Huban Creative">

         <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        {!!  Theme::asset()->container('header')->styles() !!}
        <!--/ Plugins stylesheet : optional -->
        

        <!-- Application stylesheet : mandatory -->
        {!! Theme::asset()->styles() !!}
        <!--/ Application stylesheet -->
        
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="shortcut icon" href="{!! Theme::asset()->url('front/assets/img/favicon.ico') !!}">
        <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-touch-icon-152x152.png" />    
    </head>
<body class="">
    <div id="wrapper">    
    {!! Theme::partial('header') !!}
   
    {!! Theme::content() !!}

    <a class="tothetop" href="javascript:;"><i class="icon-angle-up"></i></a>

    {!! Theme::partial('footer') !!}
    
    </div>

    <script>
        var resizefunc = [];
    </script>
    <style type="text/css">
    .inline-in-label {
        margin-bottom: 0px;
    }
    </style>
    <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
    <!-- Application and vendor script : mandatory -->
    {!! Theme::asset()->scripts() !!}
    
    <!--/ Application and vendor script : mandatory -->

    <!-- Plugins and page level script : optional -->
    {!!  Theme::asset()->container('footer')->scripts() !!}
    <!--/ Plugins and page level script : optional -->
    <!--/ END JAVASCRIPT SECTION -->
    </body>
</html>