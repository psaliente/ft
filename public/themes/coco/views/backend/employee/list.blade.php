<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Applicants</strong></h2>
        </div>
        <div class="widget-content">
            
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>         
                            <tr>
                                <th>Company Name</th>
                                <th>Job</th>
                                <th>Employee Name</th>
                                <th>Date Hired</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{!! $employee->client->name !!}</td>
                                <td>{!! $employee->job->detail->title !!}</td>
                                <td><a href="{!! url('app/employees/employee/view/'.$employee->id) !!}">{!! $employee->user->detail->firstname .' '. $employee->user->detail->lastname !!}</a></td>
                                <td>{!!Carbon::createFromFormat('Y-m-d H:i:s', $employee->created_at)->format('F d, Y')!!}</td>
                                <td>
                                    <?php echo $employee->deleted_at  == null ?  '<span class="label label-success">Active</span>'  :  '<span class="label label-danger">Inactive</span>' ?>
                                </td>
                                <td class="text-center">
                                <div class="btn-group btn-group-xs">
                               
                                <?php if($employee->deleted_at  == null) { ?>
                                <a href="{!! url('app/employee/employee/deactivate/'.$employee->id) !!}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a>
                                <?php }else { ?>
                                <a href="{!! url('app/employee/employee/activate/'.$employee->id) !!}" data-toggle="tooltip" title="Restore"  class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>
                                <?php } ?>
                                </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>   
            </div>
        </div>
    </div>
</div>
       