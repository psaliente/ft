<?php
use Carbon\Carbon as Carbon;
?>
@if(Session::has('message'))
    {!! Session::get('message') !!}
@endif
<div class="col-sm-4 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Client Details</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form class="form-horizontal form-bordered" role="form">                                    
                        <div class="form-group">
                            
                                <label class="col-md-4 col-xs-12 ">Name:</label>
                                <div class="col-md-8 col-xs-12"> 
                                    <p class="text-left">{!! $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname !!}</p>                                             
                                
                                </div>
                            
                        </div>
                        <div class="form-group">
                            
                                <label class="col-md-4 col-xs-12">Contact Number</label>
                                <div class="col-md-8 col-xs-12">
                                    <p class="text-left">{!! $employee->user->applicant->contact ? : 'No record found' !!}</p>
                                </div>
                            
                        </div>     
                        <div class="form-group">
                            
                                <label class="col-md-4 col-xs-12 ">Email</label>
                                <div class="col-md-8 col-xs-12">
                                    <p class="text-left">{!! $employee->user->username ? : 'No record found' !!}</p>
                                </div>
                            
                        </div>
                        <div class="form-group">
                           
                                <label class="col-md-4 col-xs-12 ">Residences</label>
                                <div class="col-md-8 col-xs-12">
                                    <p class="text-left">{!! $employee->user->applicant->address ? : 'No record found' !!}</p>
                                </div>
                            
                        </div>                                          
                        <div class="form-group">
                            
                                <label class="col-md-4 col-xs-12">Birthdate</label>
                                <div class="col-md-8 col-xs-12">
                                    <p class="text-left">{!! $employee->user->applicant->birthdate != null ? Carbon::createFromFormat('Y-m-d', $employee->user->applicant->birthdate)->format('F d, Y') : 'No record found' !!}</p>
                                </div>
                            
                        </div>
                        <div class="form-group">
                            
                                <label class="col-md-4 col-xs-12">Gender</label>
                                <div class="col-md-8">
                                    <p class="text-left">{!! $employee->user->applicant->gender ? : 'No record found' !!}</p>
                                </div>
                            
                        </div>
                        <div class="form-group">
                            
                                <label class="col-md-4 col-xs-12">Civil Status</label>
                                <div class="col-md-8 col-xs-12">
                                    <p class="text-left">{!! $employee->user->applicant->civil_status ? : 'No record found' !!}</p>
                                </div>
                            
                        </div> 
                          
                </form>
			</div>
		</div>
	</div>
	
</div>

<div class="col-sm-4 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Employment Details</h2>
			<div class="additional-btn">
				<a href="{!! url('app/employees/employee/employment/edit/'.$employee->id) !!}" ><i class="fa fa-pencil"></i></a>
			</div>
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form class="form-horizontal" role="form">                                    
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">Employment Type</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->employment->employment_type  ? : 'No record found' !!}</p>
                            </div>
                        
                    </div>                                    
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">Date Hired</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->employment->date_hired !='0000-00-00' ? Carbon::createFromFormat('Y-m-d', $employee->employment->date_hired)->format('F d, Y') : 'No record found' !!}</p>
                            </div>
                        
                    </div>
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">Date Regular</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->employment->date_regular !='0000-00-00' ? Carbon::createFromFormat('Y-m-d', $employee->employment->date_regular)->format('F d, Y') : 'No record found' !!}</p>
                            </div>
                        
                    </div>
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">Date Resigned</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->employment->date_seperated != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $employee->employment->date_seperated)->format('F d, Y') : 'No record found' !!}</p>
                            </div>
                        
                    </div>
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">Contract Started</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->employment->contract_started != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $employee->employment->contract_started)->format('F d, Y') : 'No record found' !!}</p>
                            </div>
                        
                    </div>
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">Contract Ended</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->employment->contract_end != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $employee->employment->contract_end)->format('F d, Y') : 'No record found' !!}</p>
                            </div>
                        
                    </div>
                    
                </form>
			</div>
		</div>
	</div>
	
</div>


<div class="col-sm-4 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Employee Numbers</h2>
			<div class="additional-btn">
				<a href="{!! url('app/employees/employee/number/edit/'.$employee->id) !!}"><i class="fa fa-pencil"></i></a>
			</div>
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				 <form class="form-horizontal" role="form">                                    
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">SSS</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->number->sss ? : 'No record found' !!}</p>
                            </div>
                       
                    </div>
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">TIN</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->number->tin ? : 'No record found'!!}</p>
                            </div>
                        
                    </div>
                    <div class="form-group">
                       
                            <label class="col-md-4 col-xs-12">PhilHealth</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->number->philhealth ? : 'No record found' !!}</p>
                            </div>
                        
                    </div>
                    <div class="form-group">
                        
                            <label class="col-md-4 col-xs-12">PAGIBIG</label>
                            <div class="col-md-8 col-xs-12">
                                <p class="text-left">{!! $employee->number->hdmf ? : 'No record found' !!}</p>
                            </div>
                       
                    </div>                                    
                           
                </form>
			</div>
		</div>
	</div>
	
</div>
<div class="col-md-12">
	<div class="row">
		<!--
		<div class="col-sm-3 portlets">
							
			<div class="widget">
				<div class="widget-header">
					<h2>Leaves</h2>
					<div class="additional-btn">
						<a href="{!! url('app/employee/'. $employee->id .'/leave/add') !!}" ><i class="fa fa-plus"></i></a>
					</div>
				</div>
				<div class="widget-content padding">							
					<div id="basic-form">
						<table class="table table-striped">
		                    <thead>
		                        <tr>
		                            <th>Name</th>
		                            <th>Balance</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    
		                        @foreach($employee->leaves as $leave)
		                        <tr>
		                        <td>{!! $leave->type->name !!}</td>
		                        <td>{!! $leave->balance !!}</td>
		                        <td>
		                            <a href="{!! url('app/employee/leave/edit/'. $leave->id) !!}" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                            <a href="{!! url('app/employee/leave/delete/'.$leave->id) !!}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a>
		                        </td>
		                        </tr>
		                        @endforeach
		                    
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
			
		</div>
		-->


		<div class="col-sm-4 portlets">
							
			<div class="widget">
				<div class="widget-header">
					<h2>Benefits</h2>
					<div class="additional-btn">
						<a href="{!! url('app/employee/'. $employee->id .'/benefit/add') !!}" ><i class="fa fa-plus"></i></a>
					</div>
				</div>
				<div class="widget-content padding">							
					<div id="basic-form">
						 <table class="table table-striped">
		                    <thead>
		                        <tr>
		                            <th>Name</th>
		                            <th>Amount</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    <tbody>
		                    
		                        @foreach($employee->benefits as $benefit)
		                        <tr>
		                        <td>{!! $benefit->name !!}</td>
		                        <td>{!! $benefit->amount !!}</td>
		                        <td>
		                            <a href="{!! url('app/employee/benefit/edit/'. $benefit->id) !!}" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                            <a href="{!! url('app/employee/benefit/delete/'.$benefit->id) !!}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a>
		                           
		                        </td>
		                        </tr>
		                        @endforeach
		                    
		                    </tbody>
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
			
		</div>


		<div class="col-sm-4 portlets">
							
			<div class="widget">
				<div class="widget-header">
					<h2>Deductions</h2>
					<div class="additional-btn">
						<a href="{!! url('app/employee/'. $employee->id .'/deduction/add') !!}" ><i class="fa fa-plus"></i></a>
					</div>
				</div>
				<div class="widget-content padding">							
					<div id="basic-form">
						<table class="table table-striped">
		                    <thead>
		                        <tr>
		                            <th>Name</th>
		                            <th>Amount</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                   
		                    <tbody>
		                        @foreach($employee->deductions as $deduction)
		                        <tr>
		                        <td>{!! $deduction->name !!}</td>
		                        <td>{!! $deduction->amount !!}</td>
		                        <td>
		                            <a href="{!! url('app/employee/deduction/edit/'. $deduction->id) !!}" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                            <a href="{!! url('app/employee/deduction/delete/'.$deduction->id) !!}}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a>
		                        </td>
		                        </tr>
		                        @endforeach
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
			
		</div>


		<div class="col-sm-4 portlets">
							
			<div class="widget">
				<div class="widget-header">
					<h2>Salary</h2>
					<div class="additional-btn">
						<a href="{!! url('app/employee/'. $employee->id .'/salary/add') !!}" ><i class="fa fa-plus"></i></a>
					</div>
				</div>
				<div class="widget-content padding">							
					<div id="basic-form">
						<table class="table table-striped">
		                    <thead>
		                        <tr>
		                            <th>Amount</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    
		                    <tbody>
		                    
		                        @foreach($employee->salaries as $salary)
		                        <tr>
		                        <td>{!! $salary->amount !!}</td>
		                        <td>
		                             <a href="{!! url('app/employee/salary/edit/'. $salary->id) !!}" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
		                            
		                        </td>
		                        </tr>
		                        @endforeach
		                    
		                    </tbody>
		                   
		                </table>
					</div>
				</div>
			</div>
			
		</div>

	</div>
</div>