<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Employment Numbers</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/employees/employee/number/update') !!}" method="POST">
				    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="id" value="{!! $employee->id !!}">
                    <div class="form-group  {!! $errors->has('tin') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="tin">Tin: <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="tin" name="tin" class="form-control" data-mask="999-99-9999" value="{!! $errors->has('tin') ? Input::old('tin') : $employee->number->tin !!}" />
                                @if($errors->has('tin'))
                                        <div class="help-block">{!! $errors->first('tin') !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('sss') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="sss">SSS: <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="sss" name="sss" class="form-control" data-mask="99-9999999-9" value="{!! $errors->has('sss') ? Input::old('sss') : $employee->number->sss !!}"/>
                                @if($errors->has('sss'))
                                    <div class="help-block">{!! $errors->first('sss') !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('philhealth') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="philhealth">Philhealth <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="philhealth" name="philhealth" class="form-control" data-mask="99-999999999-9" value="{!!$errors->has('philhealth') ? Input::old('philhealth') : $employee->number->philhealth !!}"/>
                                @if($errors->has('philhealth'))
                                        <div class="help-block">{!! $errors->first('philhealth') !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('pagibig') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="philhealth">PAGIBIG <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="pagibig" name="pagibig" class="form-control" data-mask="999999999" value="{!!$errors->has('pagibig') ? Input::old('pagibig') : $employee->number->hdmf !!}"/>
                                @if($errors->has('pagibig'))
                                        <div class="help-block">{!! $errors->first('pagibig') !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>