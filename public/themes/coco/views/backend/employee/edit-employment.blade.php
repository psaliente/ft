<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Employment Details</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/employees/employee/employment/save') !!}" method="POST">
				<input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" name="id" value="{!! $employee->id !!}">
                <div class="form-group  {!! $errors->has('employment_type') ? 'has-error': '' !!}">
                    <div class="row">
                        <label class="col-md-6 col-xs-12 control-label">Employment Type <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select id="employment_type" name="employment_type" class="form-control" size="1">
                                <option value="">Please select</option>
                                <option value="Regular" {!!  Input::old('employment_type') == "Regular" || $employee->employment->employment_type == "Regular" ? 'selected' : '' !!}>Regular</option>
                                <option value="Contractual" {!!  Input::old('employment_type') == "Contractual" || $employee->employment->employment_type == "Contractual"  ? 'selected' : '' !!}>Contractual</option>
                            </select>
                            @if($errors->has('employment_type'))
                                    <div class="help-block">{!! $errors->first('employment_type') !!}</div>
                            @endif
                        </div>
                    </div>
                </div>                   
                <div class="form-group {!! $errors->has('date_hired') ? 'has-error': '' !!}">
                    <div class="row">
                        <label class="col-md-6 col-xs-12 control-label">Date Hired</label>
                        <div class="col-md-6">
                            <input type="text" id="date_hired" name="date_hired" class="form-control datepicker-input"  value="{!! $employee->employment->date_hired == '0000-00-00' ? '' :  $employee->employment->date_hired !!}">
                            @if($errors->has('date_hired'))
                                <div class="help-block">{!! $errors->first('date_hired') !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group {!! $errors->has('date_regular') ? 'has-error': '' !!}">
                    <div class="row">
                        <label class="col-md-6 col-xs-12 control-label">Date Regular</label>
                        <div class="col-md-6">
                            <input type="text" id="date_regular" name="date_regular" class="form-control datepicker-input" value="{!! $employee->employment->date_regular == '0000-00-00' ? '' :  $employee->employment->date_regular !!}">
                            @if($errors->has('date_regular'))
                                <div class="help-block">{!! $errors->first('date_regular') !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('date_seperated') ? 'has-error': '' !!}">
                   <div class="row">
                        <label class="col-md-6 col-xs-12 control-label" for="date_seperated">Date Seperated</label>
                        <div class="col-md-6">
                            <input type="text" id="date_seperated" name="date_seperated" class="form-control datepicker-input"  value="{!! $employee->employment->date_seperated == '0000-00-00' ? '' : $employee->employment->date_seperated !!}">
                            @if($errors->has('date_seperated'))
                                <div class="help-block">{!! $errors->first('date_seperated') !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group {!! $errors->has('contract_started') ? 'has-error': '' !!}">
                    <div class="row">
                        <label class="col-md-6 col-xs-12 control-label" for="contract_started">Contract Started</label>
                        <div class="col-md-6">
                            <input type="text" id="contract_started" name="contract_started" class="form-control datepicker-input"  value="{!! $employee->employment->contract_started == '0000-00-00' ? '' : $employee->employment->contract_started !!}">
                            @if($errors->has('contract_started'))
                                <div class="help-block">{!! $errors->first('contract_started') !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('contract_ended') ? 'has-error': '' !!}">
                    <div class="row">
                        <label class="col-md-6 col-xs-12 control-label">Contract Ended</label>
                        <div class="col-md-6">
                            <input type="text" id="contract_ended" name="contract_ended" class="form-control datepicker-input" value="{!! $employee->employment->contract_end == '0000-00-00' ? '' : $employee->employment->contract_end !!}">
                            @if($errors->has('contract_ended'))
                                <div class="help-block">{!! $errors->first('contract_ended') !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>