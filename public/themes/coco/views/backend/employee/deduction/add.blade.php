<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Add Deduction</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/employee/deduction/save')!!}" method="POST">
				    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="user_id" value="{!! $id !!}">
                    
                    <div class="form-group  {!! $errors->has('type') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="name">Name: <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="SSS Contribution" value="{!! Input::old('name') !!}" />
                                    @if($errors->has('name'))
                                        <div class="help-block">{!! $errors->first('name') !!}</div>
                                    @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('amount') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="amount">Amount: <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="amount" name="amount" class="form-control" placeholder="0" value="{!! Input::old('amount') !!}" />
                                @if($errors->has('amount'))
                                    <div class="help-block">{!! $errors->first('amount') !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>