<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Employment Numbers</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/employee/leave/update')!!}" method="POST">
				    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="user_id" value="{!! $id !!}">
                    <input type="hidden" name="id" value="{!! $leave->id !!}">
                    <div class="form-group  {!! $errors->has('type') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="philhealth">Type: <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <select id="supervisor" name="type" class="form-control" size="1">
                                    <option value="">Please select</option>
                                    @foreach($types as $type)
                                        <option value="{!! $type->id !!} " {!! $type->id == $leave->leave_type_id  ? 'selected' : '' !!} >{!! $type->name !!}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('type'))
                                        <div class="help-block">{!! $errors->first('type') !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {!! $errors->has('balance') ? 'has-error': '' !!}">
                        <div class="row">
                            <label class="col-md-6 col-xs-12 control-label" for="philhealth">Balance: <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="tin" name="balance" class="form-control" placeholder="0" value="{!! $leave->balance !!}" />
                        @if($errors->has('type'))
                            <div class="help-block">{!! $errors->first('balance') !!}</div>
                        @endif
                            </div>
                        </div>
                    </div>
        				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>