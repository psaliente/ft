<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>User Details</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/user/save') !!}" method="POST">
				  <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
				  	<div class="form-group {!! $errors->has('firstname') ? 'has-error' : '' !!}">
						<label for="firstname">Firstname</label>
						<input type="text" name="firstname" class="form-control" placeholder="First" value="{!! Input::old('firstname') !!}">
	                    @if($errors->has('firstname'))
	                      <p class="help-block">{!! $errors->first('firstname') !!}</p>
	                    @endif
					 </div>
				  	<div class="form-group {!! $errors->has('lastname') ? 'has-error' : '' !!}">
				  		<label>Lastname:</label>
	                    <input type="text" name="lastname" class="form-control" placeholder="Last" value="{!! Input::old('lastname') !!}">
	                    @if($errors->has('lastname'))
	                      <p class="help-block">{!! $errors->first('lastname') !!}</p>
	                    @endif
				  	</div>
					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
	                    <label >Email:</label>
	                    <input type="text" name="email" class="form-control" placeholder="you@mail.com" value="{!! Input::old('email') !!}">
	                    @if($errors->has('email'))
	                      <p class="help-block">{!! $errors->first('email') !!}</p>
	                    @endif
                	</div>
	                <div class="form-group {!! $errors->has('module') ? 'has-error' : '' !!}">
	                    <label>Module:</label>
	                    <select class="form-control " name="module" >
	                        <option value="" selected="">Select module</option>
	                        @foreach($modules as $module)
	                            <option value="{!!  $module->id !!}">{!! $module->name !!}</option>
	                        @endforeach
	                    </select>
	                    @if($errors->has('module'))
	                      <p class="help-block">{!! $errors->first('module') !!}</p>
	                    @endif                                    
	                </div>
	                <div class="form-group {!! $errors->has('role') ? 'has-error' : '' !!}">
	                    <label>Role:</label>
	                    <select class="form-control" name="role">
	                        <option value=" " selected="">Select role</option>
	                         @foreach($roles as $role)
	                            <option value="{!!  $role->id !!}">{!! $role->name !!}</option>
	                        @endforeach
	                    </select>
	                     @if($errors->has('role'))
	                      <p class="help-block">{!! $errors->first('role') !!}</p>
	                     @endif      
	                </div>
				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>