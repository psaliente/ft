<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>System</strong> Users</h2>
        </div>
        <div class="widget-content">
            <div class="data-table-toolbar">
                <div class="row">
                    <div class="col-md-4">
                        
                    </div>
                    <div class="col-md-8">
                        <div class="toolbar-btn-action">
                            <a href="{!! url('app/user/add') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add new</a>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                            <tr>
                                
                                <th>No.</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th class="no-sort">Status</th>
                                <th class="no-sort text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{!! $user->username !!}</td>
                                    <td>{!! $user->detail->firstname . ' ' . $user->detail->lastname!!}</td>
                                    <td>
                                        @foreach ($user->roles as $role )
                                        {!! $role->module->name . " " . $role->role->name !!}<br/>
                                        @endforeach
                                    </td>
                                    <td>{!! $user->detail->email !!}</td>
                                    <td>
                                    @if($user->deleted_at  == null)
                                     <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">Inactive</span>
                                    @endif
                                    </td>
                                    <td class="text-center">
                                      @if($user->deleted_at  != null)
                                        <div class="btn-group btn-group-xs">
                                            <a href="{!! url('app/user/restore/'.$user->id) !!}" class="btn btn-success"><i class="fa fa-check"></i></a>
                                            </div>
                                      @else
                                        <div class="btn-group btn-group-xs">
                                            <a href="{!! url('app/user/edit/'.$user->id) !!}" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                            <a href="{!! url('app/user/delete/'.$user->id) !!}" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-power-off"></i></a>
                                        </div>
                                      @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
               
            </div>
        </div>
    </div>
</div>
       