<!-- Start info box -->
				<div class="row top-summary">
					<div class="col-lg-3 col-md-6">
						<div class="widget green-1 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="icon-globe-inv"></i>
								</div>
								<div class="text-box">
									<p class="maindata">TOTAL <b>USERS</b></p>
									<h2><span class="animate-number" data-value="{!! $users !!}" data-duration="2000">0</span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget darkblue-2 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="icon-bag"></i>
								</div>
								<div class="text-box">
									<p class="maindata">TOTAL <b>CLIENTS</b></p>
									<h2><span class="animate-number" data-value="{!! $clients !!}" data-duration="2000">0</span></h2>

									<div class="clearfix"></div>
								</div>
							</div>
							
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget orange-4 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="text-box">
									<p class="maindata">TOTAL <b>JOBS</b></p>
									<h2><span class="animate-number" data-value="{!! $jobs !!}" data-duration="2000">0</span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget lightblue-1 animated fadeInDown">
							<div class="widget-content padding">
								<div class="widget-icon">
									<i class="fa fa-users"></i>
								</div>
								<div class="text-box">
									<p class="maindata">TOTAL <b>APPLICANTS</b></p>
									<h2><span class="animate-number" data-value="{!! $applicants !!}" data-duration="2000">0</span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							
						</div>
					</div>

				</div>
				<!-- End of info box -->
       			<div class="row">
					<div class="col-lg-12 portlets">
						<div class="widget">
							<div class="widget-header">
								<h2><i class="icon-chart-line"></i> <strong>Recruitment</strong> Statistics</h2>
							</div>
							<div class="widget-content">
							<div class="chart mt10" id="chart-bar" style="height:250px;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 portlets">
						<div class="widget">
							<div class="widget-header">
								<h2><i class="icon-download"></i>Expoert Database Backup</h2>
							</div>
							<div class="widget-content padding">
							<p>
							<a href="{!! url('backup') !!}" class="btn btn-primary btn-lg">Export</a>
							</p>
							</div>
						</div>
					</div>
				</div>