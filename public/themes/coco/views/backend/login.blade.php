<div class="container">
        <div class="full-content-center">
            <p class="text-center"><a href="#"><img src="{!! Theme::asset()->url('img/login-logo.png') !!}" alt="Logo"></a></p>
            <div class="login-wrap">
                <div class="login-block">
                    
                    <form role="form" action="{!! url('log/user') !!}" method="POST">
                    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                        @if(Session::has('message'))
                            {!!  Session::get('message') !!}
                        @endif 
                        <div class="form-group login-input {!! $errors->has('username') ? 'has-error' : '' !!}">
                            <i class="fa fa-user overlay"></i>
                            <input name="username" type="text" class="form-control text-input" placeholder="Username">
                            @if($errors->has('username'))
                                <p class="help-block">{!! $errors->first('username') !!}</p>
                            @endif
                        </div>
                        <div class="form-group login-input {!! $errors->has('password') ? 'has-error' : '' !!}">
                            <i class="fa fa-key overlay"></i>
                            <input name="password" type="password" class="form-control text-input" placeholder="********">
                            @if($errors->has('password'))
                                <p class="help-block">{!! $errors->first('password') !!}</p>
                            @endif
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                            <button type="submit" class="btn btn-success btn-block">LOGIN</button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    </div>