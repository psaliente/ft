<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Applicants</strong></h2>
        </div>
        <div class="widget-content">
            
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                
                                <th>Done By</th>
                                <th>Action</th>
                                <th>Done to</th>
                                <th>Date Done</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($logs as $log)
                        <tr>
                            <td>{!! $log->done_by !!}</td>
                            <td>{!! $log->action !!}</td>
                            <td>{!! $log->done_to !!}</td>
                            <td>{!! $log->created_at !!}</td>
                        </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
            </div>
            <!--/ panel body  -->     
        </div>
    </div>
</div>