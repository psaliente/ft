<?php
use Carbon\Carbon as Carbon;
?>
<!-- Page Header -->
<!-- Page Header -->
<div class="row">
    <div class="col-md-12">
        <!-- START panel -->
        <div class="panel panel-default">
            <!-- panel heading/header -->
            <div class="panel-heading">
                <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-money no-print"></i></span>Employee Payslip</h3>
                <div class="panel-toolbar text-right">
                    <!-- option -->
                    <div class="option">
                        <button  href="javascript:void(0)" class="btn btn-default no-print" data-toggle="tooltip" title="" data-original-title="Print" onClick="window.print();return false">Print</button>
                    </div>
                    <!--/ option -->
                </div>
            </div>
            <!--/ panel heading/header -->
            <!-- panel body  -->  
            <div class="panel-body">
            <h3 class="text-center">Fastrust Manpower Services.</h3>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <td width="50%">Payroll Period: </td>
            <td width="50%">{!! Theme::get('page-title') !!}</td>
        </tr>
        <tr>
            <td width="50%">Name: </td>
            <td width="50%">{!! $employee->user->detail->lastname . ','.  $employee->user->detail->firstname !!}</td>
        </tr>
        <tr>
            <td width="50%">Monthly  Rate: </td>
            <td width="50%">
                @foreach($employee->salaries as $salary)
                    {!! $salary->amount !!}
                    <?php $basic = $salary->amount / 2; ?>
                @endforeach
            </td>
        </tr>
        </tbody>
        
    </table>
    <table class="table table-bordered">
        <tbody>
        
        <tr>
            <td width="50%">Hours Rendered</td>
            <td width="50%">
            <?php
                    function get_time_difference($time1, $time2) {
                            $time1 = strtotime("1980-01-01 $time1");
                            $time2 = strtotime("1980-01-01 $time2");
                            
                            if ($time2 < $time1) {
                                $time2 += 86400;
                            }
                            
                            return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
                        }
                ?>
                @forelse($timesheets as $timesheet)
                    <?php
                        if($timesheet->time_out != "00:00:00") {

                            $default =  strtotime("16:00:00");
                            $parsedOut = strtotime($timesheet->time_out);
                            
                            if($parsedOut > $default){

                                $out = "16:00:00";

                            }else {

                                $out = $timesheet->time_out;

                            }


                            $dtr = get_time_difference($timesheet->time_in, $out);
                            $lunch = get_time_difference($timesheet->lunch_out, $timesheet->lunch_in);
                            //echo $dtr 
                            //echo $lunch
                            $rendered = get_time_difference($lunch, $dtr);
                            
                            $parsedHours = strtotime($rendered);
                            $default=  strtotime("08:00");
                            

                            if($parsedHours > $default){
                                $total[] = 8;
                            }else {
                                
                                $total[] = str_replace(':', '.', $rendered);
                            }

                            

                        }
                    ?>
                @empty
                @endforelse
                <?php
                        $hours =  array_sum($total);
                        echo $hours
                ?>
            </td>
            <tr>
                <td width="50%">Late</td>
                <td width="50%">
                    <?php
                        $late = 80 - $hours;

                        echo $late;
                    ?>
                </td>
            </tr>
            <tr>
                <td width="50%">Reg Basic</td>
                <td width="50%">{!! $basic  !!}</td>
            </tr>
            <tr>
                <td width="50%">Late Deduction</td>
                <?php
                    $late_deduction = $basic / 80 * $late;
                ?>
                <td width="50%">{!! $late_deduction  !!}</td>
            </tr>

            <tr>
                <td width="50%"><b>Sub Total</b></td>
                <?php
                    $sub_total = $basic - $late_deduction;
                ?>
                <td width="50%"><b/>{!! $sub_total  !!}</td></td>
            </tr>
        </tr>

        </tbody>
        
    </table>
    <p>Benefits</p>
    <table class="table table-bordered">
        <tbody>
        <?php 
            $benefits = [];
        ?>
            @foreach($employee->benefits as $benefit)
                    <tr>
                    <td width="50%">{!! $benefit->name !!}</td>
                    <td width="50%">{!! $benefit->amount !!}</td>
                    </tr>

            <?php
                $benefits[] = $benefit->amount;

                $total_benefits = array_sum($benefits);
            ?>
            @endforeach
            <tr>
            <td width="50%"><b>Total</b></td>
            <td width="50%">{!! $total_benefits !!}</td>
            </tr>
        </tbody>
        
    </table>
    <table class="table table-bordered">
        <tbody>
        
            <tr>
            <td width="50%"><b>Gross Pay</b></td>
            <td width="50%">
                <b>
                    <?php
                        $gross_pay = $sub_total + $total_benefits;

                        echo $gross_pay;
                    ?>  
                </b>
            </td>
            </tr>
        
        </tbody>
        
    </table>
    <p>Deductions</p>
    <table class="table table-bordered">
        <tbody>
        <?php 
            $deductions = [];
        ?>
            @foreach($employee->deductions as $deduction)
                    <tr>
                    <td width="50%">{!! $deduction->name !!}</td>
                    <td width="50%">{!! $deduction->amount !!}</td>
                    </tr>

            <?php
                $deductions[] = $deduction->amount;

                $total_deduction = array_sum($deductions);
            ?>
            @endforeach
            <tr>
            <td width="50%"><b>Total</b></td>
            <td width="50%">{!! $total_deduction !!}</td>
            </tr>
        </tbody>
        
    </table>

    <table class="table table-bordered">
        <tbody>
        
            <tr>
            <td width="50%"><b>Net Pay</b></td>
            <td width="50%">
                <b>
                    <?php
                        $net_pay = $gross_pay - $total_deduction;

                        echo $net_pay;
                    ?>  
                </b>
            </td>
            </tr>
        
        </tbody>
        
    </table>
            </div>
            <!--/ panel body  -->     
        </div>
    </div>
</div>