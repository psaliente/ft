<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Client</strong></h2>
        </div>
        <div class="widget-content">
            
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                       <thead>         
                        <tr>
                            <th>No</th> 
                            <th>Employee Name</th>
                            <th>Job</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($client->employees as $employee)
                        <tr>
                            <td>{!! $employee->id !!}</td>
                            <td><a href="{!! url('app/payroll/view/employee/timesheet/'. $employee->id) !!}">{!! $employee->user->detail->firstname .' '. $employee->user->detail->lastname !!}</a></td>
                            <td>{!! $employee->job->detail->title !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    </table>   
            </div>
        </div>
    </div>
</div>
