


<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Clients</strong></h2>
        </div>
        <div class="widget-content">
            
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                           <th>Company Name</th>
                            <th>Total Employees</th>
    
                           
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)

                        
                            <tr>
                                <td><a href="{!! url('app/payroll/client/'.$client->id) !!}">{!! $client->name !!}</a></td>
                                <td>{!! count($client->employees) !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>   
            </div>
        </div>
    </div>
</div>
