
<div class="row">
	<div class="col-md-6">
        <!-- START panel -->
        <div class="panel panel-default">
            <!-- panel heading/header -->
            <div class="panel-heading">
                <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-clock4"></i></span>Upload Timesheet</h3>
            </div>
            <!--/ panel heading/header -->
            <form role="form" class="form-horizontal" method="POST" action=" {!! url('app/payroll/upload/timesheet') !!}" enctype="multipart/form-data">
		    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
		    <input type="hidden" name="employee" value="{!! $id !!}">
		    <div class="panel-body">
		       @if(Session::has('message'))
	              {!! Session::get('message') !!}
	           @endif	
		        <div class="form-group {!! $errors->has('timesheet') ? 'has-error' : '' !!}">
		        	<label class="col-sm-4 control-label">Timesheet</label>
		            <div class="col-md-8 col-xs-12">
		                <input type="file" name="timesheet" class="btn btn btn-default">
		                @if($errors->has('timesheet'))
		                      <p class="help-block">{!! $errors->first('timesheet') !!}</p>
		                @endif 
		            </div>
		        </div>
		      <button type="submit" class="btn btn-default">Submit</button>
  
		    </div>
		    
		        		    
		</form>            
        </div>
    </div>
    <div class="col-md-6">
        <!-- START panel -->
        <div class="panel panel-default">
            <!-- panel heading/header -->
            <div class="panel-heading">
                <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-clock4"></i></span>View Timesheet</h3>
            </div>
            <!--/ panel heading/header -->
            <form action="{!! url('app/payroll/employee/timesheet') !!}" method="post">
            	<!-- panel body  --> 
	            <div class="panel-body">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<input type="hidden" name="employee" value="{!! $id !!}">

						<div class="form-group {!! $errors->has('from') || $errors->has('to')  ? 'has-error': '' !!}">
							<label class="col-sm-4 control-label">Select Period</label>
	                        <div class="col-sm-8">
	                            <div class="row">
	                                <div class="col-md-6"><input type="text" name="from" class="form-control" id="datepicker-from" placeholder="From (yyyy-mm-dd)" /></div>
	                                <div class="col-md-6"><input type="text" name="to" class="form-control" id="datepicker-to" placeholder="to (yyyy-mm-dd)" /></div>
	                            </div>
	                        </div>
							@if($errors->has('from'))
								<div class="help-block">{!! $errors->first('from') !!}</div>
							@endif
							@if($errors->has('to'))
								<div class="help-block">{!! $errors->first('to') !!}</div>
							@endif
						</div>
						 
							<button type="submit" class="btn btn-default">Go</button>
						
				</div>
				<!--/ panel body  -->
				
			</form> 
        </div>
    </div>
</div>