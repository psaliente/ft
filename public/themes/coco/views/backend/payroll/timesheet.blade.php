<?php
use Carbon\Carbon as Carbon;
?>
<!-- Page Header -->
<div class="page-header page-header-block">
    <div class="page-header-section">
        <h4 class="title semibold">{!! Theme::get('page-title') !!}</h4>
    </div>
    <div class="page-header-section">
        <!-- Toolbar -->
        <div class="toolbar">
            {!! Theme::breadcrumb()->render() !!}
        </div>
        <!--/ Toolbar -->
    </div>
</div>
<!-- Page Header -->
<div class="row">
    <div class="col-md-8">
        <!-- START panel -->
        <div class="panel panel-default">
            <!-- panel heading/header -->
            <div class="panel-heading">
                <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-clock4"></i></span>View Timesheet</h3>

            </div>
            <!--/ panel heading/header -->
 
        	<!-- panel body  --> 
            
            <div class="data-table-toolbar">
                <div class="row">
                    <div class="col-md-4">
                        
                    </div>
                    <div class="col-md-8">
                        <div class="toolbar-btn-action">
                            <a href="{!! url(Request::url().'/payslip') !!}" class="btn btn-success"><i class="ico-print2"></i>Generate Payslip</a>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <table id="general-table" class="table table-striped table-vcenter table-bordered">
            <thead>
            <tr>
                <th class="text-center">Employee</th>
                <th class="text-center">Date</th>
                <th class="text-center">In</th>
                <th class="text-center">Lunch Out</th>
                <th class="text-center">Lunch In</th>
                <th class="text-center">Out</th>
                <th class="text-center">Type</th>
                <th class="text-center">Hours Rendered</th>
            </tr>
            </thead>
            <tbody>
                <?php
                    function get_time_difference($time1, $time2) {
                            $time1 = strtotime("1980-01-01 $time1");
                            $time2 = strtotime("1980-01-01 $time2");
                            
                            if ($time2 < $time1) {
                                $time2 += 86400;
                            }
                            
                            return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
                        }
                ?>
                @forelse($timesheets as $timesheet)
                <tr>
                    <td>{!! $timesheet->employee->user->detail->firstname . ' ' . $timesheet->employee->user->detail->lastname !!}</td>
                    <td>{!! Carbon::createFromFormat('Y-m-d', $timesheet->date)->format('F d, Y')!!}</td>
                    <td>{!! $timesheet->time_in != "00:00:00" ? date('h:i A', strtotime($timesheet->time_in))  : '' !!}</td>
                    <td>{!! $timesheet->lunch_out != "00:00:00" ? date('h:i A', strtotime($timesheet->lunch_out)) : ''  !!}</td>
                    <td>{!! $timesheet->lunch_in != "00:00:00" ? date('h:i A', strtotime($timesheet->lunch_in)) : '' !!}</td>
                    <td>{!! $timesheet->time_out != "00:00:00" ? date('h:i A', strtotime($timesheet->time_out)) : '' !!}</td>
                    <td>{!! $timesheet->day_type !!}</td>
                    <td>
                    <?php

                        
                        if($timesheet->time_out != "00:00:00") {

                            $default =  strtotime("16:00:00");
                            $parsedOut = strtotime($timesheet->time_out);
                            
                            if($parsedOut > $default){

                                $out = "16:00:00";

                            }else {

                                $out = $timesheet->time_out;

                            }


                            $dtr = get_time_difference($timesheet->time_in, $out);
                            $lunch = get_time_difference($timesheet->lunch_out, $timesheet->lunch_in);
                            //echo $dtr 
                            //echo $lunch
                            $rendered = get_time_difference($lunch, $dtr);
                            
                            $parsedHours = strtotime($rendered);
                            $default=  strtotime("08:00");
                            

                            if($parsedHours > $default){
                                $total[] = 8;
                            }else {
                                
                                $total[] = str_replace(':', '.', $rendered);
                            }

                            echo $rendered;

                        }else {
                            echo "00:00:00";
                        }
                    ?>
                    </td>

                </tr>
                @empty
                <tr>
                <td colspan="6" class="text-center">No employee time recorded</td>
                </tr>
                @endif
                <tr>
                    <td colspan="7">TOTAL</td>
                    <td>
                    
                    <?php
                        echo array_sum($total);

                        /*
                        $total_hours = 0;

                        for ($i=0; $i < count($total) ; $i++) { 
                            $total_hours += $total[$i];
                            echo $total_hours . "<br />";
                        }

                        echo  date("H:i",$total_hours);
                        */
                     ?>
                    </td>
                </tr>
            </table>
            
            <!--/ panel body  -->
        </div>
    </div>
</div>