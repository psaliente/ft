<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Job Details</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/recruitment/job/save') !!}" method="POST">
				  <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
				  	<div class="form-group {!! $errors->has('client') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Client:</label>                                     
                            <select class="form-control" name="client">
                                <option value="">Select Client</option>
                                @foreach($clients as $client)
                                <option value="{!! $client->id !!}" {!! $client->id == Input::old('client') ? 'selected=""' : '' !!}>{!! $client->name !!}</option>
                                @endforeach
                            </select>
                             @if($errors->has('client'))
                              <p class="help-block">{!! $errors->first('client') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('category') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Category:</label>
                            <select class="form-control" name="category">
                                <option value="">Select category</option>
                               @foreach($categories as $category)
                                <option value="{!! $category->id !!}" {!! $category->id == Input::old('category') ? 'selected=""' : '' !!}>{!! $category->name !!}</option>
                                @endforeach
                            </select>
                             @if($errors->has('category'))
                              <p class="help-block">{!! $errors->first('category') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Title:</label>                                          
                            <input type="text" class="form-control" name="title" placeholder="Clerk" value="{!! Input::old('title') !!}"/>
                            @if($errors->has('title'))
                              <p class="help-block">{!! $errors->first('title') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('location') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Location</label>                                        
                            <textarea class="form-control" rows="3" name="location">{!! Input::old('location') !!}</textarea>
                            @if($errors->has('location'))
                              <p class="help-block">{!! $errors->first('location') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                                <label class="control-label">Description</label>                                      
                                <textarea class="form-control" rows="3" name="description">{!! Input::old('description') !!}</textarea>
                                @if($errors->has('description'))
                                  <p class="help-block">{!! $errors->first('description') !!}</p>
                                @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('vacancy') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label"> Vacancy:</label>                                          
                            <input type="text" class="form-control" name="vacancy" placeholder="Ex. 20" value="{!! Input::old('vacancy') !!}"/>
                             @if($errors->has('vacancy'))
                              <p class="help-block">{!! $errors->first('vacancy') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('expires_at') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label"> Expires at:</label>                                         
                            <input type="text" class="form-control datepicker-input" name="expires_at" value="{!! Input::old('expires_at') !!}" >
                             @if($errors->has('expires_at'))
                              <p class="help-block">{!! $errors->first('expires_at') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Minimum Qualifications:</label>
                </div>
                <div class="form-group {!! $errors->has('gender') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label"> Gender:</label>                         
                            <select class="form-control" name="gender">
                                <option value="">Select Gender</option>
                                <option value="Male" {!! Input::old('gender') == "Male" ? 'selected=""' : '' !!}>Male</option>
                                <option value="Female" {!! Input::old('gender') == "Female" ? 'selected=""' : '' !!}>Female</option>
                                <option value="Male/Female" {!! Input::old('genders') == "Male/Female" ? 'selected=""' : '' !!}>Both Male and Female</option>
                            </select>
                             @if($errors->has('gender'))
                              <p class="help-block">{!! $errors->first('gender') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>     
                <div class="form-group {!! $errors->has('age') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label"> Age:</label>                                          
                            <input type="text" class="form-control" name="age" placeholder="18-35" value="{!! Input::old('age') !!}"/>
                             @if($errors->has('age'))
                              <p class="help-block">{!! $errors->first('age') !!}</p>
                            @endif
                         </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('experience') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label"> Experience:</label>                                        
                            <input type="text" class="form-control" name="experience" placeholder="1year" value="{!! Input::old('experience') !!}"/>
                             @if($errors->has('experience'))
                              <p class="help-block">{!! $errors->first('experience') !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('education') ? 'has-error' : '' !!}">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="control-label">Education:</label>                                                                                                                                                   
                            <select class="form-control" name="education">
                                <option value="">Select Education</option>
                                
                                <option value="High School Undergrad" {!! Input::old('education') == "High School Undergrad" ? 'selected=""' : '' !!}>High School Undergrad</option>
                                <option value="High School Graduate" {!! Input::old('education') == "High School Graduate" ? 'selected=""' : '' !!}>High School Graduate</option>
                                <option value="College Undergrad" {!! Input::old('education') == "College Undergrad" ? 'selected=""' : '' !!}>College Undergrad</option>
                                <option value="College Graduate" {!! Input::old('education') == "College Graduate" ? 'selected=""' : '' !!}>College Graduate</option>
                            </select>
                            @if($errors->has('education'))
                              <p class="help-block">{!! $errors->first('education') !!}</p>
                            @endif
                        </div>                                             
                    </div>                                            
                </div>
				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>