<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Jobs</strong></h2>
        </div>
        <div class="widget-content">
            <div class="data-table-toolbar">
                <div class="row">
                    <div class="col-md-4">
                        
                    </div>
                    <div class="col-md-8">
                        <div class="toolbar-btn-action">
                            <a href="{!! url('app/recruitment/job/add')  !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add new</a>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive">
                @if(Session::has('message'))
                    {!! Session::get('message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>         
                        <tr>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Employer</th>
                            <th>Location</th>
                            <th>Vacancy</th>
                            <th>Posted at</th>
                            <th>Expires at</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($jobs as $job)
                        <tr>
                            <td>{!! $job->detail->title !!}</td>
                            <td>{!! $job->detail->category->name!!}</td>
                            <td>{!! $job->client->name !!}</td>
                            <td>{!! $job->detail->location !!}</td>
                            <td>{!! $job->detail->vacancy !!}</td>
                            <td>{!! Carbon::createFromFormat('Y-m-d', $job->posted_at)->format('F d, Y') !!}</td>
                            <td>{!! Carbon::createFromFormat('Y-m-d', $job->expires_at)->format('F d, Y') !!}</td>
                            <td>
                             @if($job->deleted_at == NULL)
                                <span class="label label-success label-form">Active</span>
                            @else
                                <span class="label label-danger label-form">Inactive</span>
                            @endif
                            </td>
                            <td>
                            @if($job->deleted_at == NULL)
                                <div class="btn-group btn-group-xs">
                                    <a href="{!! url('app/recruitment/job/edit/'. $job->id) !!}" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="{!! url('app/recruitment/job/delete/'. $job->id) !!}" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-power-off"></i></a>
                                </div>
                            @else
                                <div class="btn-group btn-group-xs">
                                <a href="{!! url('app/recruitment/job/restore/'. $job->id) !!}" class="btn btn-success"><i class="fa fa-check"></i></a>
                                </div>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
       