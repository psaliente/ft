<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Clients</strong></h2>
        </div>
        <div class="widget-content">
            <div class="data-table-toolbar">
                <div class="row">
                    <div class="col-md-4">
                        
                    </div>
                    <div class="col-md-8">
                        <div class="toolbar-btn-action">
                            <a href="{!! url('app/recruitment/client/add')  !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add new</a>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                           <th>Company Name</th>
                            <th>Address</th>
                            <th>Contact Number</th>
                            <th>Email</th>
                            <th>Contact Person</th>
                            <th>Date Added</th>
                            <th class="no-sort">Status</th>
                            <th class="no-sort text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)
                            <tr>
                                <td>{!! $client->name !!}</td>
                                <td>{!! $client->address !!}</td>
                                <td>{!! $client->contact !!}</td>
                                <td>{!! $client->email !!}</td>
                                <td>{!! $client->representative !!}</td>
                                <td>{!! Carbon::createFromFormat('Y-m-d H:i:s', $client->created_at)->format('F d, Y') !!}</td>
                                <td>
                                @if($client->deleted_at == NULL)
                                    <span class="label label-success label-form">Active</span>
                                @else
                                    <span class="label label-danger label-form">Inactive</span>
                                @endif
                                </td>
                                <td>
                                @if($client->deleted_at == NULL)
                                    <div class="btn-group btn-group-xs">
                                        <a href="{!! url('app/recruitment/client/edit/'. $client->id) !!}" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="{!! url('app/recruitment/client/delete/'. $client->id) !!}" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-power-off"></i></a>
                                    </div>
                                @else
                                    <div class="btn-group btn-group-xs">
                                        <a href="{!! url('app/recruitment/client/restore/'. $client->id) !!}" class="btn btn-success"><i class="fa fa-check"></i></a>
                                    </div>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
       