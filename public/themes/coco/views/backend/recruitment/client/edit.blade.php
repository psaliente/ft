<div class="col-sm-6 portlets">
						
	<div class="widget">
		<div class="widget-header">
			<h2>Client Details</h2>
			
		</div>
		<div class="widget-content padding">							
			<div id="basic-form">
				<form action="{!! url('app/recruitment/client/update') !!}" method="POST">
				  	<input name="type" type="hidden" value="update" />
		            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
		            <input type="hidden" name="client_id" value="{!! $client->id !!}">
				  	<div class="form-group {!! $errors->has('company_name') ? 'has-error' : '' !!}">
                        <label>Company Name:</label>                                 
                        <input type="text" class="form-control" name="company_name" placeholder="Virtue Manpower" value="{!! $client->name !!}"/>
                        @if($errors->has('company_name'))
                          <p class="help-block">{!! $errors->first('company_name') !!}</p>
                        @endif
                	</div>
	                <div class="form-group {!! $errors->has('address') ? 'has-error' : '' !!}">
	                    <label>Address</label>                                           
	                  	<textarea class="form-control" rows="5" name="address">{!! $client->address !!}</textarea>
                        @if($errors->has('address'))
                          <p class="help-block">{!! $errors->first('address') !!}</p>
                        @endif	
	                </div>
	             	<div class="form-group {!! $errors->has('contact') ? 'has-error' : '' !!}">
	                    <label>Contact Number:</label>                                         
	                    <input type="text" class="form-control" name="contact" placeholder="000-0000" value="{!! $client->contact !!}"/>
                        @if($errors->has('contact'))
                          <p class="help-block">{!! $errors->first('contact') !!}</p>
                        @endif
	                </div>
	                <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
	                    <label> Email:</label>                            
	                    <input type="text" class="form-control" name="email" placeholder="me@example.com" value="{!! $client->email !!}"/>
                        @if($errors->has('email'))
                        	<p class="help-block">{!! $errors->first('email') !!}</p>
                        @endif
	                </div>
	                <div class="form-group {!! $errors->has('contact_person') ? 'has-error' : '' !!}">
	                    <label>Contact Person:</label>                                                                                                                            
	                    <input type="text" class="form-control" name="contact_person" placeholder="" value="{!! $client->representative !!}"/>  
                        @if($errors->has('contact_person'))
                          <p class="help-block">{!! $errors->first('contact_person') !!}</p>
                        @endif
	                </div>
				  
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</div>