<?php
use Carbon\Carbon as Carbon;
?>
<div class="col-md-12">
    <div class="widget">
        <div class="widget-header">
            <h2><strong>Applicants</strong></h2>
        </div>
        <div class="widget-content">
            
            <br/>
            <div class="table-responsive">
                @if(Session::has('user_message'))
                    {!! Session::get('user_message') !!}   
                @endif
               
               
                <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>         
                            <tr>
                                <th>Company Name</th>
                                <th>Job</th>
                                <th>Applicant</th>
                                <th>Applicant Contact</th>
                                <th>Applicant Email</th>
                                <th>Date Applied</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($applications as $application)
                            <tr>
                                <td>{!! $application->job->client->name !!}</td>
                                <td>{!! $application->job->detail->title !!}</td>
                                <td>{!! $application->user->detail->firstname . ' ' .  $application->user->detail->lastname !!}</td>
                                <td>{!! $application->user->detail->contact!!}</td>
                                <td>{!! $application->user->username !!}</td>
                                <td>{!! Carbon::createFromFormat('Y-m-d H:i:s', $application->created_at)->format('F d, Y') !!}</td>
                                <td>
                                    <?php
                                        switch ($application->status) {
                                            case 'New':
                                                $status = '<span class="label label-info label-form">New</span>'; 
                                                break;
                                            case 'For Interview':
                                                $status = '<span class="label label-primary label-form">For Interview</span>'; 
                                                break;

                                            case 'Passed':
                                                $status = '<span class="label label-success label-form">Passed</span>'; 
                                                break;

                                            case 'Canceled':
                                                $status = '<span class="label label-danger label-form">Canceled</span>'; 
                                                break;
                                        }
                                    ?>
                                    {!! $status !!}
                                </td>
                                <td>
                                <div class="btn-group btn-group-xs">
                                     <a href="{!! url('app/recruitment/applicant/dowload/resume/'. $application->user_id) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Resume"><i class="fa fa-download"></i></a>
                                     <?php
                                        switch ($application->status) {
                                            case 'New':
                                        ?>
                                            <a href="{!! url('app/recruitment/applicant/interview/'. $application->id) !!}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="For Interview"><i class="fa fa-comment"></i></a>
                                            <a href="{!! url('app/recruitment/applicant/cancel/'. $application->id) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                                        <?php
                                            break;
                                            case 'For Interview':
                                        ?>
                                            <a href="{!! url('app/recruitment/applicant/pass/'. $application->id) !!}" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pass"><i class="fa fa-check"></i></a>
                                            <a href="{!! url('app/recruitment/applicant/cancel/'. $application->id) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fail"><i class="fa fa-times"></i></a>
                                        <?php
                                                break;

                                            case 'Passed':
                                                $status = '<span class="label label-success label-form">Passed</span>'; 
                                                break;

                                            case 'Canceled':
                                                $status = '<span class="label label-danger label-form">Canceled</span>'; 
                                                break;
                                        }
                                    ?>
                                </div>
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
       