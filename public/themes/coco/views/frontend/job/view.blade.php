<?php
use Carbon\Carbon as Carbon;
?>
<div class="container">
	<div class="col-md-9">
                @if (Session::has('message'))
                 {!! Session::get('message') !!}
                @endif
                <!-- blog post #1 -->
                <article>
                    <!-- heading -->
                    <h3 class="section-title font-alt mt0">{!! $job->detail->title !!}</h3>
                    <!--/ heading -->

                    <!-- meta -->
                    <p class="meta">
                        <a href="javascript:void(0);">{!! $job->detail->vacancy !!} vacancy</a><!-- comments -->
                        <span class="text-muted mr5 ml5">&#8226;</span>
                        <span class="text-muted">Posted at </span><a href="javascript:void(0);">{!! Carbon::createFromFormat('Y-m-d H:i:s', $job->created_at)->format('F d, Y') !!}</a><!-- category -->
                        <span class="text-muted mr5 ml5">&#8226;</span>
                        <span class="text-muted">By </span><a href="javascript:void(0);">{!! $job->client->name !!}</a><!-- author -->
                    </p>
                    <!--/ meta -->

                    <!-- text -->
                    <p><b>Location:</b></p>
                    <p>{!! $job->detail->location !!}</p>
                    <!--/ text -->
                    <br />
                    <p><b>Description:</b></p>
                    <p>{!! $job->detail->description !!}</p><br />
                    <h4>Qualifications:</h4>
                    <p><b>Gender:</b> <br/> {!! $job->qualification->gender !!}</p>
                    <p><b>Age:</b> <br/> {!! $job->qualification->age !!} yrs. old</p>
                    <p><b>Experience:</b> <br/> {!! $job->qualification->experience !!}</p>
                    <p><b>Education:</b> <br/> {!! $job->qualification->education !!}</p>
                    <!-- button -->
                    @if(Auth::check())
                        <a href="{!! url('job/apply/'.$job->id)!!}" class="btn btn-success">Apply Now</a>
                    @else
                        <a href="{!! url('login') !!}" class="btn btn-success">Apply Now</a>
                    @endif
                    <!--/ button -->
                </article>
                <!--/ blog post #1 -->

                <hr style="margin:60px 0;"><!-- horizontal line -->
                <!-- pager -->
                <!--
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="pager nm">
                            <li class="previous"><a href="javascript:void(0);"><i class="ico-angle-left mr5"></i> Older</a></li>
                            <li class="next"><a href="javascript:void(0);">Newer <i class="ico-angle-right ml5"></i></a></li>
                        </ul>
                    </div>
                </div>
                -->
    </div>       
            <!--/ END Left Section -->

            <!-- START Right Section -->
            <div class="col-md-3">

                <!-- Category -->
                <div class="pt25 mb25">
                    <!-- Title -->
                    <h4 class="section-title font-alt mt0">Category</h4>
                    <!--/ Title -->
                    <ul class="list-unstyled">
                        @forelse($categories as $category)
                           <li class="mb5"><i class="ico-angle-right text-muted mr5"></i> <a href="javascript:void(0);">{!! $category->name !!}</a></li>
                        @empty

                        @endforelse 
                    </ul>
                </div>
                <!--/ Category -->
            </div>
    
</div>