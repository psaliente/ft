<?php
use Carbon\Carbon as Carbon;
?>
<section class="hero-banner bg-success">
    <div class="container text-center">
        
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <h2 class="text-white-1">Available Job Posts</h2><hr>
                <div class="row">
                    <div class="col-sm-12 text-white-1">
                        List of available job posts.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<div class="container">
    <div class="portfolio-container zoom-gallery">

        <div class="col-md-9">
            @forelse($jobs as $job)
                <!-- blog post #1 -->
                <article class="col-md-12">
                    <!-- heading -->
                    <h3 class="section-title font-alt mt0">{!! $job->detail->title !!}</h3>
                    <!--/ heading -->

                    <!-- meta -->
                    <p class="meta">
                        <a href="javascript:void(0);">{!! $job->detail->vacancy !!} vacancy</a><!-- comments -->
                        <span class="text-muted mr5 ml5">&#8226;</span>
                        <span class="text-muted">Posted at </span><a href="javascript:void(0);">{!! Carbon::createFromFormat('Y-m-d H:i:s', $job->created_at)->format('F d, Y') !!}</a><!-- category -->
                        <span class="text-muted mr5 ml5">&#8226;</span>
                        <span class="text-muted">By </span><a href="javascript:void(0);">{!! $job->client->name !!}</a><!-- author -->
                    </p>
                    <!--/ meta -->

                    <!-- text -->
                    <p>{!! $job->detail->location !!}</p>
                    <!--/ text -->

                    <!-- button -->
                    <a href="{!! url('job/view/'.$job->id)!!}" class="btn btn-success">View details&#8230;</a>
                    <!--/ button -->
                     <hr style="margin:20px 0;">
                </article>

                <!--/ blog post #1 -->

                
                @empty
                    <h4>No job posts available as of now. </h4>
                @endforelse
        </div>
        <div class="col-md-3">

                <!-- Category -->
                <div class="pt25 mb25">
                    <!-- Title -->
                    <h4 class="section-title font-alt mt0">Category</h4>
                    <!--/ Title -->
                    <ul class="list-unstyled">
                        @forelse($categories as $category)
                           <li class="mb5"><i class="ico-angle-right text-muted mr5"></i> <a href="javascript:void(0);">{!! $category->name !!}</a></li>
                        @empty

                        @endforelse 
                    </ul>
                </div>
                <!--/ Category -->
            </div>
    </div>
</div>