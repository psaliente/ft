<?php
use Carbon\Carbon as Carbon;
?>
<div class="container"> 
    <!-- Left / Top Side -->
    <div class="col-lg-3 pt15 pb15">
        <h5 class="semibold ellipsis mt0">{!! Theme::get('name') !!}</h5>
        <hr><!-- horizontal line -->
        <!-- tab menu -->
        <ul class="list-group list-group-tabs inverse">
            <li class="list-group-item"><a href="{!! url('home') !!}" ><span class="fa fa-home"></span> Home</a></li>
            <li class="list-group-item"><a href="{!! url('applications') !!}" ><span class="fa fa-briefcase"></span> Applications</a></li>
            <li class="list-group-item"><a href="{!! url('account') !!}" ><span class="fa fa-cog"></span> Account</a></li>
            <li class="list-group-item"><a href="{!! url('logout') !!}"><span class="fa fa-signout"></span> Logoff</a></li>
        </ul>
        <!-- tab menu -->
    </div>
    <!--/ Left / Top Side -->
    <!-- Left / Bottom Side -->
    <div class="col-lg-9">
        <!-- START Tab-content -->
        <div class="tab-content">
            <!-- tab-pane: profile -->
            <div class="tab-pane active" id="profile">
                <!-- form profile -->
                <form class="panel form-horizontal form-bordered" name="form-profile">
                    <div class="panel-body pt0 pb0">
                        <div class="form-group header bgcolor-default">
                            <div class="col-md-12">
                                <h4 class="semibold text-primary">My Account</h4>
                                
                            </div>
                        </div>
                        <div class="pt15 pb15">
                                <p class="text-default">This information appears on your public profile, search results, and beyond.</p>
                                @if (Session::has('message'))
                                    {!! Session::get('message') !!}
                                @endif
                                 
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 ">Name:</label>
                                    <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                         <p class="text-left inline-in-label">{!! Auth::user()->detail->firstname . ' ' . Auth::user()->detail->lastname !!}</p>                                             
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 ">Age:</label>
                                    <div class="col-md-9 col-xs-12">
                                        <?php
                                            $age_segment = explode("-", Auth::user()->applicant->birthdate);

                                            $age = Carbon::createFromDate($age_segment[0],$age_segment[1],$age_segment[2])->age
                                        ?>                                                                                                                                                   
                                         <p class="text-left inline-in-label">{!! Auth::user()->applicant->birthdate != "0000-00-00"? $age .'yrs old' : 'Not yet specified' !!}</p>                                             
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12">Gender:</label>
                                    <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                         <p class="text-left inline-in-label">{!! Auth::user()->applicant->gender ?  : 'Not yet specified' !!}</p>                                             
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12">Civil Status:</label>
                                    <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                         <p class="text-left inline-in-label">{!! Auth::user()->applicant->civil_status ?  : 'Not yet specified' !!}</p>                                             
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12">Weight:</label>
                                    <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                         <p class="text-left inline-in-label">{!! Auth::user()->applicant->weight ?  : 'Not yet specified' !!}kg</p>                                             
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12">Height:</label>
                                    <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                         <p class="text-left inline-in-label">{!! Auth::user()->applicant->height ?  : 'Not yet specified' !!}cm</p>                                             
                                    </div>
                                </div>
                                <div class="form-group header bgcolor-default">
                                    <div class="col-md-12">
                                        <h4 class="semibold text-primary nm">Contact</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12">Email:</label>
                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                             <p class="text-left inline-in-label">{!! Auth::user()->username ?  : 'Not yet specified' !!}</p>                                             
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12">Number:</label>
                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                             <p class="text-left inline-in-label">{!! Auth::user()->detail->contact ?  : 'Not yet specified' !!}</p>                                             
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12">Address:</label>
                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                             <p class="text-left inline-in-label">{!! Auth::user()->applicant->address ?  : 'Not yet specified' !!}</p>                                             
                                        </div>
                                </div>
                                <div class="form-group header bgcolor-default">
                                    <div class="col-md-12">
                                        <h4 class="semibold text-primary nm">Account</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12">Username:</label>
                                    <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                         <p class="text-left inline-in-label">{!! Auth::user()->username ?  : 'Not yet specified' !!}</p>                                             
                                    </div>
                                </div>
                           
                        </div>
                    </div> 
                     <div class="panel-footer">
                        <a href="{!! url('account/edit') !!}" class="btn btn-primary pull-right">Update</a>
                        <div class="clearfix"></div>
                    </div>
                </form>
                <!--/ form profile -->
            </div>
            <!--/ tab-pane: profile -->
        </div>
        <!--/ END Tab-content -->
    </div>
    <!--/ Left / Bottom Side -->
</div>
  