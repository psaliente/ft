<div class="container"> 
    <!-- Left / Top Side -->
    <div class="col-lg-3 pt15 pb15">
        <h5 class="semibold ellipsis mt0">{!! Theme::get('name') !!}</h5>
        <hr><!-- horizontal line -->
        <!-- tab menu -->
        <ul class="list-group list-group-tabs inverse">
            <li class="list-group-item"><a href="{!! url('home') !!}" ><span class="fa fa-home"></span> Home</a></li>
            <li class="list-group-item"><a href="{!! url('applications') !!}" ><span class="fa fa-briefcase"></span> Applications</a></li>
            <li class="list-group-item"><a href="{!! url('account') !!}" ><span class="fa fa-cog"></span> Account</a></li>
            <li class="list-group-item"><a href="{!! url('logout') !!}"><span class="fa fa-signout"></span> Logoff</a></li>
        </ul>
        <!-- tab menu -->
    </div>
    <!--/ Left / Top Side -->
    <!-- Left / Bottom Side -->
    <div class="col-lg-9">
        <!-- START Tab-content -->
        <div class="tab-content">
            <!-- tab-pane: profile -->
            <div class="tab-pane active" id="profile">
                <!-- form profile -->
               
                    <div class="panel-body pt0 pb0">
                        <div class="form-group header bgcolor-default">
                            
                                <h4 class="semibold text-primary mt0 mb5">Home</h4>
                                
                            
                        </div>
                        <div class="pt15 pb15">
                                            <p class="text-default">This information appears on your public profile, search results, and beyond.</p>
                                           
                                            @if (Session::has('message'))
                                              {!! Session::get('message') !!}
                                            @endif
                                            
                                    
                                            <form role="form" method="POST" action="{!! url('profile/update') !!}"   class="form-horizontal">
                                                <input type="hidden" name="from" value="front">
                                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                                <input type="hidden" name="app_id" value="{!! Auth::user()->applicant->id !!}">
                                                <div class="form-group header bgcolor-default">
                                                    <div class="col-md-12">
                                                        <h4 class="semibold text-primary nm">Personal</h4>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group {!! $errors->has('firstname') ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Firstname:</label>
                                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                                             <input type="text" class="form-control" name="firstname" value="{!! Auth::user()->detail->firstname !!}">  
                                                             @if($errors->has('firstname'))
                                                                  <p class="help-block">{!! $errors->first('firstname') !!}</p>
                                                            @endif                                          
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group {!! $errors->has('lastname')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Lastname:</label>
                                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                                             <input type="text" class="form-control" name="lastname" value="{!! Auth::user()->detail->lastname !!}">
                                                             @if($errors->has('firstname'))
                                                                  <p class="help-block">{!! $errors->first('lastname') !!}</p>
                                                            @endif                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('birthdate')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Birthdate:</label>
                                                        <div class="col-md-9 col-xs-12">
                                                            <input type="text" class="form-control datepicker-input" name="birthdate" value="{!! Auth::user()->applicant->birthdate !!}">
                                                            @if($errors->has('birthdate'))
                                                                <p class="help-block">{!! $errors->first('birthdate') !!}</p>
                                                            @endif                                                                                                                                                                                          
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('gender')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Gender:</label>
                                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="gender" id="optionsRadios1" value="Male" {!! Auth::user()->applicant->gender == "Male" ? 'checked=""' : "" !!}>
                                                                    Male
                                                                </label>
                                                                <label>
                                                                    <input type="radio" name="gender" id="optionsRadios1" value="Female" {!! Auth::user()->applicant->gender == "Female" ? 'checked=""' : "" !!}>
                                                                    Female
                                                                </label>
                                                            </div> 
                                                             @if($errors->has('gender'))
                                                                <p class="help-block">{!! $errors->first('gender') !!}</p>
                                                            @endif                                  
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Civil Status:</label>
                                                        <div class="col-md-9 col-xs-12">                                                                                                                                                                                                                                                                                                             
                                                            <select class="form-control select" name="civil_status">
                                                                <option value="Single" {!! Auth::user()->applicant->civil_status == "Single" ? 'selected=""' : '' !!}>Single</option>
                                                                <option value="Married" {!! Auth::user()->applicant->civil_status == "Marriede" ? 'selected=""' : '' !!}>Married</option>
                                                                <option value="Single Parent" {!! Auth::user()->applicant->civil_status == "Single Parent" ? 'selected=""' : '' !!}>Single Parent</option>
                                                                <option value="Widowed" {!! Auth::user()->applicant->civil_status == "Widowed" ? 'selected=""' : '' !!}>Widowed</option>
                                                            </select>                                                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('weight')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Weight (kg):</label>
                                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                                            <input type="text" class="form-control" name="weight" value="{!! Auth::user()->applicant->weight !!}"> 
                                                         @if($errors->has('weight'))
                                                            <p class="help-block">{!! $errors->first('weight') !!}</p>
                                                        @endif  
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('height')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Height (cm):</label>
                                                        <div class="col-md-9 col-xs-12">                                                                                                                                                        
                                                            <input type="text" class="form-control" name="height" value="{!! Auth::user()->applicant->height !!}">
                                                            @if($errors->has('height'))
                                                                <p class="help-block">{!! $errors->first('height') !!}</p>
                                                            @endif                                          
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('contact')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Contact Number:</label>
                                                        <div class="col-md-9 col-xs-12">
                                                            <input type="text" class="form-control" name="contact" value="{!! Auth::user()->detail->contact !!}">
                                                            @if($errors->has('contact'))
                                                                <p class="help-block">{!! $errors->first('contact') !!}</p>
                                                            @endif                                                                                                                                                                  
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('address')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Address:</label>
                                                        <div class="col-md-9 col-xs-12">
                                                             <textarea class="form-control" name="address" rows="5">{!! Auth::user()->applicant->address !!}</textarea>
                                                             @if($errors->has('address'))
                                                                <p class="help-block">{!! $errors->first('address') !!}</p>
                                                            @endif                                                                                                                                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <button type="submit" class="btn btn btn-info pull-right mb15">Save</button>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </form>

                                            <form role="form" class="form-horizontal" method="POST" action=" {!! url('upload/resume') !!}" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                                <input type="hidden" name="id" value="{!! Auth::user()->id !!}">
                                                <div class="form-group header bgcolor-default">
                                                    <div class="col-md-12">
                                                        <h4 class="semibold text-primary nm">Resume</h4>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <p>This will serve as your bio data / resume for initial assesment.</p>
                                                    <div class="form-group {!! $errors->has('resume') ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Bio Data / Resume: </label>
                                                        <div class="col-md-9 col-xs-12">
                                                            <input type="file" name="resume">
                                                            @if($errors->has('resume'))
                                                                  <p class="help-block">{!! $errors->first('resume') !!}</p>
                                                            @endif 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="col-md-3 col-xs-12 control-label">Current Resume: </label>
                                                         <div class="col-md-9 col-xs-12" style="margin-top:7px">
                                                            @if(Auth::user()->applicant->resume )
                                                            <a href="{!! url('download/resume/'.Auth::user()->id) !!}">{!! Auth::user()->applicant->resume !!}</a>
                                                            @else
                                                            <span>No resume saved</span>
                                                            @endif
                                                         </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <button type="submit" class="btn btn btn-info pull-right mb15">Upload</button>
                                                    <div class="clearfix"></div> 
                                                </div>
                                            </form>


                                            <form action="{!! url('account/update') !!}" role="form" class="form-horizontal" method="POST">
                                                <input type="hidden" name="from" value="front">
                                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                                <input type="hidden" name="app_id" value="{!! Auth::user()->applicant->id !!}">
                                                <div class="form-group header bgcolor-default">
                                                    <div class="col-md-12">
                                                        <h4 class="semibold text-primary nm">Account</h4>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group  {!! $errors->has('old_password')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Old Password:</label>
                                                        <div class="col-md-9 col-xs-12">
                                                             <input type="Password" class="form-control" name="old_password" placeholder="********">
                                                            @if($errors->has('old_password'))
                                                                <p class="help-block">{!! $errors->first('old_password') !!}</p>
                                                            @endif                                                                                                                                                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('new_password')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">New Password:</label>
                                                        <div class="col-md-9 col-xs-12">
                                                             <input type="Password" class="form-control" name="new_password" placeholder="********">
                                                             @if($errors->has('new_password'))
                                                                <p class="help-block">{!! $errors->first('new_password') !!}</p>
                                                            @endif                                                                                                                                                                                           
                                                        </div>
                                                    </div>
                                                    <div class="form-group {!! $errors->has('confirm_password')  ? 'has-error' : '' !!}">
                                                        <label class="col-md-3 col-xs-12 control-label">Confirm Password:</label>
                                                        <div class="col-md-9 col-xs-12">
                                                             <input type="Password" class="form-control" name="confirm_password" placeholder="********">
                                                             @if($errors->has('confirm_password'))
                                                                <p class="help-block">{!! $errors->first('confirm_password') !!}</p>
                                                            @endif                                                                                                                                                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    
                                                    <button type="submit" class="btn btn btn-info pull-right">Save</button>
                                                    <div class="clearfix"></div>
                                                    
                                                </div>
                                            </form>
                                        </div>
                        </div>
                    </div> 
                
                <!--/ form profile -->
            </div>
            <!--/ tab-pane: profile -->
        </div>
        <!--/ END Tab-content -->
    </div>
    <!--/ Left / Bottom Side -->
</div>
  