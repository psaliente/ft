<?php
use Carbon\Carbon as Carbon;
?>
<div class="container"> 
    <!-- Left / Top Side -->
    <div class="col-lg-3 pt15 pb15">
        <h5 class="semibold ellipsis mt0">{!! Theme::get('name') !!}</h5>
        <hr><!-- horizontal line -->
        <!-- tab menu -->
        <ul class="list-group list-group-tabs inverse">
            <li class="list-group-item"><a href="{!! url('home') !!}" ><span class="fa fa-home"></span> Home</a></li>
            <li class="list-group-item"><a href="{!! url('applications') !!}" ><span class="fa fa-briefcase"></span> Applications</a></li>
            <li class="list-group-item"><a href="{!! url('account') !!}" ><span class="fa fa-cog"></span> Account</a></li>
            <li class="list-group-item"><a href="{!! url('logout') !!}"><span class="fa fa-signout"></span> Logoff</a></li>
        </ul>
        <!-- tab menu -->
    </div>
    <!--/ Left / Top Side -->
    <!-- Left / Bottom Side -->
    <div class="col-lg-9">
        <!-- START Tab-content -->
        <div class="tab-content">
            <!-- tab-pane: profile -->
            <div class="tab-pane active" id="profile">
                <!-- form profile -->
                <form class="panel form-horizontal form-bordered" name="form-profile">
                    <div class="panel-body pt0 pb0">
                        <div class="form-group header bgcolor-default">
                            <div class="col-md-12">
                                <h4 class="semibold text-primary mt0 mb5">Job Applications</h4>
                                
                            </div>
                        </div>
                         <div class="pt15 pb15">
                                            @if (Session::has('message'))
                                             {!! Session::get('message') !!}
                                            @endif   

                                            <table class="table datatable" id="datatables-1">
                                                <thead>
                                                    <tr>
                                                        <th>Job Post No</th>
                                                        <th>Job Name</th>
                                                        <th>Company</th>
                                                        <th>Status</th>
                                                        <th>Date Applied</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($applications as $application)
                                                    <tr>
                                                        <td>{!! $application->job_id !!}</td>
                                                        <td>{!! $application->job->detail->title !!}</td>
                                                        <td>{!! $application->job->client->name !!}</td>
                                                        <td>{!! $application->status !!}</td>
                                                        <td>{!! Carbon::createFromFormat('Y-m-d H:i:s', $application->created_at)->format('F d, Y') !!}</td>
                                                        <td>
                                                            @if($application->status == "New")
                                                                <a href="{!! url('job/application/cancel/'.$application->id) !!}" class="btn btn-danger btn-xs"><i class="ico-trash"></i></a>  
                                                            @else
                                                                No action available.
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                        </div>
                    </div> 
                </form>
                <!--/ form profile -->
            </div>
            <!--/ tab-pane: profile -->
        </div>
        <!--/ END Tab-content -->
    </div>
    <!--/ Left / Bottom Side -->
</div>
  