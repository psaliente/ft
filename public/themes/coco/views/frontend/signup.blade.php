<section class="main-slider fullsize" data-stellar-background-ratio="0.5" style="background-image: url({!! Theme::asset()->url('front/images/headers/signup-login.jpg') !!} )">
	<div class="slider-caption">
        <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <form class="form-signin" method="POST" action="{!! url('signup') !!}">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <h2 class="form-signin-heading">SIGN UP</h2>

                @if(Session::has('message'))
                    {!! Session::get('message') !!}
                @endif
                <div class="form-group {!! $errors->has('firstname') || $errors->has('lastname')  ? 'has-error' : '' !!}">
                   
                            <input type="text" name="firstname" class="form-control" placeholder="Firstname" value="{!! Input::old('firstname') !!}">
                            @if($errors->has('firstname'))
                                  <p class="help-block">{!! $errors->first('firstname') !!}</p>
                            @endif
                       
                </div>


                <div class="form-group {!! $errors->has('Lastname') || $errors->has('lastname')  ? 'has-error' : '' !!}">
                    
                            <input type="text" name="lastname" class="form-control" placeholder="Lastname"  value="{!! Input::old('lastname') !!}">
                             @if($errors->has('lastname'))
                                  <p class="help-block">{!! $errors->first('lastname') !!}</p>
                            @endif
                       
                </div>

                
                <div class="form-group {!! $errors->has('email') ? 'has-error': '' !!}">
                    
                    <input type="text" class="form-control" name="email" value="{!! Input::old('email') !!}" placeholder="Username / Email">
                       
                    
                    @if($errors->has('email'))
                        <div class="help-block">{!! $errors->first('email') !!}</div>
                    @endif
                </div>
                <div class="form-group {!! $errors->has('password') ? 'has-error': '' !!}">
                    
                        <input type="password" class="form-control" name="password" placeholder="Password">
                       
                    
                    @if($errors->has('password'))
                        <div class="help-block">{!! $errors->first('password') !!}</div>
                    @endif
                </div>
                <div class="form-group {!! $errors->has('confirm_password') ? 'has-error': '' !!}">
                   
              
                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
                       
                    @if($errors->has('password'))
                        <div class="help-block">{!! $errors->first('confirm_password') !!}</div>
                    @endif
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">LOGIN</button><br>
                Already have an account? <a href="signup.html">Login</a> now!
              </form>
            </div>
        </div>
      </div>
      </div>
</section>   