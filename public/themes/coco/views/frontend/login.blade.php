<section class="main-slider fullsize" data-stellar-background-ratio="0.5" style="background-image: url({!! Theme::asset()->url('front/images/headers/signup-login.jpg') !!} )">
	<div class="slider-caption">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <form action=" {!! url('login') !!}" method="POST" class="form-signin">
                    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                    <h2 class="form-signin-heading">LOGIN</h2>
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif
                    <div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
                        
                                <input name="username" type="text" class="form-control input-lg" value="{!! Input::old('username') !!}" placeholder="Userame">
                                @if($errors->has('username'))
                                    <p class="help-block">{!! $errors->first('username') !!}</p>
                                @endif
                            
                    </div>
                    <div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
                       
                                <input name="password" type="password" class="form-control input-lg" placeholder="Password">
                                @if($errors->has('password'))
                                    <p class="help-block">{!! $errors->first('password') !!}</p>
                                @endif
                        
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">LOGIN</button><br>
                    Need an account? <a href="{!! url('signup') !!}">Sign up</a> now!
                  </form>
                </div>
            </div>
          </div>	
      </div>
</section>   