<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timesheet extends Model
{
    use SoftDeletes;

    protected $table = 'time_sheets';


    public function employee()
    {
    	return $this->belongsTo('App\Employee');
    }
}
