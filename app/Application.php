<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    public $table = 'applications';

    protected $dates = ['deleted_at'];

    public function job()
    {
    	return $this->belongsTo('App\Job');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
