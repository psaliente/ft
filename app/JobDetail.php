<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobDetail extends Model
{
    protected $table = 'job_details';

	public $timestamps = false;


	public function category()
	{
		return $this->hasOne('App\JobCategory','id','job_category_id');
	}
}
