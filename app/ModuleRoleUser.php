<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleRoleUser extends Model
{
    public $timestamps = false;

    public $table = 'module_role_users';

    protected $fillable = ['module_role_id', 'user_id'];
}
