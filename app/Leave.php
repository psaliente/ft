<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leave extends Model
{
    use SoftDeletes;

    public function type()
    {
    	 return $this->hasOne('App\LeaveType','id','leave_type_id');
    }
}
