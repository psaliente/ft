<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentNumber extends Model
{
    public $timestamps = false;

    public $table = 'employment_numbers';
}
