<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_details';

	public $timestamps = false;

	protected $fillable = ['firstname', 'lastname','user_slug','profile_photo', 'email','contact'];


	public function scopeOfUserSlug($query, $user_slug)
    {
        return $query->where('user_slug',$user_slug);
    }

    public function user() {

    	return $this->belongsTo('App\User');

    }	
}

