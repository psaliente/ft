<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    public $timestamps = false;

    public $table = 'employments';
}
