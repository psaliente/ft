<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    public $table = 'jobs';


    public function detail() {

    	return $this->hasOne('App\JobDetail');

    }

    public function qualification() {

        return $this->hasOne('App\JobQualification');

    }


    public function client() {

        return $this->belongsTo('App\Client');

    }

    public function scopeOfClientId($query, $client_id)
    {
        return $query->where('client_id',$client_id);
    }
}
