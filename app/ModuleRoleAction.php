<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleRoleAction extends Model
{
    public $timestamps = false;

    public $table = 'module_role_actions';
}
