<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    public $timestamps = false;

    protected $fillable = ['birthdate', 'gender','civil_status','height','weight'];

    public $table = 'applicants';
}
