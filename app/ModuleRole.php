<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleRole extends Model
{

    public $timestamps = false;
	
    
	public function module() {

		return $this->belongsTo('App\Module');
	}


	public function role() {

		return $this->belongsTo('App\Role');
	}


	public function actions() {

		return $this->belongsToMany('App\Action','module_role_actions','module_role_id','action_id');
	}


	public function scopeOfRole($query, $role_id)
    {
        return $query->where('role_id',$role_id);
    }

    public function scopeOfModule($query, $module_id)
    {
        return $query->where('module_id',$module_id);
    }
}
