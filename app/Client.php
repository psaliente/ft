<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    public $table = 'clients';

    protected $dates = ['deleted_at'];

   	public function jobs()
	{
	    return $this->hasMany('App\Job');
	}

	public function employees()
	{
	    return $this->hasMany('App\Employee');
	}
}
