<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveFilling extends Model
{
    use SoftDeletes;

    public $table = "leave_filings";

    public function type()
    {
    	return $this->hasOne('App\LeaveType','id','leave_type_id');
    }

     public function employee()
    {
    	return $this->belongsTo('App\Employee');
    }
}
