<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    public $table = 'employees';


    public function client()
    {
    	return $this->belongsTo('App\Client');
    }

    public function job()
    {
    	return $this->belongsTo('App\Job');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function employment()
    {
    	return $this->hasOne('App\Employment');
    }

    public function number()
    {
    	return $this->hasOne('App\EmploymentNumber');
    }

    public function leaves()
    {

        return $this->hasMany('App\Leave');
    }

    public function benefits()
    {

        return $this->hasMany('App\Benefit');
    }

    public function deductions()
    {

        return $this->hasMany('App\Deduction');
    }

     public function salaries()
    {

        return $this->hasMany('App\Salary');
    }
}
