<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateAccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' =>  'required | min:8',
            'new_password' => 'required | min:8',
            'confirm_password' => 'required | same:new_password',
        ];
    }
    public function messages()
    {
        return [
            'current_password.required' => 'Current Password is required',
            'current_password.min' => 'Current Password must be atleast 8 characters',
            'new_password.required' => 'New Password is required',
            'new_password.min' => 'New Password must be atleast 8 characters',
            'confirm_password.required' => 'Confirm Password is required',
            'confirm_password.same' => 'Confirm Password must be the same as new password.',
            
        ];
    }
}