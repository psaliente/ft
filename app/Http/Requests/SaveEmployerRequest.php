<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveEmployerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        if(Request::input('from') == "front") {
            return [
                'firstname' => 'required | alpha_spaces',
                'lastname' => 'required | alpha_spaces',
                'username' => 'required | email | unique:users',
                'password' => 'required | min:8',
                'confirm_password' => 'required | same:password',
                'company_name' => 'required',
                'contact' => 'required | numeric',
            ];
       } else {
             return [
                'firstname' => 'required | alpha_spaces',
                'lastname' => 'required | alpha_spaces',
                'username' => 'required | email | unique:users',
                'contact' => 'required | numeric',
                'company_name' => 'required',
            ];
        }
    }
    public function messages()
    {
         return [
            'firstname.alpha_spaces' => 'Firstname accepts only letters.',
            'lastname.alpha_spaces' => 'lastname accepts only letters.',
        ];
    }
}
