<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class AddClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'representative' => 'required',
            'address_street' => 'required',
            'address_line' => 'required',
            'address_city' => 'required', 
            'email' => 'required | email',
            'contact' => 'required | integer',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
            return $validator->errors()->all();
    }
}
