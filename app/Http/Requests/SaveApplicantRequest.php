<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveApplicantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required | alpha_spaces',
            'lastname' => 'required | alpha_spaces',
            'middlename' => 'alpha_spaces',
            'email' => 'required | email | unique:users,username',
            'password' => 'required | min:8',
            'confirm_password' => 'required | same:password'
        ];
    }

    public function messages()
    {
        return [
                'firstname.alpha_spaces' => "Firstname accepts only letters.",
                'lastname.alpha_spaces' => "Lastname accepts only letters.",
                'middlename.alpha_spaces' => "Middlename accepts only letters.",
                ];
    }
}
