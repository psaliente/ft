<?php namespace App\Http\Requests;

use App\Http\Requests\Request;


class SaveUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'firstname' => 'required | alpha_spaces',
			'lastname' => 'required | alpha_spaces',
			'email' => 'required | email',
			'module' => 'required',
			'role' => 'required'
		];
	}


	public function messages()
	{
	    return [
	        'firstname.alpha_spaces' => 'Firstname accepts only letters.',
	        'firstname.required' => 'Firstname is required',
	        'lastname.required' => 'Lastname is required',
	        'email.required' => 'Email is required',
	        'module.required' => 'Module role is required.',
	        'role.required' => 'Role is required.',
	    ];
	}


}
