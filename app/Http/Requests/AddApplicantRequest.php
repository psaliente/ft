<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class AddApplicantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required | alpha',
            'lastname' => 'required | alpha',
            'birthdate' => 'required',
            'gender' => 'required',
            'civil_status' => 'required',
            'height' => 'required',
            'weight' => 'required',
            'email' => 'required | email',
            'contact' => 'required'
        ];
    }

    protected function formatErrors(Validator $validator)
    {
            return $validator->errors()->all();
    }
}
