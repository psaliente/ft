<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        if(Request::input('from') == "front")
            return [
                'firstname' => 'required',
                'lastname' => 'required',
                'gender' => 'required',
                'civil_status' => 'required',
                'height' => 'required',
                'weight' => 'required',
                'contact' => 'required | numeric',
                'address' => 'required',
               
            ];
        else {
             return [
                'firstname' => 'required',
                'lastname' => 'required',
                'username' => 'required | email | unique:users',
                'contact' => 'required | numeric',
            ];
        }
    }

}
