<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateEmploymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        switch (Request::input('employment_type')) {
            case 'Regular':
                return [
                    'date_hired' => 'required ',
                    'date_regular' => 'required '
                ];
                break;

            case 'Contractual':
                return [
                    'date_hired' => 'required ',
                    
                    'contract_started' => 'required ',
                    'contract_ended' => 'required '
                ];
                break;

            default:
                return [
                    'employment_type' => 'required',
                    'date_hired' => 'required ',
                ];
                break;
        }


      


      
    }
}
