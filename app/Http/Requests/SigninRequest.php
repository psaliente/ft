<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SigninRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required | min:8'
        ];
    }

    public function remember() {
        return $this->has( 'remember' );
    }
 
    public function credentials() {
        return $this->only( 'username' , 'password' );
    }
}
