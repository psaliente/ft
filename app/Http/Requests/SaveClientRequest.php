<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Request::input('type') == "update") {
            return [
                'company_name' => 'required',
                'address' => 'required',
                'contact' => 'required | numeric',
                'email' => 'required | email',
                'contact_person' => 'required | alpha_spaces',
            ];

        }else{
            return [
                'company_name' => 'required | unique:clients,name',
                'address' => 'required',
                'contact' => 'required | numeric',
                'email' => 'required | email',
                'contact_person' => 'required | alpha_spaces',
            ];
        }
        
    }

    public function messages()
    {
        return [
                'contact_person.alpha_spaces' => "Contac person's name accepts only letters.",
                ];
    }
}
