<?php namespace App\Http\Requests;

use Illuminate\Validation\Validator;

class CustomValidation extends Validator {

    public function validateCheckPhone($attribute, $value, $parameters) {

        if(preg_match("/[0-9]/", $value))
        {
            return true;
        }

        return false;
    }
    
    //This allows alpha characters and spaces as the "alpha" validation does not allow spaces
    //It matches unicode characters, João Gabriel won't be marked as invalid 
    public function validateAlphaSpaces($attribute, $value, $parameters) {

        return preg_match('/^[\pL\s]+$/u', $value);

    }

}