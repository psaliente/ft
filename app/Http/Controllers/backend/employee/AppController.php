<?php namespace App\Http\Controllers\Backend\Employee;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Session;
use Carbon\Carbon as Carbon;
use Redirect;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\UpdateEmploymentRequest;
use App\Http\Requests\UpdateEmploymentNumberRequest;
use App\Http\Requests\SaveLeaveRequest;
use App\Http\Requests\SaveBenefitRequest;
use App\Http\Requests\SaveSalaryRequest;
use App\Http\Requests\SaveMyLeaveRequest;

/*
|--------------
| Models
|--------------
*/
use App\Employee;
use App\Employment;
use App\EmploymentNumber;
use App\LeaveType;
use App\Leave;
use App\Benefit;
use App\Deduction;
use App\Salary;
use App\LeaveFiling;
use App\Timesheet;

class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller - Backend
	|--------------------------------------------------------------------------
	|
	| This controller works on backend of the application.
	| All viewable pages by authenticated users.
	|
	*/

	protected $data = [];
	protected $date;
	protected $user;


	public function __construct(Carbon $carbon)
	{

		$this->middleware('auth');
		$this->middleware('backend');
		//$this->middleware('department');

		$this->date = $carbon;

		$this->theme = Theme::uses('coco')->layout('backend');

		if(Auth::check()){

			$this->user = Auth::user();
			
		}
		
	}


	public function listEmployee()
	{
		$this->theme->appendTitle(' | All Employees');
		$this->theme->set('page-title', 'All Employees');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);
		
		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');

		$employees = Employee::withTrashed()->get();

		$this->data = ['employees' => $employees];
		
		return $this->theme->scope('backend.employee.list', $this->data)->render();
	}

	public function employee($id)
	{
		$employee = Employee::find($id);

		$this->theme->appendTitle(' | ' .  $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname  );
		$this->theme->set('page-title',  $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname );
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

	
		$this->data = [ 'employee' => $employee];


		return $this->theme->scope('backend.employee.profile', $this->data)->render();
	}

	public function editEmployment($id)
	{

		$this->theme->asset()->container('header')->usePath()->add('jquery-ui-css', 'plugins/jquery-ui/css/jquery-ui.css');

		$this->theme->asset()->container('footer')->usePath()->add('jquery-ui', 'plugins/jquery-ui/js/jquery-ui.js');
		$this->theme->asset()->container('footer')->usePath()->add('employment', 'js/backend/pages/employment.js');

		$employee = Employee::find($id);
		
		$this->theme->appendTitle(' | Edit Employment Details');
		$this->theme->set('page-title', 'Employment Details | ' . $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname);
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);



		$this->data = ['employee' => $employee];

		return $this->theme->scope('backend.employee.edit-employment', $this->data)->render();
	}

	public function updateEmployment(UpdateEmploymentRequest $request)
	{

		$employment = [
						'employment_type' => $request->employment_type,
						'date_hired' => $request->date_hired,
						'date_regular' => $request->date_regular,
						'date_seperated' => $request->date_seperated,
						'contract_started' => $request->contract_started,
						'contract_end' => $request->contract_ended
					  ];
		
		Employment::where('employee_id',$request->id)->update($employment);

		$employee = Employee::find($request->id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit Employment Details',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);
	

		$message = '<div class="alert alert-success" role="alert">Employment details in now updated.</div>';

		return Redirect::to('app/employees/employee/view/'.$request->id)->with('message',$message);
	}

	public function editNumber($id)
	{
			
		$employee = Employee::find($id);

		$this->theme->appendTitle(' | Edit Employee Numbers');
		$this->theme->set('page-title', 'Edit Employee Numbers | ' . $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname);
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		
		$this->theme->asset()->container('footer')->usePath()->add('plugins', 'plugins/inputmask/js/inputmask.js');
		$this->theme->asset()->container('footer')->usePath()->add('employment', 'js/backend/pages/numbers.js');

		
		$data = [
					'employee' => $employee
				];

		return $this->theme->scope('backend.employee.edit-number', $data)->render();
	}

	public function updateNumber(UpdateEmploymentNumberRequest $request)
	{
		$numbers = [
						'tin' => $request->tin,
						'sss' => $request->sss,
						'philhealth' => $request->philhealth,
						'hdmf' => $request->pagibig
					  ];
		
		EmploymentNumber::where('employee_id',$request->id)->update($numbers);

		$employee = Employee::find($request->id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit Employment Numbers',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);
	

		$message = '<div class="alert alert-success" role="alert">Employment IDs in now updated.</div>';

		return Redirect::to('app/employees/employee/view/'.$request->id)->with('message',$message);
	}


	public function addLeave($id)
	{
		$this->theme->appendTitle(' | Add Employee Leave');
		$this->theme->set('page-title', 'Add Employee Leave');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$types = LeaveType::all();

		$this->data = ['types' => $types, 'id' => $id ];


		return $this->theme->scope('backend.employee.leave.add', $this->data)->render();
	}

	public function saveLeave(SaveLeaveRequest $request)
	{
		$leave = new Leave();
		$leave->leave_type_id = $request->type;
		$leave->employee_id =$request->user_id;
		$leave->balance = $request->balance;
		$leave->created_at = $this->date->now();

		$leave->save();

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee Leave was successfully saved.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function editLeave($id)
	{
		

		$this->theme->appendTitle(' | Edit Employee Leave');
		$this->theme->set('page-title', 'Edit Employee Leave');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$leave = Leave::find($id);

		$types = LeaveType::all();

		$this->data = ['types' => $types, 'id' => $leave->employee_id , 'leave' => $leave ];


		return $this->theme->scope('backend.employee.leave.edit', $this->data)->render();
	}



	public function updateLeave(SaveLeaveRequest $request)
	{

		$leave = Leave::find($request->id);
		$leave->leave_type_id = $request->type;
		$leave->balance = $request->balance;

		$leave->push();

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee Leave was successfully updated.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function deleteLeave($id)
	{

		$leave = Leave::find($id);
		$user = $leave->employee_id;
		$leave->delete();

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee Leave was successfully deleted.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$user)->with('message', $message);
	}


	public function addBenefit($id)
	{
		$this->theme->appendTitle(' | Add Employee Benefits');
		$this->theme->set('page-title', 'Add Employee benefits');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		//$types = LeaveType::all();

		$this->data = ['id' => $id ];


		return $this->theme->scope('backend.employee.benefit.add', $this->data)->render();
	}


	public function saveBenefit(SaveBenefitRequest $request)
	{
		$benefit = new Benefit();
		$benefit->employee_id =$request->user_id;
		$benefit->name = $request->name;
		$benefit->amount = $request->amount;
		$benefit->created_at = $this->date->now();

		$benefit->save();

		$employee = Employee::find($request->user_id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Add Benefits',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee benefit was successfully saved.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function editBenefit($id)
	{
		$this->theme->appendTitle(' | Edit Employee Benefits');
		$this->theme->set('page-title', 'Edit Employee Benefits');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$benefit = Benefit::find($id);

		$this->data = ['id' => $benefit->employee_id, 'benefit' => $benefit ];


		return $this->theme->scope('backend.employee.benefit.edit', $this->data)->render();
	}


	public function updateBenefit(SaveBenefitRequest $request)
	{
		$benefit = Benefit::find($request->id);
		$benefit->name = $request->name;
		$benefit->amount = $request->amount;

		$benefit->push();

		$employee = Employee::find($request->user_id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit Benefit',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee benefit was successfully updated.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function deleteBenefit($id)
	{

		$benefit = Benefit::find($id);
		$user = $benefit->employee_id;

		$employee = Employee::find($benefit->employee_id);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Delete Benefit',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$benefit->delete();

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee benefit was successfully deleted.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$user)->with('message', $message);
	}


	public function addDeduction($id)
	{
		$this->theme->appendTitle(' | Add Employee Deduction');
		$this->theme->set('page-title', 'Add Employee Deduction');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		//$types = LeaveType::all();

		$this->data = ['id' => $id ];


		return $this->theme->scope('backend.employee.deduction.add', $this->data)->render();
	}


	public function saveDeduction(SaveBenefitRequest $request)
	{
		$deduction = new Deduction();
		$deduction->employee_id =$request->user_id;
		$deduction->name = $request->name;
		$deduction->amount = $request->amount;
		$deduction->status = "active";
		$deduction->created_at = $this->date->now();

		$deduction->save();

		$employee = Employee::find($request->user_id);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Add Deduction',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee deduction was successfully saved.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function editDeduction($id)
	{
		$this->theme->appendTitle(' | Edit Employee Deduction');
		$this->theme->set('page-title', 'Edit Employee Deduction');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$deduction = Deduction::find($id);


		$this->data = ['id' => $deduction->employee_id, 'deduction' => $deduction ];


		return $this->theme->scope('backend.employee.deduction.edit', $this->data)->render();
	}


	public function updateDeduction(SaveBenefitRequest $request)
	{
		$deduction = Deduction::find($request->id);
		$deduction->name = $request->name;
		$deduction->amount = $request->amount;
		$deduction->status = "active";

		$deduction->push();

		$employee = Employee::find($request->id);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit Deduction',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee deduction was successfully updated.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function deleteDeduction($id)
	{

		$deduction = Deduction::find($id);
		$user = $deduction ->employee_id;
		$deduction ->delete();

		$employee = Employee::find($user);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Delete Deduction',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee deduction was successfully deleted.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$user)->with('message', $message);
	}



	public function addSalary($id)
	{
		$this->theme->appendTitle(' | Add Employee Salary');
		$this->theme->set('page-title', 'Add Employee Salary');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);


		$this->data = ['id' => $id];


		return $this->theme->scope('backend.employee.salary.add', $this->data)->render();
	}



	public function saveSalary(SaveSalaryRequest $request)
	{
		$deduction = new Salary();
		$deduction->employee_id =$request->user_id;
		$deduction->amount = $request->amount;
		$deduction->is_active = "active";
		$deduction->created_at = $this->date->now();

		$deduction->save();

		$employee = Employee::find($request->user_id);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Add Salary',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee salary was successfully saved.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}


	public function editSalary($id)
	{
		$this->theme->appendTitle(' | Edit Employee Salary');
		$this->theme->set('page-title', 'Edit Employee Salary');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$salary = Salary::where('id', $id)->where('is_active','active')->where('deleted_at', NULL)->first();


		$this->data = ['id' => $salary->employee_id, 'salary' => $salary];


		return $this->theme->scope('backend.employee.salary.edit', $this->data)->render();
	}


	public function updateSalary(SaveSalaryRequest $request)
	{
		$salary = Salary::find($request->id);
		$salary->amount = $request->amount;

		$salary->push();

		$employee = Employee::find($request->user_id);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit Salary',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);


		$message = "<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
							Employee salary was successfully updated.
						</div>";

		return Redirect::intended('app/employees/employee/view/'.$request->user_id)->with('message', $message);
	}
}