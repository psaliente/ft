<?php namespace App\Http\Controllers\Backend\Payroll;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Session;
use Carbon\Carbon as Carbon;
use Response;
use Redirect;
use Excel;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\ViewTimesheetRequest;
/*
|--------------
| Models
|--------------
*/
use App\User;
use App\Client;
use App\Timesheet;
use App\Employee;

class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller - Backend
	|--------------------------------------------------------------------------
	|
	| This controller works on backend of the application.
	| All viewable pages by authenticated users.
	|
	*/

	protected $data = [];
	protected $date;
	protected $user;


	/**
	 * Create a new controller instance.
	 *
	 * @return voids
	 */
	public function __construct(Carbon $carbon)
	{

		$this->middleware('auth');
		$this->middleware('backend');

		$this->date = $carbon;

		$this->theme = Theme::uses('coco')->layout('backend');

		if(Auth::check()){

			$this->user = Auth::user();
			
		}
		
	}


	/**
	 * Show the backend dashboard page to the user.
	 *
	 * @return Response
	 */
	public function clients()
	{
		
		$this->theme->appendTitle(' | Payroll Clients');
		$this->theme->set('page-title', 'Payroll Clients');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');


		$clients = Client::withTrashed()->get();

		$this->data = [ 'clients' => $clients ];

		return $this->theme->scope('backend.payroll.clients', $this->data)->render();
	}

	public function client($id)
	{
		$client = Client::find($id);
		
		$this->theme->appendTitle(' | ' . $client->name);
		$this->theme->set('page-title', $client->name);
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');

		$this->data = [ 'client' => $client ];

		return $this->theme->scope('backend.payroll.client', $this->data)->render();
	}


	public function uploadTimesheet(Request $request)
	{
		$id = $request->employee;
		

		$file = $request->file('timesheet');

		$extension =  $file->getClientOriginalExtension();

		$name = $file->getClientOriginalName();

		$filename = $name.$extension; 
		$path =  storage_path();


		$file->move($path, $filename);

		$get_file =  $path . '/'. $filename;

		 Excel::load($get_file, function($reader) use($id) {

			$reader->each(function($sheet) use($id) {

			    // Loop through all rows
			    $sheet->each(function($row) use($id) {
			    	
			    	
			    		$timesheet = new Timesheet();
				    	$timesheet->employee_id = $id;
				    	$timesheet->time_in = $row->time_in;
				    	$timesheet->time_out = $row->time_out;
				    	$timesheet->lunch_in = $row->lunch_in;
				    	$timesheet->lunch_out = $row->lunch_out;
				    	$timesheet->date = $row->date->format('Y-m-d');
				    	$timesheet->day_type = $row->day_type;
				    	$timesheet->status = $row->status;
				    	$timesheet->created_at = $row->created_at;
				    	$timesheet->updated_at = $row->updated_at;
				    	$timesheet->deleted_at = $row->deleted_at;
				    	$timesheet->save();
			    	
			    });

			});
		 });


		$employee = Employee::find($id);
		
		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Upload Employee Timesheet',
					'done_to' => $employee->user->detail->firstname . ' ' . $employee->user->detail->lastname,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);


		 $message = "<div class='alert alert-success'>Timesheet has been uploaded. View it now on timesheet.</div>";
			
		return Redirect::to('app/payroll/view/employee/timesheet/'.$id)->with('message', $message);
	}




	public function processTimesheet($id)
	{
		$this->theme->asset()->container('header')->usePath()->add('jquery-ui-css', 'plugins/jquery-ui/css/jquery-ui.css');

		$this->theme->asset()->container('footer')->usePath()->add('jquery-ui', 'plugins/jquery-ui/js/jquery-ui.js');
		$this->theme->asset()->container('footer')->usePath()->add('timesheet', 'js/backend/pages/timesheet.js');
		$this->data = [ 'id' => $id ];

		return $this->theme->scope('backend.payroll.process', $this->data)->render();
	}

	public function getEmployeeTimesheet(ViewTimesheetRequest $request)
	{
		$user = $request->employee;
		$from = $request->from;
		$to = $request->to;


		return Redirect::intended('app/payroll/employee/'. $user .'/timesheet/'. $from . '/'. $to );
	}


	public function viewEmployeeTimesheet($id, $from, $to)
	{
		$duration_from = $this->date->createFromFormat('Y-m-d', $from)->format('F d, Y');
		$duration_to = $this->date->createFromFormat('Y-m-d', $to)->format('F d, Y');
		

		
		$this->theme->appendTitle(' | Employee Timesheet');
		$this->theme->set('page-title', 'From ' . $duration_from . ' to ' . $duration_to );
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$timesheets = Timesheet::where('employee_id', $id)->whereBetween('date', array($from, $to))->orderBy('date','desc')->get();

		//dd($timesheets);

		$this->data = ['timesheets' => $timesheets];

		return $this->theme->scope('backend.payroll.timesheet', $this->data)->render();
	}

	public function viewEmployeePayslip($id, $from, $to)
	{
		$duration_from = $this->date->createFromFormat('Y-m-d', $from)->format('F d, Y');
		$duration_to = $this->date->createFromFormat('Y-m-d', $to)->format('F d, Y');
		

		
		$this->theme->appendTitle(' | Employee Payslip');
		$this->theme->set('page-title', 'From ' . $duration_from . ' to ' . $duration_to );
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$employee = Employee::find($id);

		$timesheets = Timesheet::where('employee_id', $id)->whereBetween('date', array($from, $to))->orderBy('date','desc')->get();

		//dd($timesheets);

		$this->data = ['timesheets' => $timesheets, 'employee' => $employee];

		return $this->theme->scope('backend.payroll.payslip', $this->data)->render();
	}
	
}