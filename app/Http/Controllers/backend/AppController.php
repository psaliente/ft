<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Carbon\Carbon as Carbon;
use Redirect;
use Excel;
/*
|--------------
| Requests
|--------------
*/

/*
|--------------
| Models
|--------------
*/
use App\User;
use App\UserDetail;
use App\Module;
use App\Role;
use App\ModuleRole;
use App\ModuleRoleUser;
use App\Action;
use App\ModuleRoleAction;
use App\Client;
use App\Job;
use App\JobCategory;
use App\JobDetail;
use App\Applicant;
use App\JobQualification;
use App\Application;
use App\Employee;
use App\Employment;
use App\EmploymentNumber;
use App\Benefit;
use App\Salary;
use App\Deduction;
use App\Timesheet;
use App\Trail;



class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller - Backend
	|--------------------------------------------------------------------------
	|
	| This controller works on backend of the application.
	| All viewable pages by authenticated users.
	|
	*/
	protected $user;
	protected $date;
	protected $data = [];


	/**
	 * Create a new controller instance.
	 *
	 * @return voids
	 */
	public function __construct(Carbon $carbon)
	{

		$this->middleware('auth');
		$this->middleware('backend');

		$this->date = $carbon;

		$this->theme = Theme::uses('coco')->layout('backend');

		if(Auth::check()){

			$this->user = Auth::user();
			
		}
	
	}


	public function index()
	{
		$this->theme->appendTitle(' | Home');
		$this->theme->set('page-title', 'Home');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('user_slug', $this->user->detail->user_slug);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$this->theme->asset()->container('footer')->usePath()->add('highcharts', 'js/charts/highcharts.js');
		$this->theme->asset()->container('footer')->usePath()->add('exporting', 'js/charts/modules/exporting.js');
		$this->theme->asset()->container('footer')->usePath()->add('chart', 'js/pages/chart.js');

		
		$users = User::ofUserType(3)->count();

		$clients = Client::count();

		$jobs = Job::count();

		$applicants = Applicant::count();

		$this->data = [
							'users' => $users,
							'clients' => $clients,
							'jobs' => $jobs,
							'applicants' => $applicants,
					  ];


		return $this->theme->scope('backend.index', $this->data)->render();
	}


	public function activity()
	{
		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');

		$this->theme->appendTitle(' | Audit Trail');
		$this->theme->set('page-title', 'Audit Trail');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('user_slug', $this->user->detail->user_slug);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$logs = Trail::all();

		$this->data = [ 'logs' => $logs];

		return $this->theme->scope('backend.activity',$this->data)->render();
	}

	public function backup()
	{
		$users = User::withTrashed()->get();
		$users = $users->toArray();

		$user_details = UserDetail::get();
		$user_details = $user_details->toArray();

		$modules = Module::withTrashed()->get();
		$modules = $modules->toArray();

		$roles = Role::withTrashed()->get();
		$roles = $roles->toArray();

		$module_roles = ModuleRole::all();
		$module_roles = $module_roles->toArray();

		$module_role_users = ModuleRoleUser::all();
		$module_role_users = $module_role_users->toArray();

		$actions = Action::withTrashed()->get();
		$actions = $actions->toArray();


		$module_role_actions = ModuleRoleAction::all();
		$module_role_actions = $module_role_actions->toArray();


		$clients = Client::withTrashed()->get();
		$clients = $clients->toArray();

		$jobs = Job::withTrashed()->get();
		$jobs = $jobs->toArray();


		$job_categories = JobCategory::withTrashed()->get();
		$job_categories = $job_categories->toArray();

		$job_details = JobDetail::all();
		$job_details = $job_details->toArray();

		$applicants = Applicant::all();
		$applicants = $applicants->toArray();

		$job_qualifications = JobQualification::all();
		$job_qualifications = $job_qualifications->toArray();


		$applications = Application::all();
		$applications = $applications->toArray();

		$employees = Employee::all();
		$employees = $employees->toArray();


		$employments = Employment::all();
		$employments = $employments->toArray();


		$employment_numbers = EmploymentNumber::all();
		$employment_numbers = $employment_numbers->toArray();


		$benefits = Benefit::withTrashed()->get();
		$benefits = $benefits->toArray();


		$salary = Salary::withTrashed()->get();
		$salary = $salary->toArray();


		$deductions = Deduction::withTrashed()->get();
		$deductions = $deductions->toArray();

		$timesheets = Timesheet::withTrashed()->get();
		$timesheets = $timesheets->toArray();

		$trails = Trail::all();
		$trails = $trails->toArray();

		Excel::create('database_backup_'.date('d_m_Y'), function($excel) use($users, $user_details, $modules, $roles, $module_roles, $module_role_users, $actions, $module_role_actions, $clients, $jobs, $job_categories, $job_details, $applicants, $job_qualifications, $applications, $employees, $employments, $employment_numbers, $benefits, $salary, $deductions, $timesheets,  $trails) {


			
		    $excel->sheet('users', function($sheet) use($users) {

		    	$sheet->fromArray($users, NULL, 'A1', true);

		    });

		   
		    $excel->sheet('user_details', function($sheet) use($user_details) {
		    	$sheet->fromArray($user_details, NULL, 'A1', true);
		    });


		    $excel->sheet('modules', function($sheet) use($modules) {
		    	$sheet->fromArray($modules, NULL, 'A1', true);
		    });

		    $excel->sheet('roles', function($sheet) use($roles) {
		    	$sheet->fromArray($roles, NULL, 'A1', true);
		    });

		    $excel->sheet('module_roles', function($sheet) use($module_roles) {
		    	$sheet->fromArray($module_roles, NULL, 'A1', true);
		    });

		    $excel->sheet('module_role_users', function($sheet) use($module_role_users) {
		    	$sheet->fromArray($module_role_users, NULL, 'A1', true);
		    });


		    $excel->sheet('actions', function($sheet) use($actions) {
		    	$sheet->fromArray($actions, NULL, 'A1', true);
		    });

		    $excel->sheet('module_role_actions', function($sheet) use($module_role_actions) {
		    	$sheet->fromArray($module_role_actions, NULL, 'A1', true);
		    });


		    $excel->sheet('clients', function($sheet) use($clients) {
		    	$sheet->fromArray($clients, NULL, 'A1', true);
		    });


		    $excel->sheet('jobs', function($sheet) use($jobs) {
		    	$sheet->fromArray($jobs, NULL, 'A1', true);
		    });

		    $excel->sheet('job_categories', function($sheet) use($job_categories) {
		    	$sheet->fromArray($job_categories, NULL, 'A1', true);
		    });

		    $excel->sheet('job_details', function($sheet) use($job_details) {
		    	$sheet->fromArray($job_details, NULL, 'A1', true);
		    });

		    $excel->sheet('applicants', function($sheet) use($applicants) {
		    	$sheet->fromArray($applicants, NULL, 'A1', true);
		    });
		   	
		    $excel->sheet('job_qualifications', function($sheet) use($job_qualifications) {
		    	$sheet->fromArray($job_qualifications, NULL, 'A1', true);
		    });


		    $excel->sheet('applications', function($sheet) use($applications) {
		    	$sheet->fromArray($applications, NULL, 'A1', true);
		    });

		    $excel->sheet('employees', function($sheet) use($employees) {
		    	$sheet->fromArray($employees, NULL, 'A1', true);
		    });


		    $excel->sheet('employments', function($sheet) use($employments) {
		    	$sheet->fromArray($employments, NULL, 'A1', true);
		    });

		    $excel->sheet('employment_numbers', function($sheet) use($employment_numbers) {
		    	$sheet->fromArray($employment_numbers, NULL, 'A1', true);
		    });

		   
		    $excel->sheet('benefits', function($sheet) use($benefits) {
		    	$sheet->fromArray($benefits, NULL, 'A1', true);
		    });


		    $excel->sheet('salary', function($sheet) use($salary) {
		    	$sheet->fromArray($salary, NULL, 'A1', true);
		    });

		    $excel->sheet('deductions', function($sheet) use($deductions) {
		    	$sheet->fromArray($deductions, NULL, 'A1', true);
		    });


		    $excel->sheet('timesheets', function($sheet) use($timesheets) {
		    	$sheet->fromArray($timesheets, NULL, 'A1', true);
		    });

		     $excel->sheet('trails', function($sheet) use($trails) {
		    	$sheet->fromArray($trails, NULL, 'A1', true);
		    });

					    

		})->export('xlsx');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('app');
	}
	
	
}