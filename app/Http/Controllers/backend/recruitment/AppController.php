<?php namespace App\Http\Controllers\Backend\Recruitment;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Session;
use Carbon\Carbon as Carbon;
use Response;
use Redirect;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\SaveClientRequest;
use App\Http\Requests\SaveJobRequest;
/*
|--------------
| Models
|--------------
*/
use App\User;
use App\Client;
use App\JobCategory;
use App\Job;
use App\JobDetail;
use App\JobQualification;
use App\Application;
use App\Employee;
use App\Employment;
use App\EmploymentNumber;

class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller - Backend
	|--------------------------------------------------------------------------
	|
	| This controller works on backend of the application.
	| All viewable pages by authenticated users.
	|
	*/

	protected $data = [];
	protected $date;
	protected $user;

	/**
	 * Create a new controller instance.
	 *
	 * @return voids
	 */
	public function __construct(Carbon $carbon)
	{

		$this->middleware('auth');
		$this->middleware('backend');
		//$this->middleware('department');

		$this->date = $carbon;

		$this->theme = Theme::uses('coco')->layout('backend');

		if(Auth::check()){

			$this->user = Auth::user();
			
		}
		
	}


	/**
	 * Show the backend dashboard page to the user.
	 *
	 * @return Response
	 */
	public function listClients()
	{
		
		$this->theme->appendTitle(' | Clients');
		$this->theme->set('page-title', 'Clients');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);
		
		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');

		$clients = Client::withTrashed()->get();

		$this->data = [ 'clients' => $clients ];
		
		return $this->theme->scope('backend.recruitment.client.list', $this->data)->render();
	}


	public function addClient() 
	{
		$this->theme->appendTitle(' | Add New Client');
		$this->theme->set('page-title', 'Add New Client');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);


		return $this->theme->scope('backend.recruitment.client.add', $this->data)->render();
	}



	public function saveClient(SaveClientRequest $request)
	{
		// Validate the request...

        $client = new Client();

        $client->name = $request->company_name;

        $client->representative = ucwords($request->contact_person);

        $client->address = $request->address;

        $client->email  =  $request->email;

      	$client->created_at = $this->date->now();

        $client->contact = $request->contact;

        $client->save();

         $action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Add Client',
					'done_to' => ucfirst($request->company_name),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);


        $message = '<div class="alert alert-success" role="alert"><strong>'. $request->company_name .'</strong> is successfully added as our client.</div>';

      	return Redirect::to('app/recruitment/clients')->with('message', $message);
	}

	public function editClient($id) 
	{
		$this->theme->appendTitle(' | Edit Client');
		$this->theme->set('page-title', 'Edit Client');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);
	

		$client =  Client::find($id);

		$this->data = [ 'client' => $client ];


		return $this->theme->scope('backend.recruitment.client.edit', $this->data)->render();
	}

	public function updateClient(SaveClientRequest $request)
	{

		$client = Client::find($request->client_id);

        $client->name = $request->company_name;

	    $client->representative = ucwords($request->contact_person);

        $client->address = $request->address;

        $client->email  =  $request->email;

      	$client->created_at = $this->date->now();

        $client->contact = $request->contact;

        $client->save();

        $action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Update Client',
					'done_to' => ucfirst($request->company_name),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);


        $message = '<div class="alert alert-success" role="alert"><strong>'. $request->company_name .'</strong> is successfully updated.</div>';

      	return Redirect::to('app/recruitment/clients')->with('message', $message);
	}

	public function deleteClient($id)
	{
		$client = Client::find($id);
		
		$name = $client->name;

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Delete Client',
					'done_to' => ucfirst($client->name),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$client->delete();

		$message = '<div class="alert alert-danger" role="alert"><strong>'. $name .'</strong> is now deleted.</div>';


      	return redirect('app/recruitment/clients')->with('message', $message);
	}

	public function restoreClient($id)
	{
		$client = Client::withTrashed()->where('id', $id)->first();

		$client->restore();

		$name = $client->name;


		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Restore Client',
					'done_to' => ucfirst($client->name),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-success" role="alert"><strong>'. $name .'</strong> is now restored.</div>';

		

      	return Redirect::to('app/recruitment/clients')->with('message', $message);
	}

	public function listJobs()
	{
		
		$this->theme->appendTitle(' | Job Posts');
		$this->theme->set('page-title', 'Job Posts');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');


		$jobs = Job::withTrashed()->get();

		$this->data = [ 'jobs' => $jobs];

		return $this->theme->scope('backend.recruitment.job.list', $this->data)->render();
	}

	public function addJob()
	{
		$this->theme->appendTitle(' | Add Job Post');
		$this->theme->set('page-title', 'Add Job Post');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);


		 $clients = Client::all();

		 $categories = JobCategory::all();

		 $this->data = [
		 					'clients' => $clients,
		 					'categories' => $categories
		 			   ];


		return $this->theme->scope('backend.recruitment.job.add', $this->data)->render();
	}


	public function saveJob(SaveJobRequest $request)
	{
		 // Validate the request...

        $job = new Job();

        $job->client_id = $request->client;

        $job->created_at = $this->date->now();

        $job->posted_at = $this->date->now();

        $job->expires_at = $request->expires_at;

        $job->save();

        $job_id = $job->id;

        $detail = new JobDetail();

        $detail->job_id = $job_id;

        $detail->job_category_id = $request->category;

        $detail->vacancy = $request->vacancy;

        $detail->title = $request->title;

        $detail->description = $request->description;

        $detail->location = $request->location;

        $detail->push();

        $qualification = new JobQualification();

        $qualification->job_id = $job_id;

        $qualification->gender = $request->gender;

        $qualification->age = $request->age;

        $qualification->experience = $request->experience;

        $qualification->education = $request->education;

        $qualification->push();

        $action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Add Job Post',
					'done_to' => $request->title,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

        $message = '<div class="alert alert-success" role="alert"><strong>'. $request->title .'</strong> is successfully posted.</div>';

      	return Redirect::to('app/recruitment/jobs')->with('message', $message);
	}

	public function editJob($id)
	{
		$this->theme->appendTitle(' | Edit Job Post');
		$this->theme->set('page-title', 'Edit Job Post');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$this->theme->asset()->container('header')->usePath()->add('jquery-ui-css', 'plugins/jquery-ui/css/jquery-ui.css');

		$this->theme->asset()->container('footer')->usePath()->add('jquery-ui', 'plugins/jquery-ui/js/jquery-ui.js');
		$this->theme->asset()->container('footer')->usePath()->add('clients', 'js/backend/pages/add-job.js');


		 $job = Job::find($id);

		 $clients = Client::all();

		 $categories = JobCategory::all();

		 $this->data = [
		 					'job' => $job,
		 					'clients' => $clients,
		 					'categories' => $categories
		 			   ];


		return $this->theme->scope('backend.recruitment.job.edit', $this->data)->render();
	}

	public function updateJob(SaveJobRequest $request)
	{
		$job = Job::find($request->id);

		$job->client_id = $request->client;

        $job->expires_at = $request->expires_at;

        $job->push();

        $job_id = $job->id;

        $details = [
        				'job_category_id' => $request->category,
				        'vacancy' => $request->vacancy,
				        'title' => $request->title,
				        'description' => $request->description,
				        'location' => $request->location
        		  ];

        JobDetail::where('job_id', $job_id)->update($details);


        $qualifications = [
        						'job_id' => $job_id,

						        'gender' => $request->gender,

						        'age' => $request->age,

						        'experience' => $request->experience,

						        'education' => $request->education
        				  ];

        JobQualification::where('job_id', $job_id)->update($qualifications);

         $action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit Job Post',
					'done_to' => $request->title,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

         $message = '<div class="alert alert-success" role="alert"><strong>'. $request->title .'</strong> is successfully updated.</div>';

      	return Redirect::to('app/recruitment/jobs')->with('message', $message);
	} 


	public function deleteJob($id)
	{
		$job = Job::find($id);
		
		$name = $job->detail->title;

		$job->delete();

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Delete Job Post',
					'done_to' => $name,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-danger" role="alert"><strong>'. $name .'</strong> is now deleted.</div>';


      	return Redirect::to('app/recruitment/jobs')->with('message', $message);
	}

	public function restoreJob($id)
	{
		$job = Job::withTrashed()->where('id', $id)->first();

		$job->restore();

		$name = $job->detail->title;

		$message = '<div class="alert alert-success" role="alert"><strong>'. $name .'</strong> is now restored.</div>';

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Restore Job Post',
					'done_to' => $name,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

      	return Redirect::to('app/recruitment/jobs')->with('message', $message);
	}


	public function listApplicants()
	{
		
		$this->theme->appendTitle(' | Applicants');
		$this->theme->set('page-title', 'Applicant');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);
		
		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');


		$applications = Application::withTrashed()->get();

		$this->data = [ 'applications' => $applications];

		return $this->theme->scope('backend.recruitment.applicant.list', $this->data)->render();
	}

	public function downloadResume($id)
	{
		$user = User::find($id);

		$name = $user->applicant->resume;

		$path =  storage_path();

		return Response::download(storage_path($name));
	}


	public function interviewApplicant($id)
	{
		$application = Application::find($id);

		$application->status = "For Interview";

		$application->push();

		$name = $application->user->detail->firstname . ' ' . $application->user->detail->lastname;

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Interview Applicant',
					'done_to' => $name,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-success" role="alert"><strong>'. $name .'</strong> is now for interview.</div>';

		return Redirect::to('app/recruitment/applicants')->with('message',$message);
	}	


	public function cancelApplicant($id)
	{
		$application = Application::find($id);

		$application->status = "Canceled";

		$application->push();

		$name = $application->user->detail->firstname . ' ' . $application->user->detail->lastname;


		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Fail Applicant',
					'done_to' => $name,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-danger" role="alert"><strong>'. $name ."'s".'</strong> application is now canceled.</div>';

		return Redirect::to('app/recruitment/applicants')->with('message',$message);
	}	

	public function passApplicant($id)
	{
		$application = Application::find($id);

		$application->status = "Passed";

		$application->push();

		$user_id	= $application->user_id;


		$employee = new Employee();
		$employee->client_id = $application->job->client_id;
		$employee->user_id = $user_id;
		$employee->job_id = $application->job_id;
		$employee->created_at = $this->date->now();
		$employee->save();

		$employee_id = $employee->id;

		$employment = new Employment();
		$employment->employee_id = $employee_id;
		$employment->save();
        

        $number = new EmploymentNumber();
        $number->employee_id = $employee_id;
        $number->save();


        

		$name = $application->user->detail->firstname . ' ' . $application->user->detail->lastname;

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Pass Applicant',
					'done_to' => $name,
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-success" role="alert"><strong>'. $name .'</strong> is now passed as employee. </div>';

		return Redirect::to('app/recruitment/applicants')->with('message',$message);
	}


	public function getReport()
	{
		$this->theme->appendTitle(' | Recruitment Summary');
		$this->theme->set('page-title', 'Recruitment Summary');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('user_slug', $this->user->detail->user_slug);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);


		$this->theme->asset()->container('footer')->usePath()->add('flot', 'plugins/flot/js/jquery.flot.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-pie', 'plugins/flot/js/jquery.flot.pie.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-resize', 'plugins/flot/js/jquery.flot.resize.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-categories', 'plugins/flot/js/jquery.flot.categories.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-time', 'plugins/flot/js/jquery.flot.time.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-fill', 'plugins/flot/js/jquery.flot.fillbetween.js');
		$this->theme->asset()->container('footer')->usePath()->add('flo-stack', 'plugins/flot/js/jquery.flot.stack.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-spline', 'plugins/flot/js/query.flot.spline.js');
		$this->theme->asset()->container('footer')->usePath()->add('flot-tooltip', 'plugins/flot/js/jquery.flot.tooltip.js');
		$this->theme->asset()->container('footer')->usePath()->add('chart', 'js/backend/pages/recruitment.js');

		return $this->theme->scope('backend.recruitment.report', $this->data)->render();
	}

	public function report()
	{	
		$applying = Application::where('status','New')->count();
		$interview = Application::where('status','For Interview')->count();
		$pass = Application::where('status','Passed')->count();
		$fail = Application::where('status','Canceled')->count();
		
       
           
        $data = [ $applying , $interview , $pass , $fail];

       // $data =  [49.9, 71.5, 106.4, 129.2];

       return $data;

      
   }
}