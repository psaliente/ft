<?php namespace App\Http\Controllers\Backend\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Redirect;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\LoginRequest;


class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller - Backend
	|--------------------------------------------------------------------------
	|
	| This controller works on backend of the application.
	| All viewable pages by authenticated users.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return voids
	 */
	public function __construct()
	{
		$this->middleware('guest');
		$this->theme = Theme::uses('coco');
	
	}

	public function getLogin()
	{

		$this->theme->layout('default');
		
		$this->theme->appendTitle(' | System Login');

		return $this->theme->scope('backend.login')->render();
		
	}

	public function logUser(LoginRequest $request)
	{
		if (Auth::attempt($request->credentials(), $request->remember() ) )
		{
			return Redirect::intended('app');
		    

		}else{
			
			$message = "<div class='alert alert-danger'>Username and Password do not match.</div>";
			
			return Redirect::to('app/login')->with('message', $message)->withInput();
		}
	}
}