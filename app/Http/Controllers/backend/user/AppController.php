<?php namespace App\Http\Controllers\Backend\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Carbon\Carbon as Carbon;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\SaveUserRequest;
use App\Http\Requests\UpdateUserAccountRequest;
/*	
|--------------
| Models
|--------------
*/
use App\User;
use App\UserDetail;
use App\Module;
use App\Role;
use App\ModuleRole;
use App\ModuleRoleUser;


class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller - Backend
	|--------------------------------------------------------------------------
	|
	| This controller works on backend of the application.
	| All viewable pages by authenticated users.
	|
	*/

	protected $date;
	protected $user;
	protected $data = [];

	/**
	 * Create a new controller instance.
	 *
	 * @return voids
	 */
	public function __construct(Carbon $carbon)
	{

		$this->middleware('auth');
		$this->date = $carbon;
		$this->theme = Theme::uses('coco')->layout('backend');

		if(Auth::check()){
			$this->user = Auth::user();
		}
		
	}


	public function listUsers()
	{
		

        $this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');
		/*

		$this->theme->asset()->container('footer')->usePath()->add('jquery-ui', 'plugins/jquery-ui/js/jquery-ui.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatable', 'plugins/datatables/js/jquery.dataTables.js');
		$this->theme->asset()->container('footer')->usePath()->add('users', 'js/backend/pages/users.js');
		*/

		$this->theme->appendTitle(' | Users');
		$this->theme->set('page-title', 'Users');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('user_slug', $this->user->detail->user_slug);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$users = User::OfUserType(1)->withTrashed()->get();

		$site_users = User::OfUserType(2)->withTrashed()->get();

		$this->data = [ 'users' => $users, 'site_users' => $site_users ];

		return $this->theme->scope('backend.user.list',$this->data)->render();
	}


	public function addUser()
	{
		$this->theme->appendTitle(' | Add New User');
		$this->theme->set('page-title', 'Add New User');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('user_slug', $this->user->detail->user_slug);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$modules = Module::all();
		$roles	= Role::all();

		$this->data = [
						'modules' => $modules,
						'roles'	 => $roles,
					  ];

		return $this->theme->scope('backend.user.add', $this->data)->render();
	}



	public function saveUser(SaveUserRequest $request)
	{
		$firstname = $request->firstname;
		$lastname = $request->lastname;
		$email = $request->email;

		$user = new User();

		$user->username = "";
		$user->password = "";
		$user->user_type = 1;
		$user->created_at = $this->date->now();;
		$user->updated_at = $this->date->now();

		$user->push();

		$user_id = $user->id;


		$number_code = "FT";
		$length = strlen($user_id);

		switch ($length) {
			case 6:
				$number = $user_id ;
				break;
			case 5:
				$number = '0'. $user_id ;
				break;
			case 4:
				$number = '00'.$user_id ;
				break;
			case 3:
				$number = '000'. $user_id ;
				break;
			case 2:
				$number = '0000'. $user_id ;
				break;
			default:
				$number = '00000'. $user_id ;
				break;
		}

		$username = $number_code . '-' . $number;

		

		$new_user = User::find($user_id);


		$new_user ->username = $username;
		$new_user ->password = bcrypt($username);
	

		$new_user ->save();

		$detail = new UserDetail();

		$detail->user_id = $user_id;
		$detail->firstname = ucfirst($firstname);
		$detail->lastname = ucfirst($lastname);
		$detail->user_slug = str_slug($firstname . ' ' . $lastname);
		$detail->profile_photo = "avatar.png";
		$detail->email = $email;

		$detail->save();

		$module_role = new ModuleRole();

		$module_role->module_id = $request->module;
		$module_role->role_id = $request->role;

		$module_role->push();

		$module_role_id = $module_role->id;

		$role_user = new ModuleRoleUser();
		$role_user->module_role_id = $module_role_id;
		$role_user->user_id = $user_id;

		$role_user->push();

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Add User',
					'done_to' => ucfirst($firstname) . " " . ucfirst($lastname),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-success">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="icon-remove"></i>
                          </button>
                          '. ucfirst($firstname) . ' ' .ucfirst($lastname)  . ' is now added as new user. Username & Password: <strong>' . $username . '</strong>'.
                    '</div>';

		return redirect('app/users')->with('user_message', $message);


	}


	public function viewUser($id)
	{

		$user = User::find($id);

		$this->theme->appendTitle(' | ' . ucfirst($user->detail->firstname) . ' ' .ucfirst($user->detail->lastname) );
		$this->theme->set('page-title', ucfirst($user->detail->firstname) . ' ' .ucfirst($user->detail->lastname) );
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);


		$this->data = [
						'user' => $user
					  ];

		
		return $this->theme->scope('backend.user.view', $this->data)->render();
	}



	public function editUser($id)
	{
		$this->theme->appendTitle(' | Edit User');
		$this->theme->set('page-title', 'Edit User');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('user_slug', $this->user->detail->user_slug);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$user = User::find($id);
		$modules = Module::all();
		$roles	= Role::all();	

		$this->data = [
						'user'	=> $user,
						'modules' => $modules,
						'roles'	 => $roles,
					  ];

		return $this->theme->scope('backend.user.edit', $this->data)->render();
	}


	public function updateUser(SaveUserRequest $request)
	{
		$firstname = ucfirst($request->firstname);
		$lastname = ucfirst($request->lastname);
		$email = $request->email;
		$id = $request->id;



		$detail = [
						'firstname' => $firstname,
						'lastname'	=> $lastname,
						'user_slug' => str_slug($firstname . ' ' . $lastname),
						'email'		=> $email
				  ];

		UserDetail::where('user_id',$id)->update($detail);


		$role_user = ModuleRoleUser::where('user_id',$id)->first();

		$module_role = ModuleRole::find($role_user->module_role_id);

		$module_role->module_id = $request->module;
		$module_role->role_id = $request->role;

		$module_role->save();

		$user = User::find($id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit User',
					'done_to' => ucfirst($user->detail->firstname) . " " . ucfirst($user->detail->lastname),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-success">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="icon-remove"></i>
                          </button>
                          '. ucfirst($user->detail->firstname) . ' ' .ucfirst($user->detail->lastname)  . "'s detail is now updated." .
                    '</div>';

		return redirect()->intended('app/users')->with('user_message', $message);
	}


	public function updateUserAccount(UpdateUserAccountRequest $request)
	{
			$id = $request->id;
			$user = User::find($id);

			$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Edit User Account',
					'done_to' => ucfirst($user->detail->firstname) . " " . ucfirst($user->detail->lastname),
					'created_at' => $this->date->now()
				  ];

			$this->trail($action);

			$user->password = bcrypt($request->new_password);
			$user->push();

			$message = '<div class="alert alert-success">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="icon-remove"></i>
                          </button>
                          '. ucfirst($user->detail->firstname) . ' ' .ucfirst($user->detail->lastname)  . "'s account is now updated." .
                    '</div>';

		return redirect()->intended('app/user/edit/'.$id)->with('account_message', $message);

	}




	public function deleteUser($id)
	{
		$user= User::find($id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Delete User',
					'done_to' => ucfirst($user->detail->firstname) . " " . ucfirst($user->detail->lastname),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$user->delete();


		$message = '<div class="alert alert-danger">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="icon-remove"></i>
                          </button>
                          '. ucfirst($user->detail->firstname) . ' ' .ucfirst($user->detail->lastname)  . ' is now moved to trash.' .
                    '</div>';

		return redirect()->intended('app/users')->with('user_message', $message);
	}

	public function restoreUser($id)
	{
		$user = User::onlyTrashed()->where('id', $id)->restore();

		$user= User::find($id);

		$action = [
					'done_by' => $this->user->detail->firstname . ' ' . $this->user->detail->lastname,
					'action' => 'Restore User',
					'done_to' => ucfirst($user->detail->firstname) . " " . ucfirst($user->detail->lastname),
					'created_at' => $this->date->now()
				  ];

		$this->trail($action);

		$message = '<div class="alert alert-success">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="icon-remove"></i>
                          </button>
                          '. ucfirst($user->detail->firstname) . ' ' .ucfirst($user->detail->lastname)  . "'s account is now active again." .
                    '</div>';

		return redirect()->intended('app/users')->with('user_message', $message);
	}

}

