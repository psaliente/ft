<?php namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Carbon\Carbon as Carbon;
use Redirect;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\SigninRequest;
use App\Http\Requests\SaveClientRequest;
use App\Http\Requests\SaveApplicantRequest;
/*
|--------------
| Models
|--------------
*/
use App\User;
use App\UserDetail;
use App\Applicant;

class AuthController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Application Controller
	|--------------------------------------------------------------------------
	|
	| This controller works on frontend of the application.
	| All viewable pages by authenticated users.
	|
	*/
	protected $date;


	public function __construct(Carbon $carbon)
	{
		$this->middleware('guest');
		$this->date = $carbon;
		$this->theme = Theme::uses('coco')->layout('frontend');
	}


	public function index()
	{


		$this->theme->appendTitle(' | Home');
	
		return $this->theme->scope('frontend.index')->render();
	}


	public function login()
	{
        
		$this->theme->appendTitle(' | Login to your account');
	
		return $this->theme->scope('frontend.login')->render();
	}

	public function loginUser(SigninRequest $request)
	{


		if (Auth::attempt($request->credentials(), $request->remember() ) )
		{
		
			return Redirect::intended('home');

		}else{
			$message = "<div class='alert alert-danger'>Username and Password do not match.</div>";
			
			return Redirect::to('signup')->with('message', $message)->withInput();
		}
	}

	public function signup()
	{
		$this->theme->appendTitle(' | Signup');

		return $this->theme->scope('frontend.signup')->render();
	}

	public function signupUser(SaveApplicantRequest $request)
	{
		$user = new User();

        $user->username = $request->email;

        $user->password = bcrypt($request->password);

        $user->user_type = 3;

        $user->created_at = $this->date->now();

        $user->is_verified = true;

        $user->save();

        $user_id = $user->id;

        $detail = new UserDetail;

        $detail->user_id = $user->id;

        $detail->firstname = ucfirst($request->firstname);

        $detail->middlename = ucfirst($request->middlename);

        $detail->lastname = ucfirst($request->lastname);

        $detail->user_slug = str_slug($request->firstname . ' ' . $request->lastname);

        $detail->save();

        $applicant = new Applicant;

        $applicant->user_id = $user_id;

        $applicant->save();

        
        $user = User::find($user_id);

        Auth::login($user);

        $message = '<div class="alert alert-info" role="alert">You are already registered. Please complete the following form for your profile. <b><a href="' . url('account/edit') .'">Here</a></b></div>';

      	return Redirect::intended('account/edit')->with('message', $message);
        
	}
}