<?php namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Carbon\Carbon as Carbon;
/*
|--------------
| Requests
|--------------
*/

/*
|--------------
| Models
|--------------
*/


class SiteController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller
	|--------------------------------------------------------------------------
	|
	| This controller works on frontend of the application.
	| All viewable pages by authenticated users.
	|
	*/
	protected $date;
	protected $data = [];
	protected $user;

	/**
	 * Create a new controller instance.
	 *
	 * @return View
	 */
	public function __construct(Carbon $carbon)
	{
		//$this->middleware('guest');
		$this->date = $carbon;
		$this->theme = Theme::uses('coco')->layout('frontend');

		if(Auth::check()){
			$this->user = Auth::user();
			$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		}
	}

	public function index()
	{
		$this->theme->asset()->container('header')->usePath()->add('css-carousel', 'front/assets/libs/owl-carousel/owl.carousel.css');
		$this->theme->asset()->container('header')->usePath()->add('owl.theme.', 'front/assets/libs/owl-carousel/owl.theme.css');
		$this->theme->asset()->container('header')->usePath()->add('owl.transitions', 'front/assets/libs/owl-carousel/owl.transitions.css');
		$this->theme->asset()->container('header')->usePath()->add('magnific-popup', 'front/assets/libs/jquery-magnific/magnific-popup.css');


		$this->theme->asset()->container('footer')->usePath()->add('owl.carousel.min', 'front/assets/libs/owl-carousel/owl.carousel.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('jquery.magnific-popup.min', 'front/assets/libs/jquery-magnific/jquery.magnific-popup.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('index', 'front/assets/js/pages/index.js');


		$this->theme->appendTitle(' | Home');
	
		return $this->theme->scope('frontend.index')->render();
	}
}