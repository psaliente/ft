<?php namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Carbon\Carbon as Carbon;
use Redirect;
use Session;
use Hash;
use Response;
/*
|--------------
| Requests
|--------------
*/
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\SaveResumeRequest;
use App\Http\Requests\UpdateAccountRequest;
/*
|--------------
| Models
|--------------
*/
use App\User;
use App\UserDetail;
use App\Applicant;
use App\Application;

class AppController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Application Controller
	|--------------------------------------------------------------------------
	|
	| This controller works on frontend of the application.
	| All viewable pages by authenticated users.
	|
	*/
	protected $date;
	protected $user;
	/**
	 * Create a new controller instance.
	 *
	 * @return View
	 */
	public function __construct(Carbon $carbon)
	{
		$this->middleware('auth');
		$this->middleware('frontend');
		$this->date = $carbon;
		$this->theme = Theme::uses('coco')->layout('frontend');

		if(Auth::check()){
			$this->user = Auth::user();
		}
	}

	/**
	 * Show the dashboard page to the user.
	 *
	 * @return View
	 */
	public function index()
	{
		
		$this->theme->appendTitle(' | Home');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);
	
		return $this->theme->scope('frontend.applicant.dashboard')->render();
	}

	public function applications()
	{

		$this->theme->asset()->container('header')->usePath()->add('css-datatable', 'libs/jquery-datatables/css/dataTables.bootstrap.css');
        $this->theme->asset()->container('header')->usePath()->add('dataTables.tableTools', 'libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css');
		
		$this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables.min', 'libs/jquery-datatables/js/jquery.dataTables.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'libs/jquery-datatables/js/dataTables.bootstrap.js');
		$this->theme->asset()->container('footer')->usePath()->add('dataTables.tableTools.min', 'libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
		$this->theme->asset()->container('footer')->usePath()->add('datatables', 'js/pages/datatables.js');
		
		$this->theme->appendTitle(' | My Applications');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);

		$applications = Application::withTrashed()->where('user_id', $this->user->id)->get();

		$this->data = ['applications' => $applications];
	
		return $this->theme->scope('frontend.applicant.applications.index', $this->data)->render();
	}

	public function account()
	{
	
		$this->theme->appendTitle(' | My Account');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', $this->user->detail->profile_photo);
	
		return $this->theme->scope('frontend.applicant.account.index')->render();
	}

	public function editAccount()
	{	

		$this->theme->asset()->usePath()->add('jquery-ui', 'libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css');
		$this->theme->asset()->usePath()->add('datepicker', 'libs/bootstrap-datepicker/css/datepicker.css');

		$this->theme->asset()->usePath()->add('jquery-1.11.1.min', 'libs/jquery/jquery-1.11.1.min.js');
		$this->theme->asset()->usePath()->add('jquery-ui-1.10.4.custom.min', 'libs/jqueryui/jquery-ui-1.10.4.custom.min.js');
		$this->theme->asset()->usePath()->add('bootstrap-datepicker', 'libs/bootstrap-datepicker/js/bootstrap-datepicker.js');
		$this->theme->asset()->usePath()->add('init', 'js/init.js');

		$this->theme->appendTitle(' | Edit Account');
		$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		$this->theme->set('gravatar', 'avatart.png' );
	
		return $this->theme->scope('frontend.applicant.account.edit')->render();
	}

	public function uploadResume(SaveResumeRequest $request)
	{
		$user = User::find($request->id);

		$file = $request->file('resume');

		$extension =  $file->getClientOriginalExtension();


		$filename = snake_case($user->detail->firstname .' '. $user->detail->lastname . '_resume.'.$extension); 
		$path =  storage_path();

		$detail = ['resume' => $filename];

		Applicant::where('user_id', $request->id)->update($detail);

		$file->move($path, $filename);

		$message = "<div class='alert alert-success'>Resume has been saved.</div>";
			
		return Redirect::to('account/edit')->with('message', $message)->withInput();

	}

	public function downloadResume($id)
	{	
		$user = User::find($id);

		$name = $user->applicant->resume;

		$path =  storage_path();

		return Response::download(storage_path($name));
	}


	public function updateProfile(UpdateProfileRequest $request)
	{
		

		
		$user = User::find($this->user->id);

        $user->updated_at = $this->date->now();

        $user->push();


         $details = [
        			'firstname' => ucfirst($request->firstname),
        			'lastname' => ucfirst($request->lastname),
        			'user_slug' => str_slug($request->firstname . ' '. $request->lastname),
        			'contact' =>  $request->contact
        		   ];


        UserDetail::where('user_id', $this->user->id)->update($details);


        $applicant = Applicant::find($request->app_id);

        $applicant->birthdate = $request->birthdate;

        $applicant->gender = $request->gender;

        $applicant->civil_status = $request->civil_status;

        $applicant->height = $request->height;

        $applicant->weight = $request->weight;

        $applicant->address = $request->address;

        $applicant->save();
      

      

        $message = '<div class="alert alert-success" role="alert">Your profile was successfully updated.</div>';


		return Redirect::to('account/edit')->with('message', $message)->withInput();



	}

	public function updateAccount(UpdateAccountRequest $request)
	{
		$current_password = $this->user->password;

		if(Hash::check($request->old_password, $current_password)) {

			$user = User::find($this->user->id);

			$user->password = bcrypt($request->new_password);

	        $user->updated_at = $this->date->now();

	        $user->push();

	         $message = '<div class="alert alert-success" role="alert">Your account was successfully updated.</div>';

		}else {

			$message = '<div class="alert alert-danger" role="alert">Your old password is not the same as it is saved on our application.</div>';

		}

		return Redirect::to('account/edit')->with('message', $message)->withInput();
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('home');
	}
}
