<?php namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*
|--------------
| Vendors
|--------------
*/
use Theme;
use Auth;
use Carbon\Carbon as Carbon;
use Redirect;
/*
|--------------
| Requests
|--------------
*/

/*
|--------------
| Models
|--------------
*/
use App\Job;
use App\JobCategory;
use App\Application;


class JobController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Application Controller
	|--------------------------------------------------------------------------
	|
	| This controller works on frontend of the application.
	| All viewable pages by authenticated users.
	|
	*/
	protected $date;
	protected $data;
	protected $user;

	/**
	 * Create a new controller instance.
	 *
	 * @return View
	 */
	public function __construct(Carbon $carbon)
	{
		//$this->middleware('guest');
		$this->date = $carbon;
		$this->theme = Theme::uses('coco')->layout('frontend');

		if(Auth::check()){
			$this->user = Auth::user();
			$this->theme->set('name', $this->user->detail->firstname . ' ' . $this->user->detail->lastname);
		}
	}

	/**
	 * Show the dashboard page to the user.
	 *
	 * @return View
	 */
	public function listJob() 
	{
  	
		$this->theme->appendTitle(' | Job Posts');
		

		$jobs = Job::orderBy('created_at', 'desc')->get();

		$categories = JobCategory::all();

		$this->data =  ['jobs' => $jobs, 'categories'=> $categories];
	
		return $this->theme->scope('frontend.job.list', $this->data)->render();
	}

	public function viewJob($id)
	{
  	
  		$job = Job::findOrFail($id);

		$this->theme->appendTitle(' | ' . $job->client->name .  ' | ' . $job->detail->title);


		$categories = JobCategory::all();

		$this->data =  ['job' => $job, 'categories'=> $categories];	
	
		return $this->theme->scope('frontend.job.view', $this->data)->render();
	
	}

	public function applyJob($id)
	{
		$resume = $this->user->applicant->resume;

		if(!empty($resume)) {

			$application = Application::where('user_id', $this->user->id)->where('job_id', $id)->first();

			//dd($application);

			if($application == NULL) {
				$application = new Application();

				$application->job_id = $id;
				$application->user_id = $this->user->id;
				$application->created_at = $this->date->now();
				$application->status = "New";

				$application->save();

				$message = '<div class="alert alert-success" role="alert">You are successfully applied for that job post.</div>';


				return Redirect::to('applications')->with('message', $message);

			} else {
				$message = '<div class="alert alert-danger" role="alert">You have already applied for this job post</div>';

				return Redirect::to('applications')->with('message', $message);
			}

		}else {

			$message = '<div class="alert alert-danger" role="alert">You must upload your resume first to be able to apply for this job post. Upload <a href="'. url('account/edit') .'">here</a></div>';


			return Redirect::to('job/view/'.$id)->with('message', $message);
		}
	}

	public function cancelApplication($id)
	{
		$application = Application::find($id);
		
		$application->status = "Canceled";

		$application->save();

		$application->delete();

		$message = '<div class="alert alert-success" role="alert">Your application is now canceled.</div>';


      	return redirect('applications')->with('message', $message);
	}
}