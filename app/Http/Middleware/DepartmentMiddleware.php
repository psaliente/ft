<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\UserDepartment;
use App\Department;

class DepartmentMiddleware {
	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;
	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$departments = UserDepartment::where('user_id', $this->auth->user()->id)->lists('department_id')->toArray();
		$department = 	Department::where('permalink', $request->segment(2))->pluck('id');
		if (!in_array($department, $departments))
		{
			return redirect()->intended('app')->with('message',"The page you're trying to access is not permitted.");
		}
		return $next($request);
	}

}
