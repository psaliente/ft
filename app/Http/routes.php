<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|------------------------------------------
|	Backend Application 
|------------------------------------------
*/
/*
|------------------------------------------
|	Module: Authentication
|------------------------------------------
*/
Route::get('app/login', 'Backend\Auth\AppController@getLogin');
Route::post('log/user', 'Backend\Auth\AppController@logUser');
/*
|------------------------------------------
|	Authenticated Backend Application
|------------------------------------------
*/
/*
|------------------------------------------
|	Module: Application
|------------------------------------------
*/
Route::get('app', 'Backend\AppController@index');
Route::get('activity', 'Backend\AppController@activity');
Route::get('backup', 'Backend\AppController@backup');
Route::get('recruitment', 'Backend\Recruitment\AppController@report');
Route::get('app/logout', 'Backend\AppController@logout');
/*
|------------------------------------------
|	Module: Users 
|------------------------------------------
*/
Route::get('app/users', 'Backend\User\AppController@listUsers');
Route::get('app/user/add', 'Backend\User\AppController@addUser');
Route::post('app/user/save', 'Backend\User\AppController@saveUser');
Route::get('app/user/view/{id}', 'Backend\User\AppController@viewUser');
Route::get('app/user/edit/{id}', 'Backend\User\AppController@editUser');
Route::post('app/user/update', 'Backend\User\AppController@updateUser');
Route::get('app/user/delete/{id}', 'Backend\User\AppController@deleteUser');
Route::get('app/user/restore/{id}', 'Backend\User\AppController@restoreUser');
Route::get('app/user/account/edit/{id}', 'Backend\User\AppController@editUserAccount');
Route::post('app/user/account/update', 'Backend\User\AppController@updateUserAccount');
/*
|------------------------------------------
|	Module: Recruitment - Clients
|------------------------------------------
*/
Route::get('app/recruitment/clients', 'Backend\Recruitment\AppController@listClients');
Route::get('app/recruitment/client/add', 'Backend\Recruitment\AppController@addClient');
Route::post('app/recruitment/client/save', 'Backend\Recruitment\AppController@saveClient');
Route::get('app/recruitment/client/edit/{id}', 'Backend\Recruitment\AppController@editClient');
Route::post('app/recruitment/client/update', 'Backend\Recruitment\AppController@updateClient');
Route::get('app/recruitment/client/delete/{id}', 'Backend\Recruitment\AppController@deleteClient');
Route::get('app/recruitment/client/restore/{id}', 'Backend\Recruitment\AppController@restoreClient');
Route::get('app/recruitment/report', 'Backend\Recruitment\AppController@getReport');
/*
|------------------------------------------
|	Module: Recruitment - Jobs
|------------------------------------------
*/
Route::get('app/recruitment/jobs', 'Backend\Recruitment\AppController@listJobs');
Route::get('app/recruitment/job/add', 'Backend\Recruitment\AppController@addJob');
Route::post('app/recruitment/job/save', 'Backend\Recruitment\AppController@saveJob');
Route::get('app/recruitment/job/edit/{id}', 'Backend\Recruitment\AppController@editJob');
Route::post('app/recruitment/job/update', 'Backend\Recruitment\AppController@updateJob');
Route::get('app/recruitment/job/delete/{id}', 'Backend\Recruitment\AppController@deleteJob');
Route::get('app/recruitment/job/restore/{id}', 'Backend\Recruitment\AppController@restoreJob');
/*
|------------------------------------------
|	Module: Recruitment - Applicants
|------------------------------------------
*/
Route::get('app/recruitment/applicants', 'Backend\Recruitment\AppController@listApplicants');
Route::get('app/recruitment/applicant/dowload/resume/{id}', 'Backend\Recruitment\AppController@downloadResume');
Route::get('app/recruitment/applicant/interview/{id}', 'Backend\Recruitment\AppController@interviewApplicant');
Route::get('app/recruitment/applicant/cancel/{id}', 'Backend\Recruitment\AppController@cancelApplicant');
Route::get('app/recruitment/applicant/pass/{id}', 'Backend\Recruitment\AppController@passApplicant');

/*
|------------------------------------------
|	Module: Recruitment - Employees
|------------------------------------------
*/
Route::get('app/employee/employees', 'Backend\Employee\AppController@listEmployee');
Route::get('app/employees/employee/view/{id}', 'Backend\Employee\AppController@employee');
Route::get('app/employees/employee/employment/edit/{id}', 'Backend\Employee\AppController@editEmployment');
Route::post('app/employees/employee/employment/save', 'Backend\Employee\AppController@updateEmployment');
Route::get('app/employees/employee/number/edit/{id}', 'Backend\Employee\AppController@editNumber');
Route::post('app/employees/employee/number/update', 'Backend\Employee\AppController@updateNumber');

Route::get('app/employee/{id}/leave/add', 'Backend\Employee\AppController@addLeave');
Route::post('app/employee/leave/save', 'Backend\Employee\AppController@saveLeave');
Route::get('app/employee/leave/edit/{id}', 'Backend\Employee\AppController@editLeave');
Route::post('app/employee/leave/update', 'Backend\Employee\AppController@updateLeave');
Route::get('app/employee/leave/delete/{id}', 'Backend\Employee\AppController@deleteLeave');


Route::get('app/employee/{id}/benefit/add', 'Backend\Employee\AppController@addBenefit');
Route::post('app/employee/benefit/save', 'Backend\Employee\AppController@saveBenefit');
Route::get('app/employee/benefit/edit/{id}', 'Backend\Employee\AppController@editBenefit');
Route::post('app/employee/benefit/update', 'Backend\Employee\AppController@updateBenefit');
Route::get('app/employee/benefit/delete/{id}', 'Backend\Employee\AppController@deleteBenefit');

Route::get('app/employee/{id}/deduction/add', 'Backend\Employee\AppController@addDeduction');
Route::post('app/employee/deduction/save', 'Backend\Employee\AppController@saveDeduction');
Route::get('app/employee/deduction/edit/{id}', 'Backend\Employee\AppController@editDeduction');
Route::post('app/employee/deduction/update', 'Backend\Employee\AppController@updateDeduction');
Route::get('app/employee/deduction/delete/{id}', 'Backend\Employee\AppController@deleteDeduction');


Route::get('app/employee/{id}/salary/add', 'Backend\Employee\AppController@addSalary');
Route::post('app/employee/salary/save', 'Backend\Employee\AppController@saveSalary');
Route::get('app/employee/salary/edit/{id}', 'Backend\Employee\AppController@editSalary');
Route::post('app/employee/salary/update', 'Backend\Employee\AppController@updateSalary');


Route::get('app/employee/leave/request', 'Backend\Employee\AppController@myLeaveRequest');
Route::get('app/employee/leave/request/add', 'Backend\Employee\AppController@requestLeave');
Route::post('app/employee/leave/request/save', 'Backend\Employee\AppController@saveRequest');


Route::get('app/employee/leaves', 'Backend\Employee\AppController@listLeave');
Route::get('app/employee/leave/request/approve/{id}', 'Backend\Employee\AppController@approveLeave');
Route::get('app/employee/leave/request/disapprove/{id}', 'Backend\Employee\AppController@disapproveLeave');

/*
|------------------------------------------
|	Module: Recruitment - Payroll
|------------------------------------------
*/

Route::get('app/payroll/clients', 'Backend\Payroll\AppController@clients');
Route::get('app/payroll/client/{id}', 'Backend\Payroll\AppController@client');
Route::post('app/payroll/upload/timesheet', 'Backend\Payroll\AppController@uploadTimesheet');
Route::get('app/payroll/view/employee/timesheet/{id}', 'Backend\Payroll\AppController@processTimesheet');
Route::post('app/payroll/employee/timesheet', 'Backend\Payroll\AppController@getEmployeeTimesheet');
Route::get('app/payroll/employee/{id}/timesheet/{from}/{to}', 'Backend\Payroll\AppController@viewEmployeeTimesheet');
Route::get('app/payroll/employee/{id}/timesheet/{from}/{to}/payslip', 'Backend\Payroll\AppController@viewEmployeePayslip');

















/*
|------------------------------------------
|	Frontend Application
|------------------------------------------
*/
/*
|------------------------------------------
|	Module: Website
|------------------------------------------
*/
Route::get('/', 'Frontend\SiteController@index');
/*
|------------------------------------------
|	Module: Recruitment - Jobs
|------------------------------------------
*/
Route::get('jobs', 'Frontend\JobController@listJob');
Route::get('job/view/{id}', 'Frontend\JobController@viewJob');
Route::get('job/apply/{id}', 'Frontend\JobController@applyJob');
Route::get('job/application/cancel/{id}', 'Frontend\JobController@cancelApplication');
/*
|------------------------------------------
|	Module: Authentication
|------------------------------------------
*/
Route::get('login', 'Frontend\AuthController@login');
Route::post('login', 'Frontend\AuthController@loginUser');
Route::get('signup', 'Frontend\AuthController@signup');
Route::post('signup', 'Frontend\AuthController@signupUser');
Route::get('forgot', 'Frontend\AuthController@password');

/*
|------------------------------------------
|	Authenticated Frontend Application
|------------------------------------------
*/
/*
|------------------------------------------
|	Module: Recruitment - Applicant
|------------------------------------------
*/
Route::get('home', 'Frontend\AppController@index');
Route::get('applications', 'Frontend\AppController@applications');
Route::get('account', 'Frontend\AppController@account');
Route::get('account/edit', 'Frontend\AppController@editAccount');
Route::post('profile/update', 'Frontend\AppController@updateProfile');
Route::post('account/update', 'Frontend\AppController@updateAccount');
Route::post('upload/resume', 'Frontend\AppController@uploadResume');
Route::get('download/resume/{id}', 'Frontend\AppController@downloadResume');
Route::get('logout', 'Frontend\AppController@logout');