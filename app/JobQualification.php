<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobQualification extends Model
{
    protected $table = 'job_qualifications';

	public $timestamps = false;

	public function job() {

    	return $this->belongsTo('App\Job');

    }	
}
